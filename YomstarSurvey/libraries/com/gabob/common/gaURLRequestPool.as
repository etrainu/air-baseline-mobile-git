﻿/**
 * 
 **/
package com.gabob.common {
	import flash.display.MovieClip;
	import flash.net.URLRequest;

	public class GaURLRequestPool {
		function GaURLRequestPool(blockSize:int) {
			_blockSize = blockSize;
			_objs = new GaList();
			allocateBlock();
		}

		/**
		 * Interface
		 **/

		// Call this to get an object from the pool.  Just cast it to whatever it is.
		public function getObj():URLRequest {
			if(_objs.head == null)
				allocateBlock();
			return URLRequest(_objs.removeHead());
		}

		// Call this to return the no-longer-needed object to the pool, so someone else can use it later.
		public function returnObj(obj:URLRequest):void {
			_objs.add(obj);
		}

		// This deallocates objects until only blockSize objs remain.
		public function clean():void {
			while(_objs.getCount() > _blockSize)
				_objs.removeHead();
		}

		public function destroy():void {
			_objs.clear();
		}

		/**
		 * Implementation
		 **/

		private var _class:Class, _blockSize:int, _objs:GaList;

		private function allocateBlock():void {
			for(var i:int = 0; i < _blockSize; i++) {
				var obj:Object = new URLRequest("");
				_objs.add(obj);
			}
		}
	}
}
