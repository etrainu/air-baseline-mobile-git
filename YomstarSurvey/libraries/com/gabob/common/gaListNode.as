/**
 * 
 **/
package com.gabob.common {
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;

	public class GaListNode {
		function GaListNode() {
		}

		/**
		 * Interface
		 **/

		public var next:GaListNode = null;
		public var prev:GaListNode = null;

		public var data:* = null;

		public var allocated:Boolean = true;

		/**
		 * Implementation
		 **/
	}
}
