package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.config.YomstarGlobalConsts;
	import com.yomstar.app.dataObjects.FreeTextQuestionData;
	import com.yomstar.app.dataObjects.FreeTextResponseData;
	import com.yomstar.app.dataObjects.MultiChoiceQuestionData;
	import com.yomstar.app.dataObjects.MultiChoiceResponseData;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.MatchChoices;
	
	import feathers.controls.Check;
	import feathers.controls.Label;
	import feathers.controls.Radio;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.core.ToggleGroup;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import flash.text.TextFormatAlign;
	
	import starling.display.Image;
	import starling.events.Event;
	
	public class QuestionMultiChoice extends ScrollContainer
	{
		private const GAP:uint = 20;
		private const PADDING_TOP:uint = 10;
		private const PADDING_RIGHT:uint = 10;
		private const PADDING_LEFT:uint = 10;
		private const PADDING_BOTTOM:uint = 10;
		
		private var _questionTypeLabel:Label;
		private var _questionTextLabel:Label;
		
		private var _question:MultiChoiceQuestionData;
		
		private var _artImage:Image;
		private var _initialized:Boolean;
		private var _headerColumn:ScrollContainer;
		private var _contentColumn:ScrollContainer;
		
		private var specifyInput:TextInput;
		
		private var _radioGroup:ToggleGroup;
		public function QuestionMultiChoice(question:MultiChoiceQuestionData)
		{
			super();
			_initialized = false;
			
			
			
			this._question = question;
			this.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			this.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this.scrollerProperties.snapScrollPositionsToPixels = true;
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = GAP;
			layout.paddingTop = PADDING_TOP;
			layout.paddingRight = PADDING_RIGHT;
			layout.paddingBottom = PADDING_BOTTOM;
			layout.paddingLeft = PADDING_LEFT;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_LEFT;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			this.layout = layout;
		}
		override protected function draw():void
		{
			if(!_initialized){
				this._questionTypeLabel.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT);
				this._questionTypeLabel.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
				this._questionTypeLabel.textRendererProperties.wordWrap = true;
				this._questionTextLabel.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT_DARK);
				this._questionTextLabel.textRendererProperties.wordWrap = true;
				this._initialized = true;
			}
			var realEstateWidth:int = this.actualWidth - PADDING_LEFT - PADDING_RIGHT;
			this._contentColumn.width = realEstateWidth;
			this._headerColumn.width = realEstateWidth;
			_questionTextLabel.width = realEstateWidth-_artImage.width-GAP;
			_contentColumn.invalidate(FeathersControl.INVALIDATION_FLAG_ALL);
			_contentColumn.validate();
			_headerColumn.invalidate(FeathersControl.INVALIDATION_FLAG_ALL);
			_headerColumn.validate();
			super.draw();
		}
		
		override protected function initialize():void
		{
			
			this.backgroundSkin = new LightPad();
			this._artImage = new Image(R.getAtlas().getTexture("question_multi_choice"));
			this._artImage.touchable = false;
			_headerColumn = new ScrollContainer();
			const headerColumnLayout:HorizontalLayout = new HorizontalLayout();
			headerColumnLayout.gap = GAP;
			headerColumnLayout.paddingTop = 0;
			headerColumnLayout.paddingRight = 0;
			headerColumnLayout.paddingBottom = 0;
			headerColumnLayout.paddingLeft = 0;
			headerColumnLayout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			headerColumnLayout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_MIDDLE;
			_headerColumn.layout = headerColumnLayout;
			_headerColumn.addChild(this._artImage);
			_headerColumn.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_headerColumn.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			
			_contentColumn = new ScrollContainer();
			_contentColumn.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_contentColumn.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			
			const contentColumnLayout:VerticalLayout = new VerticalLayout();
			contentColumnLayout.gap = GAP;
			contentColumnLayout.paddingTop = 0;
			contentColumnLayout.paddingRight = 0;
			contentColumnLayout.paddingBottom = 0;
			contentColumnLayout.paddingLeft = 0;
			contentColumnLayout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			contentColumnLayout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			_contentColumn.layout = contentColumnLayout;
			this.addChild(_contentColumn);
			_contentColumn.addChild(_headerColumn);
			this._questionTypeLabel = new Label();
			this._questionTypeLabel.text = YomstarGlobalConsts.MULTI_CHOICE_QUESTION_TYPE_TITLE;
			_contentColumn.addChild(this._questionTypeLabel);
			
			this._questionTextLabel = new Label();
			this._questionTextLabel.text = this._question.questionText;
			_headerColumn.addChild(this._questionTextLabel);
			
			var len:int = _question.options.length;
			var i:int;
			if(!this._question.isMulti){
				this._radioGroup = new ToggleGroup();
				this._radioGroup.isSelectionRequired = false;
				this._radioGroup.addEventListener(Event.CHANGE, radioGroup_changeHandler);
				var rChoice:Radio;
				for(i=0; i<len; i++){
					rChoice = new Radio();
					rChoice.labelOffsetX = 10;
					rChoice.defaultLabelProperties = rChoice.downLabelProperties = rChoice.upLabelProperties = rChoice.hoverLabelProperties = {textFormat:TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT_DARK), wordWrap : true, align : TextFormatAlign.LEFT};
					rChoice.defaultSelectedLabelProperties = rChoice.selectedHoverLabelProperties = rChoice.selectedDownLabelProperties = rChoice.selectedUpLabelProperties = {textFormat:TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT_HIGHLIGHT), wordWrap : true, align : TextFormatAlign.LEFT};
					rChoice.label = _question.options[i].description;
					//this._radio1.toggleGroup = this._radioGroup;
					this._radioGroup.addItem(rChoice);
					_contentColumn.addChild(rChoice);
				}
			}else{
				var cChoice:Check;
				for(i=0; i<len; i++){
					cChoice = new Check();
					cChoice.labelOffsetX = 10;
					cChoice.defaultLabelProperties = cChoice.downLabelProperties = cChoice.upLabelProperties = cChoice.hoverLabelProperties = {textFormat:TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT_DARK), wordWrap : true, align : TextFormatAlign.LEFT};
					cChoice.defaultSelectedLabelProperties = cChoice.selectedHoverLabelProperties = cChoice.selectedDownLabelProperties = cChoice.selectedUpLabelProperties = {textFormat:TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT_HIGHLIGHT), wordWrap : true, align : TextFormatAlign.LEFT};
					cChoice.label = _question.options[i].description;
					//this._radio1.toggleGroup = this._radioGroup;
					_contentColumn.addChild(cChoice);
					cChoice.addEventListener(Event.CHANGE, cChoice_changeHandler);
				}
			}
			trace("this._question.allowOther: "+this._question.allowOther);
			if(this._question.allowOther){
				trace("Adding field");
				specifyInput = new TextInput();
				specifyInput.height = 150;
				specifyInput.isEnabled = false;
				_contentColumn.addChild(specifyInput);
			}
			this.touchable = true;
			super.initialize();
		}
		private function cChoice_changeHandler(event:Event):void
		{
			var cChoice:Check = event.currentTarget as Check;
			trace("cChoice change:", cChoice.isSelected);
			if(cChoice.isSelected && cChoice.label == YomstarGlobalConsts.MULTICHOICE_EXTRA_INFO_LABEL){
				trace("Adding field");
				if(this._question.allowOther){
					specifyInput.isEnabled = true;
				}
			}else{
				if(this._question.allowOther && cChoice.label == YomstarGlobalConsts.MULTICHOICE_EXTRA_INFO_LABEL){
					if(specifyInput.isEnabled){
						trace("Removing field");
						specifyInput.isEnabled = false;
					}
				}
			}
		}
		private function radioGroup_changeHandler(event:Event):void
		{
			if(_initialized){
				trace("radio group change:", this._radioGroup.selectedIndex);
				if(this._radioGroup.selectedIndex == _question.options.length-1){
					trace("Adding field");
					if(this._question.allowOther){
						specifyInput.isEnabled = true;
					}
					
				}else{
					if(this._question.allowOther){
						if(specifyInput.isEnabled){
							trace("Removing field");
							specifyInput.isEnabled = false;
						}
						
					}
				}
			}
			
		}
			
	}
}