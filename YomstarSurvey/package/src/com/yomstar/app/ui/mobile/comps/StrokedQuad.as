package com.yomstar.app.ui.mobile.comps
{
	import starling.display.Quad;
	import starling.display.Sprite;
	
	public class StrokedQuad extends Sprite
	{
		private var _width:Number;
		private var _height:Number;
		private var _bodyQuad:Quad;
		private var _bottomStrokeQuad:Quad;
		private var _topStrokeQuad:Quad;
		public function StrokedQuad(color:Number, strokeColor:Number, strokeTop:uint = 0, strokeBottom:uint = 0)
		{
			super();
			_bodyQuad = new Quad(3, 3, color);
			addChild(_bodyQuad);
			if(strokeBottom > 0){
				_bottomStrokeQuad = new Quad(1, strokeBottom, strokeColor);
				addChild(_bottomStrokeQuad);
			}
			if(strokeTop > 0){
				_topStrokeQuad = new Quad(1, strokeTop, strokeColor);
				addChild(_topStrokeQuad);
			}
			layout();
		}

		override public function get height():Number
		{
			return _height;
		}

		override public function set height(value:Number):void
		{
			_height = value;
			layout();
		}

		override public function get width():Number
		{
			return _width;
		}

		override public function set width(value:Number):void
		{
			_width = value;
			layout();
		}
		
		public function layout():void{
			_bodyQuad.width = this._width;
			_bodyQuad.height = this._height;
			if(_topStrokeQuad){
				_topStrokeQuad.width = this.width;
			}
			if(_bottomStrokeQuad){
				_bottomStrokeQuad.width = this.width;
				_bottomStrokeQuad.y = this.height-_bottomStrokeQuad.height;
			}
		}
	}
}