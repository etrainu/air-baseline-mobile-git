package com.yomstar.app.ui.mobile.comps
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	import com.greensock.easing.Expo;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.ui.mobile.utils.Polygon;
	
	import feathers.controls.Label;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.display.GradientType;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	import starling.display.Graphics;
	import starling.display.Image;
	import starling.display.Shape;
	import starling.display.Sprite;
	import starling.display.graphics.Fill;
	import starling.display.shaders.fragment.TextureVertexColorFragmentShader;
	import starling.extensions.pixelmask.PixelMaskDisplayObject;
	import starling.textures.GradientTexture;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	
	public class PercentGraphCircular extends FeathersControl
	{
		private const VERTICAL_GAP:uint = 10;
		private const HORIZENTAL_GAP:uint = 10;
		private var _image:Image;
		private var graph:PercentGraphCircularLegacyDrawer
		private var _graphColor:Number
		private var _title:String;
		private var _percent:int;
		private var _label:Label;
		private var _percentageText:Label;
		private var _textRendererProperties:Object;
		private var _initialized:Boolean;

		public function PercentGraphCircular(title:String, percent:Number = 0, graphColor:Number = 0xFFFFFF)
		{
			super();
			_initialized = false;
			this._graphColor = graphColor;
			this._title = title;
			this._percent = percent;
		}

		public function get percent():int
		{
			return _percent;
		}

		public function set percent(value:int):void
		{
			this._percent = value;
			if(!this._image){
				this.invalidate(INVALIDATION_FLAG_DATA);
			}else{
				TweenLite.to(this._image, 1, {rotation:45, scaleX:0.1, scaleY:0.1, alpha:0.05, ease:Expo.easeOut, delay:0});
				if(this._percentageText){
					TweenLite.to(this._percentageText, 1, {scaleX:0.1, scaleY:0.1, alpha:0.05, ease:Expo.easeOut, delay:0.5, onComplete:onFadeOut});
				}
				
			}
			
		}
		public function onFadeOut():void{
			this._image.rotation=0;
			this._image.scaleX = this._image.scaleY=1;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		public function fadeGraphIn():void{
			this._percentageText.alpha = 1;
			this._percentageText.scaleX = this._percentageText.scaleY=1;
			TweenLite.from(this._percentageText, 1, {scaleX:0.1, scaleY:0.1, alpha:0.05, ease:Expo.easeOut, delay:0.5});
			this._image.alpha = 1;
			TweenLite.from(this._image, 1, {rotation:-45, scaleX:0.1, scaleY:0.1, alpha:0.05, ease:Expo.easeOut, delay:0});
		}
		public function get textRendererProperties():Object
		{
			if(!_textRendererProperties){
				if(this._label){
					_textRendererProperties = this._label.textRendererProperties;
				}else{
					_textRendererProperties = {};
				}
			}
			return _textRendererProperties;
		}
		
		public function set textRendererProperties(value:Object):void
		{
			_textRendererProperties = value;
			this._label.textRendererProperties = _textRendererProperties;
			this._label.invalidate(INVALIDATION_FLAG_SIZE);
			this._label.invalidate(INVALIDATION_FLAG_STYLES);
			this._percentageText.textRendererProperties = _textRendererProperties;
			this._percentageText.invalidate(INVALIDATION_FLAG_SIZE);
			this._percentageText.invalidate(INVALIDATION_FLAG_STYLES);
			this.invalidate(INVALIDATION_FLAG_SIZE);
			this.invalidate(INVALIDATION_FLAG_STYLES);
		}
		public function get title():String
		{
			return _title;
		}
		
		public function set title(value:String):void
		{
			_title = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		override protected function draw():void
		{
			super.draw();
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			const styleInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STYLES);
			if(!_initialized){
				_initialized = true;
				this._label.textRendererProperties= _textRendererProperties;
				this._label.validate();
				this._percentageText.textRendererProperties= _textRendererProperties;
				this._percentageText.validate();
			}
			if(styleInvalid)
			{
				this.applyStyle();
			}
			if(dataInvalid)
			{
				this.commitData();
			}
			this._label.validate();
			this._percentageText.validate();
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			if(dataInvalid || sizeInvalid || styleInvalid)
			{
				this.layout();
			}
			if(dataInvalid){
				fadeGraphIn();
			}
		}
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				if(this._image){
					newWidth = this._image.width;
				}
				
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
				if(this._image){
					newHeight = this._image.height;
				}
				if(this._label){
					newHeight += this._label.height+VERTICAL_GAP;
				}
			}
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		protected function applyStyle():void
		{
			if(this._label){
				this._label.textRendererProperties= textRendererProperties;
			}
			if(this._percentageText){
				this._percentageText.textRendererProperties= textRendererProperties;
			}
		}
		override protected function initialize():void
		{
			super.initialize();
			this.touchable = false;
			_label = new Label();
			this._label.textRendererProperties.wordWrap = false;
			if(_title){
				this._label.text = _title;
			}
			this._label.textRendererProperties= textRendererProperties;
			addChild(_label);
			
			_percentageText = new Label();
			this._percentageText.textRendererProperties.wordWrap = false;
			this._percentageText.textRendererProperties= textRendererProperties;
			addChild(_percentageText);
			this.percent = _percent;
			
			graph = new PercentGraphCircularLegacyDrawer();
		}
		
		protected function commitData():void{
			if(!this._image){
				this._image = new Image(Texture.fromBitmapData(graph.draw(this._percent, 0x000000, this._graphColor, 105, 20)));
				addChild(this._image);
			}else{
				this._image.texture.dispose();
				this._image.texture= Texture.fromBitmapData(graph.draw(this._percent, 0x000000, this._graphColor, 105, 20));
				this._image.readjustSize();
			}
			this._image.pivotX = this._image.width*0.5;
			this._image.pivotY = this._image.height*0.5;
			this._image.color = this._graphColor;
			addChild(_image);
			if(this._label){
				this._label.text = _title;
			}
			if(this._percentageText){		
				this._percentageText.text = String(this._percent)+"%";
				this._percentageText.validate();
				this._percentageText.pivotX = this._percentageText.width*0.5;
				this._percentageText.pivotY = this._percentageText.height*0.5;
				trace("Percentage is set to: "+this._percentageText.text);
			}
		}
		protected function layout():void{
			
			if(this._label){
				this._label.x = (actualWidth-this._label.width)*0.5;
				this._label.validate();
				if(this._image){
					this._image.x = this._image.pivotX;
					this._image.y = this._label.y + this._label.height+VERTICAL_GAP+(this._image.pivotY);
				}
			}else{
				if(this._image){
					this._image.x = this._image.pivotX;
					this._image.y = this._image.pivotY;
				}
			}
			if(this._percentageText){
				this._percentageText.validate();
				
				if(this._image){
					this._percentageText.x = (this._image.x);
					this._percentageText.y = (this._image.y);
				}else{
					this._percentageText.x = (actualWidth-this._percentageText.width)*0.5;
					this._percentageText.y = actualHeight-this._percentageText.height-40;
				}
				
			}
		}
		
		override public function dispose():void
		{
			if(this._image){
				if(this._image.texture){
					this._image.texture.dispose();
				}
			}
			super.dispose();
		}
		
		
	}
}