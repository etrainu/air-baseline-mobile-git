package com.yomstar.app.ui.mobile.screens
{

	import com.google.zxing.BarcodeFormat;
	import com.google.zxing.DecodeHintType;
	import com.google.zxing.common.flexdatatypes.HashTable;
	import com.logosware.event.QRdecoderEvent;
	import com.logosware.event.QRreaderEvent;
	import com.logosware.utils.QRcode.GetQRimage;
	import com.logosware.utils.QRcode.QRdecode;
	import com.yomstar.app.ui.mobile.utils.GPUCam;
	
	import flash.display.BitmapData;
	import flash.display.StageAspectRatio;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	//import de.patrickkulling.air.mobile.extensions.toast.Toast;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Screen;
	import feathers.controls.TabBar;
	import feathers.data.ListCollection;
	
	import pl.mateuszmackowiak.nativeANE.NativeDialogEvent;
	import pl.mateuszmackowiak.nativeANE.alert.NativeAlert;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.Event;

	public class Qrcode extends Screen
	{
		private var _header:Header;
		private var _tabBar:TabBar;
		private var _backButton:Button;
		private var gpuCam:GPUCam;
		private var firstDraw:Boolean;
		private var getQRimage:GetQRimage;
		private var qrDecode:QRdecode;
		private var qrDEcodeTimer:Timer;
		private var readyForDecode:Boolean;
		private var readSuccess:Number = 0;
		private var qrDecodedDataOld:String;
		
		private var alert:NativeAlert;
		public function Qrcode()
		{
			super();
		}
		override protected function draw():void
		{
			_header.width = actualWidth;
			this._header.width = this.actualWidth;
			this._header.validate();
			
			this._tabBar.width = this.actualWidth;
			this._tabBar.validate();
			this._tabBar.y = this.actualHeight - this._tabBar.height;
			if(firstDraw){
				var transitionCompleteTimer:Timer = new Timer(1500, 1);
				transitionCompleteTimer.addEventListener(TimerEvent.TIMER_COMPLETE, transitionComplete, false, 0, true);
				transitionCompleteTimer.start();
			}
			
			//this._scrollText.width = this.actualWidth;
			//this._scrollText.y = this._header.height;
			//this._scrollText.height = this.actualHeight - this._scrollText.y - this._tabBar.height;
			firstDraw = false;
		}
		
		override protected function initialize():void
		{
			alert = new NativeAlert();
			gpuCam= new GPUCam();
			qrDEcodeTimer = new Timer(2500, 0);
			readyForDecode = true;
			firstDraw = true;
			setupScreen();
			_header = new Header();
			_header.title = "QR Code";
			this._tabBar = new TabBar();
			this._tabBar.dataProvider = new ListCollection(
				[
					{ label: "Read" },
					{ label: "Generate" }
				]);
			this._tabBar.addEventListener(starling.events.Event.CHANGE, tabBar_changeHandler);
			this.addChild(this._tabBar);
			this._backButton = new Button();
			this._backButton.label = "Back";
			this._backButton.addEventListener(starling.events.Event.TRIGGERED, backButton_triggeredHandler);
			this.addChild(this._header);
			this._header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
			this.backButtonHandler = this.onBackButton;
			tabBar_changeHandler(null);
			
		}
		private function transitionComplete(e:TimerEvent):void{
			var transitionCompleteTimer:Timer = e.target as Timer;
			transitionCompleteTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, transitionComplete);
			transitionCompleteTimer.stop();
			transitionCompleteTimer = null;
			setupCamera();
		}
		private function setupScreen():void{
			Starling.current.nativeStage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			Starling.current.nativeStage.autoOrients = false;
			Starling.current.nativeStage.setAspectRatio(StageAspectRatio.LANDSCAPE);
			
		}
		private function revertScreen():void{
			Starling.current.nativeStage.displayState = StageDisplayState.NORMAL;
			Starling.current.nativeStage.setAspectRatio(StageAspectRatio.ANY);
			Starling.current.nativeStage.autoOrients = true;
		}
		private function setupCamera():void{
			//Starling.current.stage3D.visible = false;
			//Starling.current.stage.touchable = false;
			Starling.current.shareContext = true;
			trace(this._dpiScale);
			this._dpiScale = 1;
			//Starling.current.stage.visible = false;
			//Starling.current.stage.touchable = false;
			gpuCam.start(Starling.current.nativeStage);
			getQRimage = new GetQRimage(gpuCam.stageW, gpuCam.stageH);
			
			// run onQrImageReadComplete when the QR Code is found 
			getQRimage.addEventListener(QRreaderEvent.QR_IMAGE_READ_COMPLETE, onQrImageReadComplete);
			getQRimage.addEventListener(flash.events.Event.CLOSE, onQrImageReadClosed);
			
			// setup QRdecoder object
			qrDecode = new QRdecode();
			qrDecode.addEventListener(flash.events.Event.CLOSE, onQrImageReadClosed);
			// invoked when QR Code is found  
			qrDecode.addEventListener(QRdecoderEvent.QR_DECODE_COMPLETE, onQrDecodeComplete);
			
			qrDEcodeTimer.addEventListener(TimerEvent.TIMER, attemptDecode, false, 0, true);
			qrDEcodeTimer.start();
		}
		private function attemptDecode(e:TimerEvent):void{
			if(readyForDecode){
				trace("attemptDecode");
				//alert.message = "attemptDecode";
				//alert.closeLabel = "OK";
				//alert.title = "Notice";
				//alert.closeHandler = onDeleteMenuResult;
				//alert.show();
				var bmd:BitmapData = gpuCam.snapshot();
				if(bmd){
					trace("valid bd");
					readyForDecode = false;
					getQRimage.process(bmd);
					
					
				}else{
					trace("invalid bd");
				}
				
				
				//System.gc();
			}
			
		}
		private function onQrImageReadComplete(e:QRreaderEvent):void
		{	
			trace("found qr");
			qrDecode.setQR(e.data);
			qrDecode.startDecode();		
		}
		private function onQrImageReadClosed(e:flash.events.Event):void
		{	
			//trace("no qr detected");
			readyForDecode = true;	
		}
		
		private function onQrDecodeComplete(e:QRdecoderEvent):void
		{	// e.data is the string decoded from QR Code  
			alert.message = "decoded: "+e.data;
			alert.closeLabel = "OK";
			alert.title = "Notice";
			alert.closeHandler = onDeleteMenuResult;
			alert.show();
			trace("decoded: "+e.data);
			var decodedDataNew:String = e.data;
			var compare = decodedDataNew.localeCompare(qrDecodedDataOld);
			
			if(compare==0 && readSuccess == 0)
			{
				trace("decoded: "+e.data);
			}
			else
			{ trace("\t ERROR \n");
			}
			
			qrDecodedDataOld = decodedDataNew;
			readyForDecode = true;
		}
		protected function onDeleteMenuResult(event:NativeDialogEvent):void{
			//
			//if(event.index == "0"){
				
			//}
		}

		public function getAllHints():HashTable
		{
			var ht:HashTable=new HashTable;
			ht.Add(DecodeHintType.POSSIBLE_FORMATS, BarcodeFormat.QR_CODE);
			return ht;
		}
		private function onBackButton():void
		{
			qrDEcodeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, transitionComplete);
			qrDEcodeTimer.stop();
			qrDEcodeTimer = null;
			gpuCam.stop();
			revertScreen();
			Starling.current.shareContext = false;
			qrDEcodeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, attemptDecode);
			qrDEcodeTimer.stop();
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function backButton_triggeredHandler(event:starling.events.Event):void
		{
			this.onBackButton();
		}
		private function tabBar_changeHandler(event:starling.events.Event):void
		{
			trace("selectedIndex: " + this._tabBar.selectedIndex.toString());
			switch(this._tabBar.selectedIndex){
				case 0:
					break
				case 1:
					break
			}
			this.invalidate();
		}
	}
}