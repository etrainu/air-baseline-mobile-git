package com.yomstar.app.ui.mobile.renderers.ResponseCells
{
	import com.yomstar.app.config.ResponseType;
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.dataObjects.MediaResponseData;
	import com.yomstar.app.dataObjects.Participant;
	import com.yomstar.app.dataObjects.QuestionData;
	import com.yomstar.app.dataObjects.ResponseCaptureData;
	import com.yomstar.app.dataObjects.ResponseData;
	import com.yomstar.app.dataObjects.Reward;
	import com.yomstar.app.dataObjects.RewardScheme;
	import com.yomstar.app.dataObjects.SampleDetail;
	import com.yomstar.app.events.ImageLoaderEvent;
	import com.yomstar.app.services.AvatarImageLoader;
	import com.yomstar.app.services.ParticipantDetails;
	import com.yomstar.app.services.RewardDetails;
	import com.yomstar.app.services.RewardSchemes;
	import com.yomstar.app.services.SampleDetails;
	import com.yomstar.app.services.StaticMapManager;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.comps.BorderedImage;
	import com.yomstar.app.ui.mobile.comps.BottomShadowedLine;
	import com.yomstar.app.ui.mobile.comps.FeathersImage;
	import com.yomstar.app.ui.mobile.comps.IconnedLabel;
	import com.yomstar.app.ui.mobile.comps.StrokedQuad;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.controls.Label;
	import feathers.core.FeathersControl;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.geom.Orientation3D;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import org.osmf.events.TimeEvent;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class MediaResponseCell extends FeathersControl
	{
		private const INVALIDATION_FLAG_MEDIA:String = "mediaInvalid";
		private const NOT_AVAILABLE_TEXT:String = "NA";
		private const TEXTLINE_VERTICAL_GAP:uint = 5;
		private const VERTICAL_GAP:uint = 15;
		private const HORIZENTAL_GAP:uint = 15;
		private const PADDING_TOP:uint = 10;
		private const PADDING_RIGHT:uint = 10;
		private const INNER_PADDING_RIGHT:uint = 30;
		private const INNER_PADDING_LEFT:uint = 30;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 10;
		private const MAP_WIDTH:uint = 115;
		private const MAP_HEIGHT:uint = 100;
		private const AVATAR_WIDTH:uint = 60;
		private const AVATAR_HEIGHT:uint = 60;
		private const MAX_INNERTEXT_CHARS:uint = 24;
		private const COMPARTMENT_BG_COLOR:Number = 0x0f1214;
		private const STROKE_COLOR:Number = 0x060708;
		
		private const INVALIDATION_FLAG_AVATAR:String = "avatarInvalid";
		private var _isLayedOut:Boolean;
		private var _initialized:Boolean;
		private var _questionText:Label;
		private var _media:BorderedImage;
		private var _questionTypeIcon:Image;
		private var _loadedMediaTexture:Texture;
		

		private var _questionData:QuestionData;
		private var _responseData:MediaResponseData;


		private var _questionBG:StrokedQuad;

		public function MediaResponseCell()
		{
			super();
			_initialized = false;
			_isLayedOut = false;
		}	

		public function get responseData():ResponseData
		{
			return _responseData;
		}

		public function set responseData(value:ResponseData):void
		{
			trace("this._responseData: "+this._responseData);
			if(this._responseData){
				if(this._responseData.type == ResponseType.MEDIA_QUESTION_RESPONSE){
					_responseData = MediaResponseData(value);
				}
			}
			
		}

		public function get questionData():QuestionData
		{
			return _questionData;
		}

		public function set questionData(value:QuestionData):void
		{
			_questionData = value;
		}

		override protected function draw():void
		{
			super.draw();
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			const avatarInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_AVATAR);
			const mediaInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_MEDIA);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			
			if(!_initialized){
				_initialized = true;
				this._questionText.textRendererProperties = {wordWrap:true , isHTML:false, textFormat:TextFormats.getTextFormat(TextFormats.DEFAULT_TEAXT_FORMAT_BOLD_HLT1)};
			}
			if(dataInvalid)
			{
				this.commitData();
			}
			if(mediaInvalid)
			{
				this.commitMedia();
			}
			this._questionText.validate();
			this._media.validate();
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			if(dataInvalid || sizeInvalid || avatarInvalid)
			{
				this.layout();
			}
			if(sizeInvalid && !dataInvalid){
				if(this.parent is FeathersControl){
					FeathersControl(this.parent).invalidate(FeathersControl.INVALIDATION_FLAG_SIZE);
				}
			}
		}
		protected function layout():void{
			this._questionBG.y = 0;
			this._questionBG.width = this.actualWidth;
			
			this._questionText.x = this._questionTypeIcon.width+(HORIZENTAL_GAP*2);
			this._questionText.width = this.actualWidth-(2*this._questionText.x);
			this._questionText.validate();
			this._questionBG.height = Math.max(this._questionTypeIcon.height, this._questionText.height)+(2*VERTICAL_GAP);
			this._questionText.y = (this._questionBG.height-this._questionText.height)*0.5;
			this._questionTypeIcon.x = PADDING_LEFT;
			this._questionTypeIcon.y = (this._questionBG.height-this._questionTypeIcon.height)*0.5;
			this._media.x = (this.actualWidth-this._media.width)*0.5;
			this._media.y = _questionBG.y+_questionBG.height+VERTICAL_GAP;
			_isLayedOut = true;
		}
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			
			this._questionText.width = NaN;
			this._questionText.height = NaN;
			this._questionText.validate();
			
			
			
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				if(this._questionBG){
					newWidth = _questionBG.width;
				}
				
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
				if(this._questionBG){
					if(_isLayedOut){
						newHeight = 200;
					}else{
						newHeight = 200;
					}
				}
			}
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		protected function commitMedia():void
		{
			if(_loadedMediaTexture){
				this._media.texture = _loadedMediaTexture;
			}
		}
		protected function commitData():void
		{
			this.reset();
			if(this._questionData){
				this._questionText.text = _questionData.questionText;
			}else{
				this._questionText.text = this.NOT_AVAILABLE_TEXT;
			}
			if(this._responseData){
				trace("this._responseData.mediaCaptureHost+this._responseData.mediaCaptureURI: :"+this._responseData.mediaCaptureHost+this._responseData.mediaCaptureURI);
				if(this._responseData.mediaCaptureHost && this._responseData.mediaCaptureURI){
					getMedia(this._responseData.mediaCaptureHost+this._responseData.mediaCaptureURI);
				}
			}
			this._media.texture = R.getAtlas().getTexture("locationnotavailable");
		}
		
		override protected function initialize():void
		{
			super.initialize();
			if(!this._media){
				this._media = new BorderedImage(null, 3, GlobalAppValues.FEATURED_COLOR_TWO, MAP_WIDTH, MAP_HEIGHT);
				this._media.touchable = false;
			}
			if(!this._questionTypeIcon){
				this._questionTypeIcon = new Image(R.getAtlas(0.5).getTexture("question_image_capture"));
				this._questionTypeIcon.touchable = false;
			}
			if(this._questionText){
				this._questionText.text = "";
			}else{
				this._questionText = new Label();
				this._questionText.touchable = false;
			}
			if(!_questionBG){
				this._questionBG = new StrokedQuad(COMPARTMENT_BG_COLOR, STROKE_COLOR, 1, 1);
				this._questionBG.touchable = false;
			}
			addChild(this._questionBG);
			reset();
			this.addChild(this._media);
			this.addChild(this._questionText);
			this.addChild(this._questionTypeIcon);
			this.invalidate();
		}
		public function reset():void{
			
			this._questionText.text = "";
			_isLayedOut = false;
			if(this._media){
				if(this._media.texture){
					this._media.texture.dispose();
				}
			}
			if(this._questionTypeIcon){
				if(this._questionTypeIcon.texture){
					this._questionTypeIcon.texture.dispose();
				}
			}
		}
		override public function dispose():void
		{
			// TODO Auto Generated method stub
			super.dispose();
			if(this._media){
				if(this._media.texture){
					this._media.texture.dispose();
				}
			}
			if(this._questionTypeIcon){
				if(this._questionTypeIcon.texture){
					this._questionTypeIcon.texture.dispose();
				}
			}
		}
		public function getMedia(mediaURL:String):void{
			var mediaLoader:Loader = Loader(Main.loaderPool.getObj());
			mediaLoader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, onMediaLoaded);
			mediaLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onMediaLoadError);
			var req:URLRequest = URLRequest(Main.urlRequestPool.getObj());
			req.requestHeaders = null;
			req.data = null;
			req.url= mediaURL;
			mediaLoader.load(req);
			Main.urlRequestPool.returnObj(req);
		}
		protected function onMediaLoaded(event:flash.events.Event):void
		{
			
			trace("onMediaLoaded");
			var mediaLoader:Loader = LoaderInfo(event.currentTarget).loader;
			mediaLoader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, onMediaLoaded);
			mediaLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onMediaLoadError);
			Main.loaderPool.returnObj(mediaLoader);
			if(this._loadedMediaTexture){
				this._loadedMediaTexture.dispose();
			}
			this._loadedMediaTexture = Texture.fromBitmap(event.currentTarget.loader.content as Bitmap);
			this.invalidate(INVALIDATION_FLAG_MEDIA);
		}
		protected function onMediaLoadError(event:IOErrorEvent):void
		{
			trace("onMediaLoadError");
			var mediaLoader:Loader = LoaderInfo(event.currentTarget).loader;
			mediaLoader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, onMediaLoaded);
			mediaLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onMediaLoadError);
			Main.loaderPool.returnObj(mediaLoader);
		}
	}
}