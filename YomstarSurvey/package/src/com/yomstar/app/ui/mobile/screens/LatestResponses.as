package com.yomstar.app.ui.mobile.screens
{
	import com.yomstar.app.config.SurveySorting;
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.events.ImageLoaderEvent;
	import com.yomstar.app.services.AccountDetails;
	import com.yomstar.app.services.AccountStats;
	import com.yomstar.app.services.AvatarImageLoader;
	import com.yomstar.app.services.PlansDetails;
	import com.yomstar.app.services.SurveyResponses;
	import com.yomstar.app.services.UserInfo;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.comps.BorderedImage;
	import com.yomstar.app.ui.mobile.comps.IconnedLabel;
	import com.yomstar.app.ui.mobile.comps.LightPad;
	import com.yomstar.app.ui.mobile.comps.ListDetailsCalloutButton;
	import com.yomstar.app.ui.mobile.comps.ListDetailsCalloutContent;
	import com.yomstar.app.ui.mobile.comps.Spacer;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.DateFormat;
	import com.yomstar.app.utils.ObjectDigger;
	
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollText;
	import feathers.controls.Scroller;
	import feathers.controls.TabBar;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.ITextRenderer;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	import feathers.skins.StandardIcons;
	
	import flash.events.ErrorEvent;
	import flash.events.IOErrorEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import pl.mateuszmackowiak.nativeANE.alert.NativeAlert;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;

	public class LatestResponses extends YomScreen
	{
		private const VERTICAL_GAP:uint = 10;
		private const PADDING_TOP:uint = 20;
		private const PADDING_RIGHT:uint = 20;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 20;
		private const CALLOUT_WIDTH:uint = 350;
		private const CALLOUT_HEIGHT:uint = 500;
		
		
		private var _dataLoaded:Boolean;

		private var _list:List;
		private var _listdb:ListCollection;
		private var _callout:Callout;
		public function LatestResponses()
		{
			super();
		}
		override protected function draw():void
		{
			super.draw();
			_list.y = this.header.height;
			_list.width = this.actualWidth;
			_list.height = (this.actualHeight-this.header.height);
		}
		
		
		override protected function initialize():void
		{
			super.initialize();
			_list = new List();
			this._list.addEventListener(starling.events.Event.CHANGE, listChanged);
			_dataLoaded = false;
			
			
			this.backButtonHandler = this.onBackButton;
						
			this.addChild(this._list);
		}
		
		override protected function run():void{
			super.run();
			busyIndicator.start();
			getLatestResponses();
		}




		private function getLatestResponses():void{
			var latestResponses:SurveyResponses = new SurveyResponses();
			latestResponses.addEventListener(flash.events.Event.COMPLETE, onLatestResponses, false, 0, true);
			latestResponses.addEventListener(flash.events.ErrorEvent.ERROR, onLatestResponsesError, false, 0, true);
			latestResponses.getResponses(Main.data.token, 0, 10, SurveySorting.DESCENDING);
		}
		private function onLatestResponsesError(event:flash.events.ErrorEvent):void
		{
			trace("onLatestResponsesError: "+event.text);
			var latestResponses:SurveyResponses = event.currentTarget as SurveyResponses;
			latestResponses.removeEventListener(flash.events.Event.COMPLETE, onLatestResponses);
			latestResponses.removeEventListener(flash.events.ErrorEvent.ERROR, onLatestResponsesError);
			if(alert){
				alert.message = event.text;
				alert.closeLabel = "OK";
				alert.title = "Error";
				alert.show();
			}
		}
		private function onLatestResponses(event:flash.events.Event):void
		{
			var latestResponses:SurveyResponses = event.currentTarget as SurveyResponses;
			latestResponses.removeEventListener(flash.events.Event.COMPLETE, onLatestResponses);
			latestResponses.removeEventListener(flash.events.ErrorEvent.ERROR, onLatestResponsesError);
			//sample result [{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"woluGR1hSlOJoGN3pEi9Ng","version":1,"latitude":-27.470933,"longitude":153.02350199999998,"averagePercentageScore":54,"isGeolocated":true,"dateCaptured":"2012-12-11T16:57:14","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":75,"responses":["option 2"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"CRHHD3pRy6zeZ1pxFhebA","version":1,"averagePercentageScore":41,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:37","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":50,"responses":["option 3"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"HbcyoJTgqOuoKwn4FOg","version":1,"averagePercentageScore":12,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:45","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":0,"responses":["option 1"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":25,"responses":["option 4"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"LeDRxjFQcqQKLQKT4tJVw","version":1,"averagePercentageScore":58,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:50","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":50,"responses":["option 3"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":50,"responses":["option 3"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"lkc5f39QAm1C4pFXELO4Q","version":1,"averagePercentageScore":100,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:56","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":75,"responses":["option 4"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":100,"responses":["option 1"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"jkKlDhWxTcuTKX5Dp4M2pg","version":1,"averagePercentageScore":66,"isGeolocated":false,"dateCaptured":"2012-12-11T16:59:01","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":100,"responses":["option 1"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"6tePcDq3RNO1CwCtd0zCw","version":1,"averagePercentageScore":41,"isGeolocated":false,"dateCaptured":"2012-12-11T16:59:07","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":50,"responses":["option 3"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"iKQfUMHKRc6KdViQU02M1A","version":1,"averagePercentageScore":66,"isGeolocated":false,"dateCaptured":"2012-12-11T16:59:17","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":100,"responses":["option 1"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"SXWOaJPoRsevJJqL7pPww","version":1,"latitude":-27.470933,"longitude":153.02350199999998,"averagePercentageScore":70,"isGeolocated":true,"dateCaptured":"2012-12-11T16:59:34","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":50,"responses":["option 3"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":75,"responses":["option 2"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"A4SuNuCIRh6ezMNQDE58Lw","version":1,"latitude":-27.470933,"longitude":153.02350199999998,"averagePercentageScore":62,"isGeolocated":true,"dateCaptured":"2012-12-11T16:59:51","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":75,"responses":["option 4"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":25,"responses":["option 4"]}]}]
			//trace("Latest Response: "+ObjectDeigger.digg(SurveyResponses.surveyResponses[0]));
			displayResponses();
			busyIndicator.stop();
			draw();
		}
		
		private function displayResponses():void{//http://feathersui.com/documentation/feathers/controls/renderers/BaseDefaultItemRenderer.html
			_listdb = new ListCollection(SurveyResponses.surveyResponses.samples);
			_list.itemRendererProperties.iconTextureFunction = responseIconFunction;
			_list.itemRendererProperties.labelFunction = responseLabelFunction;
			_list.itemRendererProperties.accessoryFunction  = listItemAccessoryFunction;
			_list.dataProvider = _listdb;
		}
		private function responseIconFunction(o:Object):Texture{
			var moodTexture:Texture;
			switch(Math.floor(o.averagePercentageScore/20)){
				case 0:
					moodTexture =  R.getAtlas().getTexture("Mood1");
					break
				case 1:
					moodTexture =  R.getAtlas().getTexture("Mood2");
					break
				case 2:
					moodTexture =  R.getAtlas().getTexture("Mood3");
					break
				case 3:
					moodTexture =  R.getAtlas().getTexture("Mood4");
					break
				case 4:
					moodTexture =  R.getAtlas().getTexture("Mood5");
					break
				default:
					moodTexture =  R.getAtlas().getTexture("Mood1");
			}
			return moodTexture
		}
		private function listItemAccessoryFunction(o:Object):DisplayObject{
			var accessoryBtn:ListDetailsCalloutButton = new ListDetailsCalloutButton();
			accessoryBtn.label = "Details";
			accessoryBtn.calloutStr = "Survey: "+o.baselineName;
			accessoryBtn.calloutStr = accessoryBtn.calloutStr.concat("\nAverage Percentage Score: "+o.averagePercentageScore);
			accessoryBtn.calloutStr = accessoryBtn.calloutStr.concat("\nDate Captured: "+o.dateCaptured);
			if(o.isGeolocated){
				var mapDimentions:uint = CALLOUT_WIDTH-PADDING_LEFT - PADDING_RIGHT;
				var geoData:GeoData = new GeoData();
				geoData.init(o.latitude, o.longitude, GeoData.TILE_FORMAT_PNG, 5, mapDimentions, mapDimentions);
				accessoryBtn.geoData = geoData;
			}
			accessoryBtn.addEventListener(Event.TRIGGERED, onAccessoryTriggered);
			//map http://dev.virtualearth.net/REST/v1/Imagery/Map/AerialWithLabels/?mapSize=400,400&zoomLevel=5&pushpin=-27.470933,153.02350199999998&format=png&key=AiiU246TJT-STd8PI3UNOZUQUfmT5C1NRfzkum0gTLL6r6b86IJWZH0o6iKmQtd5
			return accessoryBtn;
		}
		private function onAccessoryTriggered(e:Event):void{
			var accessoryBtn:ListDetailsCalloutButton = ListDetailsCalloutButton(e.currentTarget);
			var cWidth:uint = CALLOUT_WIDTH;
			var cHeight:uint = CALLOUT_HEIGHT;
			var listDetails:ListDetailsCalloutContent = new ListDetailsCalloutContent();
			listDetails.init(accessoryBtn.calloutStr, accessoryBtn.geoData, cWidth, cHeight);
			listDetails.addEventListener(starling.events.Event.REMOVED_FROM_STAGE, onCalloutClosed);
			listDetails.addEventListener("compResized", onCalloutContentResized);
			listDetails.orig = accessoryBtn;
			_callout = Callout.show(listDetails, listDetails.orig, Callout.DIRECTION_ANY);
			
		}
		private function onCalloutContentResized(e:starling.events.Event):void{
			trace("onCalloutContentResized");
			if(_callout){
				var listDetails:ListDetailsCalloutContent = ListDetailsCalloutContent(e.currentTarget);
				listDetails.disposable = false;
				_callout.close(false);
				_callout = Callout.show(listDetails, listDetails.orig, Callout.DIRECTION_ANY);
				_callout.validate();
				listDetails.disposable = true;
			}
		}
		private function onCalloutClosed(e:starling.events.Event):void{
			var listDetails:ListDetailsCalloutContent = ListDetailsCalloutContent(e.currentTarget);
			if(listDetails.disposable){
				listDetails.removeEventListeners();
				listDetails.dispose();
				_callout.dispose();
				trace("listDetails object disposed");
			}
		}
		private function responseLabelFunction(o:Object):String{
			return o.baselineName+"\n"+String(o.dateCaptured).replace("T", " at ");
		}
		private function listChanged(e:starling.events.Event):void
		{
			dispatchEventWith("sampleSelected", false, this._list.selectedItem);
		}
		private function onBackButton():void
		{
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
	}
}