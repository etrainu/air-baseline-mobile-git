package com.yomstar.app.ui.mobile.renderers.ResponseCells
{
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.dataObjects.Participant;
	import com.yomstar.app.dataObjects.ResponseCaptureData;
	import com.yomstar.app.dataObjects.Reward;
	import com.yomstar.app.dataObjects.RewardScheme;
	import com.yomstar.app.dataObjects.SampleDetail;
	import com.yomstar.app.events.ImageLoaderEvent;
	import com.yomstar.app.services.AvatarImageLoader;
	import com.yomstar.app.services.ParticipantDetails;
	import com.yomstar.app.services.RewardDetails;
	import com.yomstar.app.services.RewardSchemes;
	import com.yomstar.app.services.SampleDetails;
	import com.yomstar.app.services.StaticMapManager;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.comps.BorderedImage;
	import com.yomstar.app.ui.mobile.comps.BottomShadowedLine;
	import com.yomstar.app.ui.mobile.comps.FeathersImage;
	import com.yomstar.app.ui.mobile.comps.IconnedLabel;
	import com.yomstar.app.ui.mobile.comps.StrokedQuad;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.controls.Label;
	import feathers.core.FeathersControl;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.geom.Orientation3D;
	import flash.utils.Timer;
	
	import org.osmf.events.TimeEvent;
	
	import starling.display.DisplayObject;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class ResponseCaptureCell extends FeathersControl
	{
		private const NOT_AVAILABLE_TEXT:String = "NA";
		private const TEXTLINE_VERTICAL_GAP:uint = 5;
		private const VERTICAL_GAP:uint = 15;
		private const HORIZENTAL_GAP:uint = 15;
		private const PADDING_TOP:uint = 10;
		private const PADDING_RIGHT:uint = 10;
		private const INNER_PADDING_RIGHT:uint = 30;
		private const INNER_PADDING_LEFT:uint = 30;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 10;
		private const MAP_WIDTH:uint = 115;
		private const MAP_HEIGHT:uint = 100;
		private const AVATAR_WIDTH:uint = 60;
		private const AVATAR_HEIGHT:uint = 60;
		private const MAX_INNERTEXT_CHARS:uint = 24;
		private const COMPARTMENT_BG_COLOR:Number = 0x0f1214;
		private const STROKE_COLOR:Number = 0x060708;
		
		private const INVALIDATION_FLAG_AVATAR:String = "avatarInvalid";
		private var _isLayedOut:Boolean;
		private var _initialized:Boolean;
		private var _map:BorderedImage;
		private var _avatar:BorderedImage;
		private var _staticMapManager:StaticMapManager;
		private var _avatarLoader:AvatarImageLoader
		private var _emailTxt:Label;
		private var _phoneTxt:Label;
		private var _captureData:ResponseCaptureData;
		private var _responsesText:IconnedLabel;
		private var _resultsText:IconnedLabel;
		private var _captureDateText:Label;
		private var _respondantBG:Quad;
		private var _seperator:BottomShadowedLine;
		private var _buttomSeperator:BottomShadowedLine;
		private var _captureDateBG:StrokedQuad;
		private var _respondatnEmail:String;
		private var _respondantPhone:String;
		public function ResponseCaptureCell()
		{
			super();
			_initialized = false;
			_isLayedOut = false;
		}
		public function get captureData():ResponseCaptureData
		{
			return this._captureData;
		}

		public function set captureData(value:ResponseCaptureData):void
		{
			if(value != this._captureData){
				this._captureData = value;
				this.invalidate(INVALIDATION_FLAG_DATA);
			}
		}

		
		
		override protected function draw():void
		{
			super.draw();
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			const avatarInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_AVATAR);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			
			if(!_initialized){
				_initialized = true;
				this._emailTxt.textRendererProperties = {wordWrap:false , isHTML:false, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._phoneTxt.textRendererProperties = {wordWrap:false , isHTML:false, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._responsesText.textRendererProperties = {wordWrap:true , isHTML:false, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._resultsText.textRendererProperties = {wordWrap:true , isHTML:false, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._captureDateText.textRendererProperties = {wordWrap:true , isHTML:false, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
			}
			if(dataInvalid)
			{
				this.commitData();
			}
			if(avatarInvalid){
				this.commitAvatar();
			}
			this._emailTxt.validate();
			this._phoneTxt.validate();
			this._avatar.validate();
			this._map.validate();
			this._responsesText.validate();
			this._resultsText.validate();
			this._captureDateText.validate();
			this._seperator.validate();
			this._buttomSeperator.validate();
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			if(dataInvalid || sizeInvalid || avatarInvalid)
			{
				this.layout();
			}
			if(sizeInvalid && !dataInvalid){
				if(this.parent is FeathersControl){
					FeathersControl(this.parent).invalidate(FeathersControl.INVALIDATION_FLAG_SIZE);
				}
			}
		}
		protected function layout():void{
			this._responsesText.x = PADDING_LEFT;
			this._responsesText.width = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			this._responsesText.validate();
			this._resultsText.x = PADDING_LEFT;
			this._resultsText.width = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			this._resultsText.validate();
			this._respondantBG.width = this.actualWidth;
			this._respondantBG.height = this._responsesText.height*2;
			this._responsesText.y = 0.5*(_respondantBG.height-_responsesText.height);
			
			this._seperator.width = this.actualWidth;
			this._seperator.y = _respondantBG.y+_respondantBG.height;
			this._map.x = this.actualWidth- PADDING_RIGHT-this._map.width;
			this._map.y = this._seperator.y+this._seperator.height+(VERTICAL_GAP);
			this._captureDateBG.width = this.actualWidth;
			this._captureDateBG.height = this._resultsText.height*2+this._buttomSeperator.height;
			this._captureDateBG.y = this._map.y+this._map.height+VERTICAL_GAP;
			this._resultsText.y = _captureDateBG.y+(0.5*(_captureDateBG.height-_resultsText.height-this._buttomSeperator.height));
			this._captureDateText.y = _captureDateBG.y+(0.5*(_captureDateBG.height-_captureDateText.height-this._buttomSeperator.height));
			this._captureDateText.x = actualWidth-INNER_PADDING_RIGHT-this._captureDateText.width;
			this._buttomSeperator.width = actualWidth;
			this._buttomSeperator.y = _captureDateBG.y+_captureDateBG.height-this._buttomSeperator.height;
			var leftSide:uint;
			if(this._avatar){
				this._avatar.x = INNER_PADDING_LEFT;
				this._avatar.y = this._map.y+((this._map.height-this._avatar.height)*0.5);
				leftSide = this._avatar.x+this._avatar.width+HORIZENTAL_GAP;
			}else{
				leftSide = PADDING_LEFT;
			}
			var infoRealestate:int = this._map.x-leftSide-HORIZENTAL_GAP;
			this._emailTxt.x = leftSide;
			this._emailTxt.y = this._map.y+((this._map.height-this._emailTxt.height-this._phoneTxt.height)*0.5);
			this._emailTxt.text = "Email: "+_respondatnEmail;
			this._emailTxt.validate();
			if(this._emailTxt.width > infoRealestate){
				if(String("Email: "+_respondatnEmail).length>MAX_INNERTEXT_CHARS){
					this._emailTxt.text = this._emailTxt.text.slice(0, (this._emailTxt.text.length-(Math.ceil(Number((this._emailTxt.width-infoRealestate)/(this._emailTxt.textRendererProperties.textFormat.size*0.5))))-3))+"...";
				}
				this._emailTxt.validate();
			}
			
			this._phoneTxt.x = leftSide;
			this._phoneTxt.y = this._emailTxt.y+this._emailTxt.height;
			this._phoneTxt.text = "Phone: "+_respondantPhone;
			this._phoneTxt.validate();
			if(this._phoneTxt.width > infoRealestate){
				if(String("Phone: "+_respondantPhone).length>MAX_INNERTEXT_CHARS){
					this._phoneTxt.text = this._phoneTxt.text.slice(0, (this._phoneTxt.text.length-(Math.ceil(Number((this._phoneTxt.width-infoRealestate)/(this._phoneTxt.textRendererProperties.textFormat.size*0.5))))-3))+"...";
				}
				this._phoneTxt.validate();
			}
			_isLayedOut = true;
			
		}
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			
			this._emailTxt.width = NaN;
			this._emailTxt.height = NaN;
			this._emailTxt.validate();
			
			this._phoneTxt.width = NaN;
			this._phoneTxt.height = NaN;
			this._phoneTxt.validate();
			
			this._buttomSeperator.validate();
			
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				if(this._seperator){
					newWidth = _seperator.width;
				}
				
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
				if(this._respondantBG){
					if(_isLayedOut){
						newHeight = this._buttomSeperator.y+this._buttomSeperator.height;
					}else{
						newHeight = _respondantBG.height+this._seperator.height+VERTICAL_GAP+this._map.height+VERTICAL_GAP+this._captureDateBG.height+this._buttomSeperator.height;
					}
				}
			}
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		protected function commitData():void
		{
			this.reset();
			_respondatnEmail = "nomail@etrainu.com";
			this._phoneTxt.text = "Phone: "+this.NOT_AVAILABLE_TEXT;
			if(this._captureData){
				_respondatnEmail = this._captureData.respondantEmail;
				_respondantPhone = this._captureData.respondantPhone;
				this._responsesText.text = "Respondent: "+this._captureData.respondantFullName;
				this._emailTxt.text = "Email: "+_respondatnEmail;
				this._phoneTxt.text = "Phone: "+_respondantPhone;
				this._captureDateText.text = "Captured: "+this._captureData.captureDate;
			}else{
				this._responsesText.text = "Respondent: "+this.NOT_AVAILABLE_TEXT;
				this._emailTxt.text = "Email: "+this.NOT_AVAILABLE_TEXT;
				this._phoneTxt.text = "Phone: "+this.NOT_AVAILABLE_TEXT;
			}
			
			if(!_avatarLoader){
				_avatarLoader = new AvatarImageLoader();
			}
			_avatarLoader.addEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
			_avatarLoader.addEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
			_avatarLoader.getAvatar(_respondatnEmail, AVATAR_WIDTH);
			if(this._captureData.getoData){
				this._map.texture = R.getAtlas().getTexture("locationloading");
				var geoData:GeoData = new GeoData();
				geoData.init(this._captureData.getoData.latitude, this._captureData.getoData.longitude, GeoData.TILE_FORMAT_PNG, 5, this._map.texture.width, this._map.texture.height);
				if(!_staticMapManager){
					_staticMapManager = new StaticMapManager();
				}
				
				_staticMapManager.addEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onMapRecieved, false, 0, true);
				_staticMapManager.getMapTile(geoData);
			}else{
				this._map.texture = R.getAtlas().getTexture("locationnotavailable");
			}
		}
		protected function commitAvatar():void{
			if(this._avatar){
				this.addChild(this._avatar);
			}
		}
		private function removeMapListeners():void{
			if(_staticMapManager){
				if(_staticMapManager.hasEventListener(ImageLoaderEvent.IMAGE_RECIEVED)){
					_staticMapManager.removeEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onMapRecieved);
				}
			}
		}
		private function onMapRecieved(e:ImageLoaderEvent):void{
			trace("onMapRecieved");
			this._map.texture.dispose();
			removeMapListeners();
			this._map.texture = e.imageTexture;
		}
		private function removeAvatarListeners():void{
			if(this._avatarLoader){
				if(this._avatarLoader.hasEventListener(ImageLoaderEvent.IMAGE_RECIEVED)){
					_avatarLoader.removeEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
				}
				if(this._avatarLoader.hasEventListener(IOErrorEvent.IO_ERROR)){
					_avatarLoader.removeEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
				}
			}
		}
		private function onAvatarLoadError(e:IOErrorEvent):void{
			removeAvatarListeners();
			trace("Avatar load error: "+e.text);
		}
		private function onAvatarLoaded(e:ImageLoaderEvent):void{
			trace("onAvatarRecieved");
			removeAvatarListeners();
			this._avatar.texture= e.imageTexture;
			this.invalidate(INVALIDATION_FLAG_AVATAR);
		}
		override protected function initialize():void
		{
			super.initialize();
			if(!this._map){
				this._map = new BorderedImage(null, 3, GlobalAppValues.FEATURED_COLOR_TWO, MAP_WIDTH, MAP_HEIGHT);
				this._map.touchable = false;
			}
			if(!_avatar){
				this._avatar = new BorderedImage(null, 3, GlobalAppValues.FEATURED_COLOR_TWO, AVATAR_WIDTH, AVATAR_HEIGHT);
				this._avatar.touchable = false;
			}
			if(this._emailTxt){
				this._emailTxt.text = "";
			}else{
				this._emailTxt = new Label();
				this._emailTxt.touchable = false;
			}
			if(this._phoneTxt){
				this._phoneTxt.text = "";
			}else{
				this._phoneTxt = new Label();
				this._phoneTxt.touchable = false;
			}
			if(!this._respondantBG){
				this._respondantBG = new Quad(10, 10, COMPARTMENT_BG_COLOR);
			}
			if(!this._seperator){
				this._seperator = new BottomShadowedLine();
				this._seperator.touchable = false;
			}
			if(!this._buttomSeperator){
				this._buttomSeperator = new BottomShadowedLine();
				this._buttomSeperator.touchable = false;
			}
			if(!_captureDateBG){
				this._captureDateBG = new StrokedQuad(COMPARTMENT_BG_COLOR, STROKE_COLOR, 1, 0);
				this._captureDateBG.touchable = false;
			}
			addChild(this._respondantBG);
			addChild(this._seperator);
			addChild(this._captureDateBG);
			addChild(this._buttomSeperator);
			if(!this._responsesText){
				this._responsesText = new IconnedLabel();
				this._responsesText.touchable = false;
			}
			this.addChild(this._responsesText);
			this._responsesText.icon = new FeathersImage(R.getAtlas().getTexture("Icon_Human"), GlobalAppValues.WHITE_COLOR);
			if(!_resultsText){
				this._resultsText = new IconnedLabel();
				this._resultsText.touchable = false;
			}
			
			this.addChild(this._resultsText);
			this._resultsText.icon = new FeathersImage(R.getAtlas().getTexture("ResponsesIcon"), GlobalAppValues.WHITE_COLOR);
			this._resultsText.text = "Results";
			if(!_captureDateText){
				this._captureDateText = new Label();
				this._captureDateText.touchable = false;
			}
			this.addChild(this._captureDateText);
			reset();
			this.addChild(this._map);
			this.addChild(this._avatar);
			this.addChild(this._emailTxt);
			this.addChild(this._phoneTxt);
			
			this.invalidate();
		}
		public function reset():void{
			
			this._emailTxt.text = "";
			this._phoneTxt.text = "";
			this._captureDateText.text = "";
			this._responsesText.text = "";
			_isLayedOut = false;
			removeMapListeners();
			removeAvatarListeners();
			if(this._map){
				if(this._map.texture){
					this._map.texture.dispose();
				}
			}
			if(this._avatar){
				if(this._avatar.parent){
					this._avatar.removeFromParent();
				}
				if(this._avatar.texture){
					this._avatar.texture.dispose();
				}
			}
		}
		override public function dispose():void
		{
			// TODO Auto Generated method stub
			super.dispose();
			if(this._map){
				if(this._map.texture){
					this._map.texture.dispose();
				}
			}
			if(this._avatar){
				if(this._avatar.texture){
					this._avatar.texture.dispose();
				}
			}
		}
	}
}