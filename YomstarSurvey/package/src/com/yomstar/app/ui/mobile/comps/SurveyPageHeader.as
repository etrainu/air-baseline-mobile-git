package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.dataObjects.Baseline;
	import com.yomstar.app.dataObjects.FreeTextQuestionData;
	import com.yomstar.app.dataObjects.FreeTextResponseData;
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.dataObjects.SampleDetail;
	import com.yomstar.app.events.ImageLoaderEvent;
	import com.yomstar.app.services.StaticMapManager;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.controls.Label;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import flash.text.TextFormatAlign;
	
	import starling.display.Image;
	import starling.events.Event;
	
	public class SurveyPageHeader extends ScrollContainer
	{
		private const GAP:uint = 10;
		private const PADDING_TOP:uint = 10;
		private const PADDING_RIGHT:uint = 10;
		private const PADDING_LEFT:uint = 10;
		private const PADDING_BOTTOM:uint = 10;
		
		private var _questionTypeLabel:Label;
		private var _questionTextLabel:Label;
		private var _ansTextInput:ChoiceLine;
		private var _baseline:Baseline;
		private var _pageTitle:String;
		private var _pageNumber:int;
		
		private var _artImage:Image;
		private var _initialized:Boolean;
		private var _contentColumn:ScrollContainer;
		public function SurveyPageHeader(baseline:Baseline, pageTitle:String, pageNumber:int, previewOnly:Boolean = true)
		{
			super();
			_initialized = false;
			this._baseline = baseline;
			this._pageTitle = pageTitle;
			this._pageNumber = pageNumber;
			this.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			this.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			
			const layout:HorizontalLayout = new HorizontalLayout();
			layout.gap = GAP;
			layout.paddingTop = PADDING_TOP;
			layout.paddingRight = PADDING_RIGHT;
			layout.paddingBottom = PADDING_BOTTOM;
			layout.paddingLeft = PADDING_LEFT;
			layout.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_LEFT;
			layout.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_TOP;
			this.layout = layout;
		}
		override protected function draw():void
		{
			if(!_initialized){
				this._questionTypeLabel.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT);
				this._questionTypeLabel.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
				this._questionTypeLabel.textRendererProperties.wordWrap = true;
				this._questionTextLabel.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT_DARK);
				this._questionTextLabel.textRendererProperties.wordWrap = true;
				this._initialized = true;
			}
			
			var realEstateWidth:int = this.actualWidth - PADDING_LEFT - PADDING_RIGHT - this._artImage.width - GAP*2;
			this._contentColumn.width = realEstateWidth;
			_contentColumn.invalidate(FeathersControl.INVALIDATION_FLAG_ALL);
			_contentColumn.validate();
			super.draw();
		}
		
		override protected function initialize():void
		{
			this.backgroundSkin = new LightPad();
			
			this._artImage = new Image(R.getAtlas().getTexture("Graph"));
			this.addChild(this._artImage);
			
			_contentColumn = new ScrollContainer();
			_contentColumn.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_contentColumn.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			const contentColumnLayout:VerticalLayout = new VerticalLayout();
			contentColumnLayout.gap = GAP;
			contentColumnLayout.paddingTop = 0;
			contentColumnLayout.paddingRight = 0;
			contentColumnLayout.paddingBottom = 0;
			contentColumnLayout.paddingLeft = 0;
			contentColumnLayout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			contentColumnLayout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			_contentColumn.layout = contentColumnLayout;
			this.addChild(_contentColumn);
			this._questionTypeLabel = new Label();
			this._questionTypeLabel.text = this._baseline.name;
			_contentColumn.addChild(this._questionTypeLabel);
			this._questionTextLabel = new Label();
			this._questionTextLabel.text = "Page "+String(Number(this._pageNumber+1))+": "+_pageTitle;
			_contentColumn.addChild(this._questionTextLabel);
			this._ansTextInput = new ChoiceLine("Date Created: "+_baseline.dateCreated.replace("T", " at "), false, -1, false);
			_contentColumn.addChild(this._ansTextInput);

			this.touchable = false;
			super.initialize();

		}
		private function onMapRecieved(e:ImageLoaderEvent):void{
			trace("onMapRecieved");
			var staticMapManager:StaticMapManager = StaticMapManager(e.currentTarget);
			staticMapManager.removeEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onMapRecieved);
			if(!_artImage){
				_artImage = new Image(e.imageTexture);
			}else{
				_artImage.texture= e.imageTexture;
				_artImage.readjustSize();
			}
			this.invalidate(INVALIDATION_FLAG_ALL);
			this.draw();
			Main.staticMapManagerPool.returnObj(staticMapManager);
		}
	}
}