package com.yomstar.app.ui.mobile.comps
{
	import starling.display.Quad;
	
	public class Spacer extends Quad
	{
		public function Spacer(width:Number = 10, height:Number = 10, premultipliedAlpha:Boolean=true)
		{
			super(width, height, 0x000000, premultipliedAlpha);
			this.visible = false;
			this.touchable = false;
		}
	}
}