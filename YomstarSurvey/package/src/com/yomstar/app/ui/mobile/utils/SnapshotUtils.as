package com.yomstar.app.ui.mobile.utils
{
	import flash.display.BitmapData;
	import flash.display3D.Context3D;
	import flash.geom.Rectangle;
	
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.utils.getNextPowerOfTwo;

	public class SnapshotUtils
	{
		public static function copyAsBitmapData(sprite:starling.display.DisplayObject, w:int = 0, h:int=0):BitmapData {
			if (sprite == null) return null;
			
			var support:RenderSupport = new RenderSupport();
			RenderSupport.clear(sprite.stage.color, 1.0);
			support.setOrthographicProjection(0, 0, w, h);
			sprite.stage.render(support, 1.0);
			support.finishQuadBatch();
			if(w<=0)w=sprite.width;
			if(h<=0)h=sprite.height;
			var result:BitmapData = new BitmapData(w, h, false, 0x00000000);
			
			//support.pushMatrix();
			
			//support.blendMode = sprite.blendMode;
			//support.transformMatrix(sprite);
			sprite.render(support, 1.0);
			//support.popMatrix();
			
			support.finishQuadBatch();
			
			Starling.context.drawToBitmapData(result);	
			
			return result;
		}
	}
}