package com.yomstar.app.ui.mobile.comps
{
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.easing.Cubic;
	import com.greensock.motionPaths.*;
	import com.greensock.plugins.*;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.core.PopUpManager;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	public class BusyIndicatorYommed extends Sprite
	{
		private const STAGE_DIM_COLOR:Number = 0x000000;
		private const STAGE_DIM_ALPHA:Number = 0.6;
		private const ROCKET_MOON_DISTANCE:uint = 30;
		private var busyArt:YommedBusyArt;
		private var stageDimmer:starling.display.Quad;
		private var moonImage:MoonImage;
		private var rocketImage:RocketImage;
		private var circlePath:CirclePath2D;
		private var ex:RocketExhust;
		private var onStoppedFunction:Function;
		public function BusyIndicatorYommed()
		{
			super();
			stageDimmer = new starling.display.Quad(10, 10, STAGE_DIM_COLOR);
			stageDimmer.alpha = STAGE_DIM_ALPHA;
		}
		public function start(dimStage:Boolean = true):void{
			Starling.current.stage.addEventListener(Event.RESIZE, onStageResized);
			
			this.addChildAt(stageDimmer, 0);
			if(!dimStage){
				stageDimmer.visible = false;
			}else{
				stageDimmer.visible = true;
			}
			if(!busyArt){
				setupArt();
			}
			this.addChild(busyArt);
			onStageResized();
			rotateRocket();
			PopUpManager.addPopUp(this, false, true);
		}
		private function setupArt():void{
			TweenPlugin.activate([CirclePath2DPlugin]);
			busyArt = new YommedBusyArt();
			moonImage = new MoonImage(R.getAtlas().getTexture("Moon"));
			rocketImage = new RocketImage(R.getAtlas().getTexture("Rocket"));
			rocketImage.pivotX = (rocketImage.width*0.5);
			rocketImage.pivotY = (rocketImage.height*0.5);
			moonImage.pivotX = (moonImage.width*0.5);
			moonImage.pivotY = (moonImage.height*0.5);
			moonImage.x = 0;
			moonImage.y = 0;
			
			ex = new RocketExhust();
			circlePath = new CirclePath2D(0, 0, (moonImage.width)*0.5+ROCKET_MOON_DISTANCE);
		}
		private function rotateRocket():void{
			TweenLite.killTweensOf(circlePath, true);
			TweenLite.killTweensOf(rocketImage, true);
			TweenLite.killTweensOf(moonImage, true);
			circlePath.scaleX = circlePath.scaleY = busyArt.scaleX = busyArt.scaleY = busyArt.alpha = 1;
			moonImage.rotation = 0;
			rocketImage.rotation = 0;
			Starling.juggler.add(busyArt);
			Starling.juggler.add(moonImage);
			TweenLite.from(moonImage, 0.7, {scaleX:0.01, scaleY:0.01, rotation:(360*3) , ease:Back.easeOut, onComplete:onArtVisible});
			busyArt.addChild(moonImage);
			
			TweenLite.from(circlePath, 0.5, {scaleX:0.1, scaleY:0.1, ease:Back.easeOut, delay:0.5});
			TweenLite.to(rocketImage, 25000, {circlePath2D:{path:circlePath, startAngle:359, endAngle:0, autoRotate:true, direction:Direction.COUNTER_CLOCKWISE, extraRevolutions:10000}, onComplete:rotateRocket, ease:Cubic.easeOut, delay:0.5});
			TweenLite.to(ex, 25000, {circlePath2D:{path:circlePath, startAngle:359, endAngle:0, autoRotate:false, direction:Direction.COUNTER_CLOCKWISE, extraRevolutions:10000}, ease:Cubic.easeOut, delay:0.5});
			addExhust();
		}
		private function onArtVisible():void{
			busyArt.addChildAt(rocketImage, 0);
			Starling.juggler.add(rocketImage);
			TweenLite.to(moonImage, 25000, {rotation:moonImage.rotation+400000, ease:Cubic.easeOut, delay:0});
		}
		private function addExhust():void
		{ 
			ex.addEventListener("complete", removeExhust);
			Starling.juggler.add(ex);
			ex.emitterX = 0;
			ex.emitterY = 0;
			busyArt.addChild(ex);               
			ex.start();
		}
		private function removeExhust(event:Event = null):void
		{       
			ex.stop();
			Starling.juggler.remove(ex);
			removeChild(ex);
		}
		private function stopRocket():void{
			
			Starling.juggler.add(busyArt);
			removeExhust();
			TweenLite.to(circlePath, 0.5, {scaleX:0.1, scaleY:0.1, ease:Back.easeIn});
			TweenLite.to(busyArt, 0.7, {scaleX:0.01, scaleY:0.01, ease:Back.easeIn, onComplete:onArtTweenOver, delay:0.3});
			//TweenLite.to(circlePath, 0.7, {scaleX:0.1, scaleY:0.1, onComplete:onArtTweenOver, ease:Back.easeInOut});
			//TweenLite.to(moonImage, 1, {scaleX:0.1, scaleY:0.1, alpha:0, onComplete:onArtVisible, ease:Back.easeInOut});
		}
		private function onArtTweenOver():void{
			TweenLite.killTweensOf(rocketImage);
			TweenLite.killTweensOf(moonImage);
			TweenLite.killTweensOf(busyArt);
			TweenLite.killTweensOf(circlePath);
			Starling.juggler.remove(busyArt);
			Starling.juggler.remove(rocketImage);
			Starling.juggler.remove(moonImage);
			busyArt.removeFromParent();
			PopUpManager.removePopUp(this);
			if(onStoppedFunction != null){
				onStoppedFunction();
			}
		}
		public function stop(onStoppedFunction:Function = null):void{
			this.onStoppedFunction = onStoppedFunction;
			Starling.current.stage.removeEventListener(Event.RESIZE, onStageResized);
			if(stageDimmer.parent){
				stageDimmer.removeFromParent();
			}
			stopRocket();
		}
		private function onStageResized(e:Event = null):void{
			stageDimmer.width = Starling.current.stage.stageWidth;
			stageDimmer.height = Starling.current.stage.stageHeight;
			busyArt.x = (Starling.current.stage.stageWidth)*0.5;
			busyArt.y = (Starling.current.stage.stageHeight)*0.5;
		}
			
	}
}