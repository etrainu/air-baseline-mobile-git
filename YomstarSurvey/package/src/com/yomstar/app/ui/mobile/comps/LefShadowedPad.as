package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.textures.Texture;

	public class LefShadowedPad extends FeathersControl
	{
		private var _image:Scale9Image;
		public function LefShadowedPad()
		{
		}
		override protected function initialize():void
		{
			const textures:Scale9Textures = new Scale9Textures(R.getAtlas().getTexture("leftShadowedRec"), new Rectangle(15, 1, 5, 3));
			this._image = new Scale9Image(textures);
			this.addChild(this._image);
		}
		override protected function draw():void
		{
			this._image.width = this.actualWidth;
			this._image.height = this.actualHeight;
		}
	}
}