package com.yomstar.app.ui.mobile.comps
{
	import starling.display.DisplayObject;

	public class QuedObject
	{
		private var _object:starling.display.DisplayObject;
		private var touchable:Boolean;
		public function QuedObject(object:DisplayObject)
		{
			this._object = object;
			touchable = object.touchable;
			object.touchable = false;
		}

		public function get displayObject():starling.display.DisplayObject
		{
			return _object;
		}

		public function restoreTouchability():void{
			_object.touchable = touchable;
		}
	}
}