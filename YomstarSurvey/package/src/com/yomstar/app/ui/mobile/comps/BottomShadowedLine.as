package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;

	public class BottomShadowedLine extends FeathersControl
	{
		private var _image:Image;
		public function BottomShadowedLine()
		{
		}
		override protected function initialize():void
		{
			this._image = new Image(R.getAtlas().getTexture("bottomShadowedSeperator"));
			this.addChild(this._image);
			this.invalidate();
		}
		override protected function draw():void
		{
			this._image.width = this.actualWidth;
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
		}
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				if(this._image){
					newWidth = this._image.width;
				}
				
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
				if(this._image){
					newHeight = this._image.height;
				}
			}
			return this.setSizeInternal(newWidth, newHeight, false);
		}
	}
}