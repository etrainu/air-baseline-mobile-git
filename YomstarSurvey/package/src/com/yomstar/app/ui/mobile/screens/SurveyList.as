package com.yomstar.app.ui.mobile.screens
{
	import com.yomstar.app.dataObjects.Baseline;
	import com.yomstar.app.services.UserInfo;
	import com.yomstar.app.services.UserSurvey;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.comps.BottomShadowedLine;
	import com.yomstar.app.ui.mobile.comps.FeathersImage;
	import com.yomstar.app.ui.mobile.comps.IconnedLabel;
	import com.yomstar.app.ui.mobile.comps.ListSurveysCalloutButton;
	import com.yomstar.app.ui.mobile.comps.ListSurveysCalloutContent;
	import com.yomstar.app.ui.mobile.comps.TransButton;
	import com.yomstar.app.ui.mobile.renderers.SurveyItemRenderer;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.ObjectDigger;
	
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.controls.Scroller;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	import feathers.skins.StandardIcons;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class SurveyList extends YomScreen
	{
		private const VERTICAL_GAP:uint = 20;
		private const HORIZENTAL_GAP:uint = 20;
		private const PADDING_TOP:uint = 20;
		private const PADDING_RIGHT:uint = 20;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 20;
		private var _initialized:Boolean;
		private var _backButton:TransButton;
		private var _list:List;
		private var listdb:ListCollection;
		private var _surveyText:IconnedLabel;
		private var _seperator:BottomShadowedLine;
		public function SurveyList()
		{
			super();
			_initialized = false;
		}
		
		override protected function draw():void
		{
			super.draw();
			
			super.draw();
			if(!_initialized){
				_initialized = true;
				this._surveyText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.HIGHLIGHT_TEAXT_FORMAT)};
				this._surveyText.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
			}
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			if(dataInvalid)
			{
				this.commitData();
			}
			this.header.validate();
			this._surveyText.validate();
			if(dataInvalid || sizeInvalid)
			{
				this.layout();
			}
			
		}
		private function layout():void{
			this._surveyText.x = this.PADDING_LEFT;
			this._surveyText.y = this.header.height+this.PADDING_TOP;
			this._surveyText.width = this.actualWidth-this.PADDING_LEFT-this.PADDING_RIGHT;
			this._surveyText.validate();
			
			this._seperator.width = actualWidth;
			this._seperator.y = this._surveyText.y + this._surveyText.height+VERTICAL_GAP;
			this._seperator.validate();
			this._list.width = actualWidth;
			this._list.y = this._seperator.y + this._seperator.height+5;
			this._list.height = actualHeight-this._list.y;
		}
		override protected function initialize():void
		{
			super.initialize();
			this._backButton = new TransButton();
			var beckImage:Image = new Image(R.getAtlas().getTexture("back"));
			this._backButton.defaultIcon = beckImage;
			this._backButton.defaultSelectedIcon = beckImage;
			this._backButton.autoFlatten = true
			this._backButton.addEventListener(starling.events.Event.TRIGGERED, backButton_triggeredHandler);
			this.addChild(this.header);
			this.header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
			this.backButtonHandler = this.onBackButton;
			this._list = new List();
			this._list.itemRendererType = SurveyItemRenderer;
			
			if(!_surveyText){
				_surveyText = new IconnedLabel();
			}
			this.addChild(_surveyText);
			_surveyText.icon = new FeathersImage(R.getAtlas().getTexture("SurveysIcon"), GlobalAppValues.FEATURED_COLOR_TWO);
			this._seperator = new BottomShadowedLine();
			this.addChild(this._seperator);
		}
		override protected function run():void{
			super.run();
			busyIndicator.start();
			if(UserInfo.userInfo){
				getSurveyList();
			}else{
				var userInfo:UserInfo = new UserInfo();
				userInfo.addEventListener(flash.events.Event.COMPLETE, onUserInfo, false, 0, true);
				userInfo.addEventListener(flash.events.ErrorEvent.ERROR, onUserInfoError, false, 0, true);
				userInfo.getInfo(Main.data.token);
			}
			
		}
		override protected function screen_addedToStageHandler(event:starling.events.Event):void
		{
			super.screen_addedToStageHandler(event);
		}
		private function commitData():void{
			_surveyText.text = "Surveys";
			if(listdb){
				this._list.dataProvider = listdb;
			}
		}
		private function onUserInfoError(event:flash.events.ErrorEvent):void
		{
			trace("onUserInfoError: "+event.text);
		}
		private function onUserInfo(event:flash.events.Event):void
		{
			var userInfo:UserInfo = event.target as UserInfo;
			userInfo.removeEventListener(flash.events.Event.COMPLETE, onUserInfo);
			getSurveyList();
		}
		private function getSurveyList():void{
			var userSurvey:UserSurvey = new UserSurvey();
			userSurvey.addEventListener(flash.events.Event.COMPLETE, onUserSurveyInfo, false, 0, true);
			userSurvey.getUserSurvey(Main.data.token);
		}
		protected function onUserSurveyInfo(event:flash.events.Event):void
		{
			var userSurvey:UserSurvey = event.target as  UserSurvey;
			userSurvey.removeEventListener(flash.events.Event.COMPLETE, onUserSurveyInfo);
			//{"baselines":[{"id":"46jrJUQ8TqzjeYD5ohN5g","name":"Mulpile Page Survey","dateCreated":"2012-12-20T13:59:22","createdBy":{"id":"1yTw4Y5nTwGJOUY7Ib0Cdw","name":"Amir Zahedi"},"versionNumber":"4","isPublic":"false","isReleased":"true","allowAnonymous":"true","account":{"id":"bjS7OO13TiKrOe5OFioxKw","name":"Amir's company"},"hasRecommendations":"false","publicResults":"true","shareURI":"46jrJUQ8TqzjeYD5ohN5g","shortURL":"http://goo.gl/zG09C","sampleCount":"2","firstSample":"2012-12-20T14:05:06","lastSample":"2012-12-22T16:36:00"}]}
			//sample response: isPublic:false, sampleCount:20, versionNumber:1, allowAnonymous:true, createdBy:[object Object], dateCreated:2012-12-11T16:56:47, shareURI:WJGosSJaRU2KuPBank1bCg, firstSample:2012-12-11T16:57:14, name:Mobile App Test Survey, hasRecommendations:false, id:WJGosSJaRU2KuPBank1bCg, shortURL:http://goo.gl/cpQrI, isReleased:true, lastSample:2012-12-14T13:17:21, account:[object Object], publicResults:true
			trace("onUserSurveyInfo: "+ObjectDigger.digg(UserSurvey.userSurveyInfo.baselines[0]));
			listdb = new ListCollection(UserSurvey.userSurveyInfo.baselines);
			this._list.itemRendererProperties.labelField = "name";
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = 0;
			layout.paddingTop = 0;
			layout.paddingRight = 0;
			layout.paddingBottom = 0;
			layout.paddingLeft = 0;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			layout.hasVariableItemDimensions = true;
			this._list.layout = layout;
			this._list.itemRendererProperties.accessoryFunction  = listItemAccessoryFunction;
			this._list.itemRendererProperties.detailsLabelFunction = listItemDetailsFunction;
			this._list.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._list.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this._list.scrollerProperties.snapScrollPositionsToPixels = true;
			this._list.addEventListener(starling.events.Event.CHANGE, listChanged);
			addChild(this._list);
			busyIndicator.stop();
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		private function listItemDetailsFunction(o:Baseline):String{
			var detailsStr:String = String(o.dateCreated).replace("T", " at ");
			return detailsStr;
		}
		private function listItemAccessoryFunction(o:Baseline):DisplayObject{
			var accessoryBtn:ListSurveysCalloutButton = new ListSurveysCalloutButton();
			accessoryBtn.defaultIcon = new Image(R.getAtlas().getTexture("Icon_Launch_Survey"));
			accessoryBtn.baseline = o;
			accessoryBtn.addEventListener(starling.events.Event.TRIGGERED, onAccessoryTriggered);
			accessoryBtn.addEventListener(starling.events.Event.OPEN, onAccessoryOpen);
			return accessoryBtn;
		}
		private function onAccessoryTriggered(e:starling.events.Event):void{
			var accessoryBtn:ListSurveysCalloutButton = ListSurveysCalloutButton(e.currentTarget);
			this.dispatchEventWith("webViewRequest", false, {baseline:accessoryBtn.baseline});
		}
		private function onAccessoryOpen(e:starling.events.Event):void{
			var accessoryBtn:ListSurveysCalloutButton = ListSurveysCalloutButton(e.currentTarget);
			this.dispatchEventWith("surveyRequest", false, {baseline:accessoryBtn.baseline});
		}
		private function listChanged(e:starling.events.Event):void
		{
			trace("List Cahnged");
			dispatchEventWith("surveySelected", false, this._list.selectedItem);
		}
		private function onBackButton():void
		{
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function backButton_triggeredHandler(event:starling.events.Event):void
		{
			this.onBackButton();
		}
		
	}
}