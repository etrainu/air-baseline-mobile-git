package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.dataObjects.GeoData;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	
	import starling.events.Event;
	
	
	public class ListDetailsCalloutButton extends FeathersControl
	{
		private var _label:String;
		public var calloutStr:String;
		public var geoData:GeoData;
		private var _button:Button;
		public function ListDetailsCalloutButton()
		{
			super();
			_label = "";
		}

		public function get label():String
		{
			_label = _button.label;
			return _label;
		}

		public function set label(value:String):void
		{
			_label = value;
			if(this._button){
				this._button.label = _label;
			}
			
		}
		
		override protected function draw():void
		{
			this._button.validate();
			this._button.x = -1*this._button.width;
			this._button.y = this._button.height*-0.5;
			
		}
		
		override protected function initialize():void
		{
			this._button = new Button();
			this._button.addEventListener(Event.TRIGGERED, onResponseDetailsRequest);
		
			this._button.label = _label;
			//this._button.x = -200;
			addChild(_button);
		}
		private function onResponseDetailsRequest(e:Event):void{
			this.dispatchEvent(e);
		}
	}
}