package com.yomstar.app.ui.mobile.screens
{
	import com.yomstar.app.config.QuestionType;
	import com.yomstar.app.config.ResponseType;
	import com.yomstar.app.dataObjects.Baseline;
	import com.yomstar.app.dataObjects.FreeTextQuestionData;
	import com.yomstar.app.dataObjects.FreeTextResponseData;
	import com.yomstar.app.dataObjects.MultiChoiceQuestionData;
	import com.yomstar.app.dataObjects.MultiChoiceResponseData;
	import com.yomstar.app.dataObjects.QuestionData;
	import com.yomstar.app.dataObjects.ResponseData;
	import com.yomstar.app.dataObjects.SampleDetail;
	import com.yomstar.app.dataObjects.ScaledQuestionData;
	import com.yomstar.app.dataObjects.ScaledResponseData;
	import com.yomstar.app.services.BaselineDetails;
	import com.yomstar.app.services.SampleDetails;
	import com.yomstar.app.services.UserInfo;
	import com.yomstar.app.services.UserSurvey;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.comps.HighLightPad;
	import com.yomstar.app.ui.mobile.comps.ListSurveysCalloutButton;
	import com.yomstar.app.ui.mobile.comps.QuestionFreeText;
	import com.yomstar.app.ui.mobile.comps.QuestionFreeTextPreview;
	import com.yomstar.app.ui.mobile.comps.QuestionMultiChoice;
	import com.yomstar.app.ui.mobile.comps.QuestionMultiChoicePreview;
	import com.yomstar.app.ui.mobile.comps.QuestionScaled;
	import com.yomstar.app.ui.mobile.comps.QuestionScaledPreview;
	import com.yomstar.app.ui.mobile.comps.SamplePageHeader;
	import com.yomstar.app.ui.mobile.comps.SurveyPageHeader;
	import com.yomstar.app.ui.mobile.comps.TransButton;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.MatchResponse;
	import com.yomstar.app.utils.ObjectDigger;
	
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.PageIndicator;
	import feathers.controls.Screen;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.data.ListCollection;
	import feathers.events.FeathersEventType;
	import feathers.layout.VerticalLayout;
	import feathers.skins.StandardIcons;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class SurveyView extends YomScreen
	{
		public var baseline:Baseline;
		private const VERTICAL_GAP:uint = 10;
		private const PADDING_TOP:uint = 20;
		private const PADDING_RIGHT:uint = 20;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 20;
		private const CALLOUT_WIDTH:uint = 350;
		private const CALLOUT_HEIGHT:uint = 500;
		private const NEXT_PAGE_LABEL:String = "Next Page";
		private const SUBMIT_LABEL:String = "Submit";
		
		private var _container:ScrollContainer;
		private var _pageSubmitBtn:Button;
		private var _currentPage:int;
		private var _sampleDetails:SampleDetail;
		private var _baselineObject:Object;
		private var _backButton:TransButton;
		
		private var _surveyQuestions:Vector.<QuestionData>;
		public function SurveyView()
		{
			super();
			_surveyQuestions = new Vector.<QuestionData>();
			_currentPage = 0;
		}
		
		override protected function draw():void
		{
			super.draw();
			this._pageSubmitBtn.x = PADDING_LEFT;
			this._pageSubmitBtn.width = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			this._pageSubmitBtn.validate();
			this._pageSubmitBtn.y = actualHeight-_pageSubmitBtn.height-PADDING_BOTTOM;
			this._container.y = this.header.height;
			this._container.width = this.actualWidth;
			this._container.height = this.actualHeight-this._container.y-(this.actualHeight - _pageSubmitBtn.y)-VERTICAL_GAP;
			this._container.validate();
			const layout:VerticalLayout = VerticalLayout(this._container.layout);
			var realEstateWidth:int = this._container.width - layout.paddingLeft - layout.paddingRight;
			var len:uint = this._container.numChildren;
			for(var i:int = 0; i<len; i++){
				this._container.getChildAt(i).width = realEstateWidth;
			}
		}
		
		override protected function initialize():void
		{
			super.initialize();
			_surveyQuestions.length = 0;
			
			this._backButton = new TransButton();
			var beckImage:Image = new Image(R.getAtlas().getTexture("back"));
			this._backButton.defaultIcon = beckImage;
			this._backButton.defaultSelectedIcon = beckImage;
			this._backButton.autoFlatten = true
			//ObjectDeigger.digg("	defaultLabelProperties: "+this._backButton.	defaultLabelProperties);
			this._backButton.addEventListener(starling.events.Event.TRIGGERED, backButton_triggeredHandler);
			this.addChild(this.header);
			this.header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
			this.backButtonHandler = this.onBackButton;
			
			this._container = new ScrollContainer();
			//this._container.backgroundSkin = new HighLightPad();
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = VERTICAL_GAP;
			layout.paddingTop = PADDING_TOP;
			layout.paddingRight = PADDING_RIGHT;
			layout.paddingBottom = PADDING_BOTTOM;
			layout.paddingLeft = PADDING_LEFT;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_CENTER;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			this._container.layout = layout;
			this._container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._container.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this._container.scrollerProperties.snapScrollPositionsToPixels = true;
			this._container.addEventListener(starling.events.Event.SCROLL, onListScrolling);
			this._container.addEventListener(feathers.events.FeathersEventType.SCROLL_COMPLETE, onListScrollComplete);
			this.addChild(this._container);
			
			this._pageSubmitBtn = new Button();
			this._pageSubmitBtn.isEnabled = false;
			this._pageSubmitBtn.addEventListener(starling.events.Event.TRIGGERED, pageSubmitTriggered);
			this.addChild(this._pageSubmitBtn);
		}
		private function onListScrolling(e:starling.events.Event):void{
			for(var i:int =0; i<this._container.numChildren; i++){
				this._container.getChildAt(i).touchable = false;
			}
		}
		private function onListScrollComplete(e:starling.events.Event):void{
			for(var i:int =0; i<this._container.numChildren; i++){
				this._container.getChildAt(i).touchable = true;
			}
		}
		override protected function run():void{
			super.run();
			var sampleDetails:SampleDetails = new SampleDetails();
			busyIndicator.start();
			getBaselineDetails();
		}
		private function pageSubmitTriggered(event:starling.events.Event):void
		{
			if(this._currentPage < Number(this._baselineObject.pages.length)-1){
				trace("page updated");
				this._container.verticalScrollPosition = 0;
				this._container.removeChildren(0, -1, true);
				this._currentPage++;
				updateSbmitPageLabel();
				addPage(this._currentPage);
			}else{
				//submit results
				this._pageSubmitBtn.isEnabled = false;
				trace("page submitted");
			}
		}
		private function updateSbmitPageLabel():void{
			if(_currentPage == Number(_baselineObject.pages.length)-1){
				_pageSubmitBtn.label = SUBMIT_LABEL;
			}else{
				_pageSubmitBtn.label = NEXT_PAGE_LABEL;
			}
		}
		private function getBaselineDetails():void{
			var baselineDetails:BaselineDetails = new BaselineDetails();
			baselineDetails.addEventListener(flash.events.Event.COMPLETE, onBaselineDetails, false, 0, true);
			baselineDetails.addEventListener(ErrorEvent.ERROR, onBaselineError, false, 0, true);
			trace("shareURI: "+baseline.shareURI);
			baselineDetails.getSurveyById(Main.data.token, baseline.shareURI);
			
		}
		private function onBaselineError(event:flash.events.Event):void
		{
			var baselineDetails:BaselineDetails = event.target as BaselineDetails;
			baselineDetails.removeEventListener(flash.events.Event.COMPLETE, onBaselineDetails);
			baselineDetails.removeEventListener(ErrorEvent.ERROR, onBaselineError);
			alert.title = "Survey not found";
			alert.message = "The survey you are asking for is not accessible. It might have been deleted by the owner.";
			alert.closeLabel = "OK";
			alert.show();
		}
		private function onBaselineDetails(event:flash.events.Event):void
		{
			var baselineDetails:BaselineDetails = event.target as BaselineDetails;
			baselineDetails.removeEventListener(flash.events.Event.COMPLETE, onBaselineDetails);
			baselineDetails.removeEventListener(ErrorEvent.ERROR, onBaselineError);
			//sample response json1:  {"id":"46jrJUQ8TqzjeYD5ohN5g","createdByAccountName":"Amir's company","name":"Mulpile Page Survey","dateCreated":"2012-12-20T13:59:22","createdBy":{"id":"1yTw4Y5nTwGJOUY7Ib0Cdw","name":"Amir Zahedi"},"versionNumber":"4","isPublic":"false","isReleased":"true","allowAnonymous":"true","account":{"id":"bjS7OO13TiKrOe5OFioxKw","name":"Amir's company"},"hasRecommendations":"false","publicResults":"true","shareURI":"46jrJUQ8TqzjeYD5ohN5g","shortURL":"http://goo.gl/zG09C","captureParticipants":"false","notifyResults":"false","notifyThresholdPercentage":"100","pages":[{"id":"uawT7VoNT0JqduCZlfEg","pageTitle":"Wheather","order":"1","componentGroups":[{"id":"JG2R8E6tSkegCzJUrxg","groupName":"","order":"0","components":[{"type":"multichoicequestion","id":"WLEJIZfsRTG8qFg4JGgiyA","questionText":"How often dose it rain in your area","order":"0","isMulti":"false","isRequired":"false","allowOther":"true","allowMediaCapture":"false","scoreCalculationMethod":"AVERAGE","options":[{"type":"multichoiceoption","id":"2anGpzE2Qvib0FoEoZPVKA","description":"1-5 times a year","value":"0"},{"type":"multichoiceoption","id":"C73rVoQMTcajRhWkRoagg","description":"5-10 times a year","value":"25"},{"type":"multichoiceoption","id":"dM2Jl4lQR69EJ9e0Ntlw","description":"10-20 times a year","value":"75"},{"type":"multichoiceoption","id":"xj5Z6cVyQHW5D0D3cCAiPg","description":"20+ times a year","value":"100"},{"type":"multichoiceotheroption","id":"rGIZ7ArhRKmWvpX3mf1Q","description":"Other. Please specify:"}],"metrics":[]},{"type":"sliderquestion","id":"jRf0cgZPTnaXheo3qbcsVA","questionText":"What is the temperature like in your area","order":"0","isRequired":"false","allowMediaCapture":"false","options":[{"type":"slideroption","id":"9cuUVom4TeWzwNonwyPdtg","description":"Too hot","value":"0","isDefault":"false"},{"type":"slideroption","id":"xfGpnt8QEGdoJalBv4fCg","description":"Very hot","value":"25","isDefault":"false"},{"type":"slideroption","id":"q0WW0x19SKe8MyHD7anC2g","description":"Hot","value":"50","isDefault":"false"},{"type":"slideroption","id":"VMTKRQNGStwqdEhzUBaQ","description":"Fair","value":"75","isDefault":"false"},{"type":"slideroption","id":"4iYclZaRr6iYPTWWU9Tg","description":"Cold","value":"100","isDefault":"false"}],"metrics":[]},{"type":"sliderquestion","id":"Hggci5BJSpyurBq7bGMA1A","questionText":"How far do you live from the coast?","order":"0","isRequired":"false","allowMediaCapture":"false","options":[{"type":"slideroption","id":"8r2FSTPTxCHdnjQrPAOpA","description":"Pretty close","isDefault":"false"},{"type":"slideroption","id":"FLEVwNU5QYGq5edxV1M54Q","description":"Within 10k","isDefault":"false"},{"type":"slideroption","id":"COaqCCl1QmOdSPQebNvFEw","description":"Within 20k","isDefault":"false"},{"type":"slideroption","id":"a3mf5gtTZiyVBdp2iWujw","description":"Within 30k","isDefault":"false"},{"type":"slideroption","id":"R8o3BVqNQ7O2Oj2eFfxnqA","description":"Within 40k","isDefault":"false"},{"type":"slideroption","id":"huaHgAbmTxmyMa8TpbNHMg","description":"Within 50k","isDefault":"false"},{"type":"slideroption","id":"3arJaCSXaRygCfDfPmw","description":"50+ k","isDefault":"false"},{"type":"slideroption","id":"H4XNtlCfRVmg2EUUTzFyPQ","description":"100+ k","isDefault":"false"},{"type":"slideroption","id":"Mrec4RC9SmuF6qrSPbLc9A","description":"200+ k","isDefault":"false"}],"metrics":[]},{"type":"freetextquestion","id":"B9SCe8nQqaXUUfXwkLaZw","questionText":"Do you have any more to share?","order":"0","isAutoMark":"false","isRequired":"false","allowMediaCapture":"false","options":[],"metrics":[]}]}]},{"id":"NnXxMoUTECl9Mew1cFT0A","pageTitle":"Traffic","order":"2","componentGroups":[{"id":"y7dKJjeKROChN6GnoLQpvQ","groupName":"","order":"0","components":[{"type":"sliderquestion","id":"cOUxpX8QQlzziKU0HIWg","questionText":"How many times a week do you get stock in traffic congestion?","order":"0","isRequired":"false","allowMediaCapture":"false","options":[{"type":"slideroption","id":"vDeiFhzHR0iFboBFcKeHVg","description":"More than 10 times","value":"0","isDefault":"false"},{"type":"slideroption","id":"LUKSfdTQT4GZrvym2VYZpw","description":"7-10 times","value":"25","isDefault":"false"},{"type":"slideroption","id":"MWmzduP1QIiOAb4B8uTkXA","description":"4-6 times","value":"50","isDefault":"false"},{"type":"slideroption","id":"eBBxYB4Tcq0THbmyuxZw","description":"1-3 times","value":"75","isDefault":"false"},{"type":"slideroption","id":"0Zd3EEPuShuTsH670g2CmA","description":"Not at all","value":"100","isDefault":"false"}],"metrics":[]},{"type":"multichoicequestion","id":"IjBtk8SHiAoVio3hAVpg","questionText":"How do you describe public transport in your area?","order":"0","isMulti":"false","isRequired":"false","allowOther":"false","allowMediaCapture":"false","scoreCalculationMethod":"AVERAGE","options":[{"type":"multichoiceoption","id":"kXTT4mHKSMoonMOLoDNoA","description":"Terrible","value":"0"},{"type":"multichoiceoption","id":"6Vhy93zpQEGMRGa3L06lw","description":"Very bad","value":"25"},{"type":"multichoiceoption","id":"GAa6MatcQJGOmBj6h3lMQ","description":"Bad","value":"50"},{"type":"multichoiceoption","id":"72MOep7CR4uNinGfy7WvVA","description":"Fair","value":"75"},{"type":"multichoiceoption","id":"36Rw1T7T4WCI9UsARG0KA","description":"Good","value":"100"}],"metrics":[]},{"type":"freetextquestion","id":"extG4cRLbLHNZwWB4ow","questionText":"Any more to share?","order":"0","isAutoMark":"false","isRequired":"false","allowMediaCapture":"false","options":[],"metrics":[]}]}]}]}
			//sample response json2:  {"id":"ZErLqFNmSJ2YHNL9P4XcQ","createdByAccountName":"bree's company","name":"API Test Multi","dateCreated":"2013-01-03T14:35:40","createdBy":{"id":"MnpPF0BdT065SCX06wSfSg","name":"bree dev test"},"versionNumber":"1","isPublic":"false","isReleased":"true","allowAnonymous":"true","account":{"id":"AU9Rp2qgT2OjccEINWJQ","name":"bree's company"},"hasRecommendations":"false","publicResults":"true","shareURI":"ZErLqFNmSJ2YHNL9P4XcQ","shortURL":"http://goo.gl/y7wyo","captureParticipants":"false","notifyResults":"false","notifyThresholdPercentage":"100","pages":[{"id":"jkNLwRggTq6fOSCrDvBHA","pageTitle":"Page1 ","order":"1","componentGroups":[{"id":"F2VJxBjQmVKQybOdaRpQ","groupName":"","order":"0","components":[{"type":"multichoicequestion","id":"TwqD1GPmRqi19R1QD3WpXA","questionText":"multi + other","order":"0","isMulti":"true","isRequired":"false","allowOther":"true","allowMediaCapture":"false","scoreCalculationMethod":"AVERAGE","options":[{"type":"multichoiceoption","id":"VuBH76WQY23K03tO0kqA","description":"value 1","value":"0"},{"type":"multichoiceoption","id":"keWrHzJfTCapdLxbTwcWYQ","description":"value 2","value":"25"},{"type":"multichoiceoption","id":"kB0qFu0mTreWttSorp3Q","description":"value 4"},{"type":"multichoiceoption","id":"PHOMqhhuTliIOfp2vXMOg","description":"value 5"},{"type":"multichoiceoption","id":"0J4SLB4oQ8eX2xPeMf20Bw","description":"value 7"},{"type":"multichoiceotheroption","id":"5zHcNQVeQMKZKYu8odb8vw","description":"Other. Please specify:"}],"metrics":[]},{"type":"multichoicequestion","id":"0KXGd2DQSOzFdPlrWfzQ","questionText":"Mandetory","order":"0","isMulti":"false","isRequired":"true","allowOther":"false","allowMediaCapture":"false","scoreCalculationMethod":"AVERAGE","options":[{"type":"multichoiceoption","id":"fEoKg4PREetf5vN8LTneQ","description":"value 1"},{"type":"multichoiceoption","id":"AgptdjtrTNqi3rEk2riSQ","description":"value 2"},{"type":"multichoiceoption","id":"jSXyI1odQJ2x07yZ29R4sg","description":"value 3"},{"type":"multichoiceoption","id":"qJCqECuQRiUB5pKDWQcIA","description":"value 4"}],"metrics":[]},{"type":"multichoicequestion","id":"OVaMePyESfO1CEDJzdVIiQ","questionText":"multi + other + Mandetory","order":"0","isMulti":"true","isRequired":"true","allowOther":"true","allowMediaCapture":"false","scoreCalculationMethod":"AVERAGE","options":[{"type":"multichoiceoption","id":"H3Zg2G5fSn6UgepLrtbboQ","description":"value 1"},{"type":"multichoiceoption","id":"hjY4AtNNSf6nCevFXseT0Q","description":"value 2"},{"type":"multichoiceoption","id":"Q3saiMq7TdeXkjqj9MSNQw","description":"value 3"},{"type":"multichoiceoption","id":"C6RQgJ26SnqnjFTLIGuYKA","description":"value 4"},{"type":"multichoiceotheroption","id":"WGm0u1Z5T7i1h5Vye8ec9A","description":"Other. Please specify:"}],"metrics":[]}]}]}]}
			//sample response: createdBy:[object Object], hasRecommendations:false, publicResults:true, account:[object Object], name:Survey 2, captureParticipants:false, isPublic:false, id:kCYdTJbTZKdMjdMiCxBvQ, notifyResults:false, isReleased:true, versionNumber:1, shortURL:http://goo.gl/vVsyj, allowAnonymous:true, notifyThresholdPercentage:100, dateCreated:2012-12-17T12:32:28, createdByAccountName:Amir's company, pages:[object Object], shareURI:kCYdTJbTZKdMjdMiCxBvQ
			_baselineObject = BaselineDetails.baselineInfo;
			//trace("onBaselineDetails: "+ObjectDeigger.digg(baselineObject));
			diplayBaseline();
		}
		private function diplayBaseline():void{
			header.title = String(_baselineObject.name);
			updateSbmitPageLabel();
			busyIndicator.stop();
			addPage(0);
		}
		private function addPage(pageNumber:uint):void{
			this._pageSubmitBtn.isEnabled = false;
			generateQuestionObjectsForPage(pageNumber);
			var surveyPageHeader:SurveyPageHeader = new SurveyPageHeader(this.baseline, _baselineObject.pages[pageNumber].pageTitle, pageNumber);
			_container.addChild(surveyPageHeader);
			var freeTextQ:QuestionFreeText;
			var multiChoiceQ:QuestionMultiChoice;
			var scaledQ:QuestionScaled;
			var freeResponseData:FreeTextResponseData;
			var scaledResponseData:ScaledResponseData;
			var responseDataIndex:int;
			var len:int = _surveyQuestions.length;
			for(var i:int = 0; i<len; i++){
				switch(_surveyQuestions[i].type){
					case QuestionType.FREE_TEXT_QUESTION:
						freeTextQ = new QuestionFreeText(_surveyQuestions[i] as FreeTextQuestionData);
						_container.addChild(freeTextQ);
						trace(freeTextQ+" added");
						break
					case QuestionType.MULTI_CHOICE_QUESTION:
						multiChoiceQ = new QuestionMultiChoice(_surveyQuestions[i] as MultiChoiceQuestionData);
						_container.addChild(multiChoiceQ);
						trace(multiChoiceQ+" added");
						break
					case QuestionType.SLIDER_QUESTION:
						scaledQ = new QuestionScaled(_surveyQuestions[i] as ScaledQuestionData);
						_container.addChild(scaledQ);
						trace(scaledQ+" added");
						break
					default:
						throw new Error(_surveyQuestions[i].type+" is not a recognized question type");
						break
				}
			}
			draw();
			this._container.validate();
			this._container.addEventListener(starling.events.Event.SCROLL, onContainerScrolled);
		}
		private function onContainerScrolled(e:starling.events.Event):void{
			if(this._container.verticalScrollPosition>=this._container.maxVerticalScrollPosition){
				this._pageSubmitBtn.isEnabled = true;
				this._container.removeEventListener(starling.events.Event.SCROLL, onContainerScrolled);
			}
		}
		private function generateQuestionObjectsForPage(pageNumber:uint):void{
			_surveyQuestions.length = 0;
			var page:Object = _baselineObject.pages[pageNumber];
			trace("page "+Number(pageNumber+1)+": "+page.pageTitle);
			var components:Array = page.componentGroups[0].components as Array;
			var len:int = components.length;
			var freeQuestionData:FreeTextQuestionData;
			var multiChoiceQuestionData:MultiChoiceQuestionData;
			var scaledQuestionData:ScaledQuestionData;
			for(var i:int = 0; i<len; i++){
				trace(i+": "+components[i].type);
				switch(components[i].type){
					case QuestionType.FREE_TEXT_QUESTION:
						freeQuestionData = new FreeTextQuestionData(components[i]);
						trace("freeQuestionData: "+freeQuestionData);
						_surveyQuestions.push(freeQuestionData);
						
						break
					case QuestionType.MULTI_CHOICE_QUESTION:
						multiChoiceQuestionData = new MultiChoiceQuestionData(components[i]);
						_surveyQuestions.push(multiChoiceQuestionData);
						break
					case QuestionType.SLIDER_QUESTION:
						scaledQuestionData = new ScaledQuestionData(components[i]);
						_surveyQuestions.push(scaledQuestionData);
						break
					default:
						throw new Error(components[i].type+" is not a recognized question type");
						break
				}
			}
		}
		private function addQuestion():void{
			
		}
		private function onBackButton():void
		{
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function backButton_triggeredHandler(event:starling.events.Event):void
		{
			this.onBackButton();
		}
		
	}
}