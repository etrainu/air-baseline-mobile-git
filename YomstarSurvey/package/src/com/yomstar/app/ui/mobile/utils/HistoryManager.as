package com.yomstar.app.ui.mobile.utils
{

	public class HistoryManager
	{
		private static const MAX_HISTORY_LENGTH:uint = 10;
		private static var historyArr:Vector.<String> = new Vector.<String>();
		public static function addScreen(screenId:String):void{
			if(historyArr.length > 0){
				if(screenId != historyArr[historyArr.length-1]){
					if(historyArr.length >= 10){
						historyArr.shift();
					}
					historyArr.push(screenId);
				}
			}else{
				historyArr.push(screenId);
			}
		}
		public static function previousScreen():String{
			historyArr.pop();
			return historyArr[historyArr.length-1]
		}
		public static function clearHistory():void{
			historyArr.length = 0;
		}
		
	}
}