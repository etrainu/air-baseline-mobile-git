package com.yomstar.app.ui.mobile.comps
{
	
	import flash.display.BitmapData;
	import flash.display.CapsStyle;
	import flash.display.Graphics;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	import starling.utils.deg2rad;
	
	public class PercentGraphCircularLegacyDrawer extends Sprite
	{
		private var _bitmapData:BitmapData;
		public function PercentGraphCircularLegacyDrawer()
		{
			super();
		}
		public function draw(percent:Number, graphColorBack:Number, graphColor:Number, size:uint, thickness:uint):BitmapData{
			var thicknessHalf:uint = 0.5*thickness;
			var deg:uint = (percent*360)/100;
			var radius:uint = (0.5*size)-thicknessHalf;
			var trueHalf:uint = radius+thicknessHalf;
			var trueSize:uint = (radius+thicknessHalf)*2;
			var baseCircle:Shape = new Shape();
			baseCircle.graphics.lineStyle(thickness, graphColorBack);
			baseCircle.graphics.drawCircle(trueHalf, trueHalf, radius);
			this.addChild(baseCircle);
			
			var graphCircle:Shape = new Shape();
			graphCircle.graphics.lineStyle(thickness, graphColor, 4, true, LineScaleMode.NORMAL, CapsStyle.NONE, JointStyle.BEVEL);
			graphCircle.graphics.drawCircle(trueHalf, trueHalf, radius);
			
			var mask:Sprite = new Sprite();
			trace("deg: "+deg);
			var chunk:Shape;
			if(deg<=90){
				mask.addChild(drawChunkForDeg(deg, trueHalf));
				mask.x = trueHalf;
			}else if(deg<=180){
				mask.graphics.beginFill(0x00FF00);
				mask.graphics.moveTo(0, 0);
				mask.graphics.lineTo(trueHalf, 0);
				mask.graphics.lineTo(trueHalf, trueHalf);
				mask.graphics.lineTo(0, trueHalf);
				mask.graphics.lineTo(0, 0);
				mask.graphics.endFill();
				
				chunk = drawChunkForDeg(deg-90, trueHalf);
				mask.addChild(chunk);
				chunk.rotation = 90;
				chunk.x = chunk.y = trueHalf;
				mask.x = trueHalf;
			}else if(deg<=270){
				mask.graphics.beginFill(0x00FF00);
				mask.graphics.moveTo(0, 0);
				mask.graphics.lineTo(trueHalf, 0);
				mask.graphics.lineTo(trueHalf, trueSize);
				mask.graphics.lineTo(0, trueSize);
				mask.graphics.lineTo(0, 0);
				mask.graphics.endFill();
				chunk = drawChunkForDeg((deg-180), trueHalf);
				mask.addChild(chunk);
				chunk.rotation = 180;
				chunk.x = 0;
				chunk.y = trueSize;
				mask.x = trueHalf;
			}
			
			//mask.y = -trueHalf;
			graphCircle.mask = mask;
			this.addChild(graphCircle);
			//addChild(mask);
			return bitmapData
		}
		private function drawChunkForDeg(deg:uint, radius:uint):Shape{
			deg = 90-deg;
			var degRad:Number = deg2rad(deg);
			var sin:Number = Math.abs(Math.sin(degRad));
			var cos:Number = Math.abs(Math.cos(degRad));
			var maskCunkShape:Shape = new Shape();
			maskCunkShape.graphics.beginFill(0x00FF00);
			maskCunkShape.graphics.moveTo(0, 0);
			maskCunkShape.graphics.lineTo(radius, 0);
			maskCunkShape.graphics.lineTo(cos*radius, radius-(sin*radius));
			maskCunkShape.graphics.lineTo(0, radius);
			maskCunkShape.graphics.lineTo(0, 0);
			maskCunkShape.graphics.endFill();
			return maskCunkShape
		}
		public function get bitmapData():BitmapData
		{
			if(_bitmapData){
				_bitmapData.dispose();
			}
			_bitmapData = new BitmapData(this.width, this.height, true, 0xFFFFFF);
			_bitmapData.draw(this, null, null, null, null, true);
			return _bitmapData;
		}

	}
}