package com.yomstar.app.ui.mobile.Styles
{
	import com.yomstar.app.ui.mobile.comps.IconnedTextInput;
	import com.yomstar.app.ui.mobile.comps.TransButton;
	import com.yomstar.app.ui.mobile.renderers.SampleItemRenderer;
	import com.yomstar.app.ui.mobile.renderers.SurveyItemRenderer;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.controls.Button;
	import feathers.controls.renderers.BaseDefaultItemRenderer;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.display.Scale9Image;
	import feathers.skins.ImageStateValueSelector;
	import feathers.skins.Scale9ImageStateValueSelector;
	import feathers.text.BitmapFontTextFormat;
	import feathers.textures.Scale9Textures;
	import feathers.themes.AzureMobileTheme;
	
	import flash.geom.Rectangle;
	import flash.text.engine.FontWeight;
	
	import starling.display.BlendMode;
	import starling.display.DisplayObjectContainer;
	
	public class YomstarMobileTheme extends AzureMobileTheme
	{
		protected var blankTextures:Scale9Textures;
		protected static const BLANK_SCALE_9_GRID:Rectangle = new Rectangle(1, 1, 1, 1);
		public function YomstarMobileTheme(root:DisplayObjectContainer, scaleToDPI:Boolean=true)
		{
			super(root, scaleToDPI);
		}

		public function get appScale():Number
		{
			return this.scale;
		}

		override protected function headerButtonInitializer(button:Button):void
		{
			super.headerButtonInitializer(button);
			button.defaultLabelProperties.textFormat = TextFormats.getTextFormat(TextFormats.HEADER_BUTTON_TEXT_FORMAT);
			button.defaultSelectedLabelProperties.textFormat = TextFormats.getTextFormat(TextFormats.HEADER_BUTTON_TEXT_FORMAT);
		}
		override protected function initialize():void
		{
			super.initialize();
			this.bitmapFont.dispose();
			this.blankTextures = new Scale9Textures(R.getAtlas().getTexture("BlankTile"), BLANK_SCALE_9_GRID);
			this.bitmapFont = TextFormats.getDefaultFont();
			this.setInitializerForClass(IconnedTextInput, iconnedIextInputInitializer);
			this.setInitializerForClass(SurveyItemRenderer, surveyRendererInitializer);
			this.setInitializerForClass(SampleItemRenderer, sampleRendererInitializer);
			this.setInitializerForClass(TransButton, transButtonInitializer);
			
		}
		protected function transButtonInitializer(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = blankTextures;
			skinSelector.defaultSelectedValue = buttonDownSkinTextures;
			skinSelector.setValueForState(buttonDownSkinTextures, Button.STATE_DOWN, false);
			skinSelector.setValueForState(blankTextures, Button.STATE_DISABLED, false);
			skinSelector.imageProperties =
				{
					width: 66 * this.scale,
						height: 66 * this.scale,
						textureScale: this.scale
				};
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont, this.fontSize, PRIMARY_TEXT_COLOR);
			button.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont, this.fontSize, SELECTED_TEXT_COLOR);
			
			button.paddingTop = button.paddingBottom = 8 * this.scale;
			button.paddingLeft = button.paddingRight = 16 * this.scale;
			button.gap = 12 * this.scale;
			button.minWidth = button.minHeight = 66 * this.scale;
			button.minTouchWidth = button.minTouchHeight = 88 * this.scale;
		}
		protected function iconnedIextInputInitializer(input:IconnedTextInput):void
		{//http://feathersui.com/documentation/feathers/controls/text/StageTextTextEditor.html
			input.minWidth = input.minHeight = 66 * this.scale;
			input.minTouchWidth = input.minTouchHeight = 66 * this.scale;
			input.paddingLeft = input.paddingRight = 16 * this.scale;
			input.textEditorProperties.fontFamily = "Helvetica";
			input.textEditorProperties.fontWeight = FontWeight.BOLD;
			input.textEditorProperties.autoCorrect = false;
			input.textEditorProperties.fontSize = GlobalAppValues.TEXT_INPUT_FONT_SIZE * this.scale;
			input.textEditorProperties.color = GlobalAppValues.FEATURED_COLOR_ONE;
			input.paddingTop = 0.5*(GlobalAppValues.TEXT_INPUT_HEIGHT-input.textEditorProperties.fontSize) * this.scale;
			const backgroundSkin:Scale9Image = new Scale9Image(insetBackgroundSkinTextures, this.scale);
			backgroundSkin.width = 264 * this.scale;
			backgroundSkin.height = 66 * this.scale;
			input.backgroundSkin = backgroundSkin;
			const backgroundDisabledSkin:Scale9Image = new Scale9Image(insetBackgroundDisabledSkinTextures, this.scale);
			backgroundDisabledSkin.width = 264 * this.scale;
			backgroundDisabledSkin.height = 66 * this.scale;
			input.backgroundDisabledSkin = backgroundDisabledSkin;
		}
		
		override protected function tabInitializer(tab:Button):void
		{
			const skinSelector:ImageStateValueSelector = new ImageStateValueSelector();
			skinSelector.defaultValue = R.getAtlas().getTexture("TabBarDown");
			skinSelector.defaultSelectedValue = tabSelectedSkinTexture;
			skinSelector.setValueForState(tabSelectedSkinTexture, Button.STATE_DOWN, false);
			skinSelector.imageProperties =
				{
					width: 88 * this.scale,
						height: 88 * this.scale
				};
			tab.stateToSkinFunction = skinSelector.updateValue;
			
			tab.minWidth = tab.minHeight = 88 * this.scale;
			tab.minTouchWidth = tab.minTouchHeight = 88 * this.scale;
			tab.paddingTop = tab.paddingRight = tab.paddingBottom =
				tab.paddingLeft = 16 * this.scale;
			tab.gap = 12 * this.scale;
			tab.iconPosition = Button.ICON_POSITION_TOP;
			
			tab.defaultLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont, this.fontSize, GlobalAppValues.WHITE_COLOR);
			tab.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(bitmapFont, this.fontSize, GlobalAppValues.FEATURED_COLOR_ONE);
		}
		protected function surveyRendererInitializer(renderer:SurveyItemRenderer):void
		{
			const skinSelector:ImageStateValueSelector = new ImageStateValueSelector();
			skinSelector.defaultValue = listItemUpTexture;
			skinSelector.defaultSelectedValue = listItemDownTexture;
			skinSelector.setValueForState(listItemDownTexture, SurveyItemRenderer.STATE_DOWN, false);
			skinSelector.imageProperties =
				{
					width: 88 * this.scale,
						height: 88 * this.scale,
						blendMode: BlendMode.NONE
				};
			renderer.stateToSkinFunction = skinSelector.updateValue;
			
			renderer.defaultLabelProperties.textFormat = TextFormats.getTextFormat(TextFormats.LIST_TEXT_FORMAT);
			renderer.defaultLabelProperties.wordWrap = true;
			renderer.defaultDetailsLabelProperties.textFormat = TextFormats.getTextFormat(TextFormats.LIST_DETAILS_TEXT_FORMAT);
			renderer.defaultDetailsLabelProperties.wordWrap = true;
			
			//renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			//renderer.paddingTop = renderer.paddingBottom = 11 * this.scale;
			//renderer.paddingLeft = renderer.paddingRight = 20 * this.scale;
			renderer.minWidth = 88 * this.scale;
			renderer.minHeight = 88 * this.scale;
			
			//renderer.gap = 10 * this.scale;
			//renderer.iconPosition = Button.ICON_POSITION_LEFT;
			//renderer.accessoryGap = Number.POSITIVE_INFINITY;
			//renderer.accessoryPosition = BaseDefaultItemRenderer.ACCESSORY_POSITION_RIGHT;
			
			//renderer.accessoryLoaderFactory = this.imageLoaderFactory;
			//renderer.iconLoaderFactory = this.imageLoaderFactory;
		}
		protected function sampleRendererInitializer(renderer:SampleItemRenderer):void
		{
			const skinSelector:ImageStateValueSelector = new ImageStateValueSelector();
			skinSelector.defaultValue = listItemUpTexture;
			skinSelector.defaultSelectedValue = listItemDownTexture;
			skinSelector.setValueForState(listItemDownTexture, SampleItemRenderer.STATE_DOWN, false);
			skinSelector.imageProperties =
				{
					width: 88 * this.scale,
						height: 88 * this.scale,
						blendMode: BlendMode.NONE
				};
			renderer.stateToSkinFunction = skinSelector.updateValue;
			
			renderer.defaultLabelProperties.textFormat = TextFormats.getTextFormat(TextFormats.LIST_TEXT_FORMAT);
			renderer.defaultLabelProperties.wordWrap = true;
			renderer.defaultDetailsLabelProperties.textFormat = TextFormats.getTextFormat(TextFormats.LIST_DETAILS_TEXT_FORMAT);
			renderer.defaultDetailsLabelProperties.wordWrap = true;
			
			//renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			//renderer.paddingTop = renderer.paddingBottom = 11 * this.scale;
			//renderer.paddingLeft = renderer.paddingRight = 20 * this.scale;
			renderer.minWidth = 88 * this.scale;
			renderer.minHeight = 88 * this.scale;
			
			//renderer.gap = 10 * this.scale;
			//renderer.iconPosition = Button.ICON_POSITION_LEFT;
			//renderer.accessoryGap = Number.POSITIVE_INFINITY;
			//renderer.accessoryPosition = BaseDefaultItemRenderer.ACCESSORY_POSITION_RIGHT;
			
			//renderer.accessoryLoaderFactory = this.imageLoaderFactory;
			//renderer.iconLoaderFactory = this.imageLoaderFactory;
		}
	}
}