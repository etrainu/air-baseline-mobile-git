package com.yomstar.app.ui.mobile.screens
{
	import com.yomstar.app.ui.mobile.comps.TransButton;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Screen;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.LocationChangeEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;

	public class WebViewScreen extends YomScreen
	{
		private var webView:StageWebView;
		private var _backButton:TransButton;
		public var url:String;
		public function WebViewScreen()
		{
			super();
		}
		override protected function draw():void
		{
			super.draw();
			if(webView){
				webView.viewPort = new Rectangle(0 , header.height, actualWidth, actualHeight-header.height);
			}
			
		}
		
		override protected function run():void
		{
			trace("running");
			super.run();
			if(!webView){
				var req:URLRequest = Main.urlRequestPool.getObj();
				req.requestHeaders = null;
				req.data = null;
				req.url = url;
				navigateToURL(req, "_blank");
				Main.urlRequestPool.returnObj(req);
				this.dispatchEventWith(starling.events.Event.COMPLETE);
			}
		}
		
		
		override protected function initialize():void
		{	
			super.initialize();
			this.backButtonHandler = this.onBackButton;
			//header.title = "YomStar";
			addChild(header);
			if(StageWebView.isSupported){
				webView = new  StageWebView();
				webView.stage = Starling.current.nativeStage;
				webView.addEventListener(flash.events.Event.COMPLETE, completeHandler);
				webView.addEventListener(ErrorEvent.ERROR, errorHandler);
				webView.addEventListener(LocationChangeEvent.LOCATION_CHANGING, locationChangingHandler);
				webView.addEventListener(LocationChangeEvent.LOCATION_CHANGE, locationChangeHandler);
				webView.loadURL(url);
				webView.title
			}
			this._backButton = new TransButton();
			var beckImage:Image = new Image(R.getAtlas().getTexture("back"));
			this._backButton.defaultIcon = beckImage;
			this._backButton.defaultSelectedIcon = beckImage;
			this._backButton.autoFlatten = true
			this._backButton.addEventListener(starling.events.Event.TRIGGERED, backButton_triggeredHandler);
			this.addChild(this.header);
			this.header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
		}
		private function completeHandler(event:flash.events.Event):void
		{
			//header.title = webView.title;
		}
		
		private function locationChangingHandler(event:LocationChangeEvent):void
		{
			trace("locationChangingHandler: "+webView.location+"\n"+event);
		}
		
		private function locationChangeHandler(event:LocationChangeEvent):void{
			trace("locationChangeHandler: "+webView.location+"\n"+event);
			//header.title = webView.title;
		}
		
		private function errorHandler(event:ErrorEvent):void
		{
			alert.title = "Error loading page:";
			alert.message = event.text;
			alert.closeLabel = "OK";
			alert.show();
		}
		private function backButton_triggeredHandler(event:starling.events.Event):void
		{
			this.onBackButton();
		}
		private function onBackButton():void
		{
			if(webView){
				webView.stage = null;
				webView.dispose();
			}
			webView.removeEventListener(flash.events.Event.COMPLETE, completeHandler);
			webView.removeEventListener(ErrorEvent.ERROR, errorHandler);
			webView.removeEventListener(LocationChangeEvent.LOCATION_CHANGING, locationChangingHandler);
			webView.removeEventListener(LocationChangeEvent.LOCATION_CHANGE, locationChangeHandler);
			this.dispatchEventWith(starling.events.Event.COMPLETE);
			
		}
		
	}
}