package com.yomstar.app.ui.mobile.comps
{
	import starling.animation.IAnimatable;
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.utils.deg2rad;
	
	public class RocketImage extends Image implements IAnimatable
	{
		public function RocketImage(texture:Texture)
		{
			super(texture);
		}
		override public function set rotation(value:Number):void
		{
			super.rotation = deg2rad(value);
		}
		public function advanceTime(time:Number):void
		{
		}
	}
}