package com.yomstar.app.ui.mobile.screens
{
	import com.yomstar.app.events.AuthenticationEvent;
	import com.yomstar.app.services.Authenticator;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.comps.BusyIndicator;
	import com.yomstar.app.ui.mobile.comps.IconnedTextInput;
	import com.yomstar.app.ui.mobile.comps.Spacer;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.ObjectDigger;
	import com.yomstar.app.utils.ScaleUtils;
	import com.yomstar.app.utils.ValidationCheck;
	
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.Screen;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.controls.TextInput;
	import feathers.controls.popups.VerticalCenteredPopUpContentManager;
	import feathers.core.PopUpManager;
	import feathers.layout.VerticalLayout;
	import feathers.system.DeviceCapabilities;
	
	import flash.data.EncryptedLocalStore;
	import flash.events.Event;
	import flash.text.SoftKeyboardType;
	import flash.utils.ByteArray;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.Texture;
	
	
	public class UserLogin extends YomScreen
	{ 
		private const UNCOMPLETE_FORM:String = "Please complete all fields\nbefore submitting";
		private const INVALIDE_EMAIL:String = "Provided email in not in valid format";
		private const LOCAL_STORAGE_USER_KEY:String = "userName";
		private const LOCAL_STORAGE_PASSWORD_KEY:String = "password";
		private const VERTICAL_GAP:uint = 20;
		private const PADDING_TOP:uint = 50;
		private const PADDING_RIGHT:uint = 50;
		private const PADDING_LEFT:uint = 50;
		private const PADDING_BOTTOM:uint = 10;
		
		public var autoLogin:Boolean;
		private var _emailInput:IconnedTextInput;
		private var _submitButton:Button;
		private var _passwordInput:IconnedTextInput;
		private var _spacer:Spacer;
		private var _yomStarBtn:Button;
		private var authenticator:Authenticator;
		private var _container:ScrollContainer;
		public var token:String = "";
		public function UserLogin()
		{
			super();
			authenticator = new Authenticator();
		}
		
		
		

		override protected function draw():void
		{
			super.draw();
			this._emailInput.width = this._passwordInput.width = this._submitButton.width = this.actualWidth*0.7;
			this._container.y = this.header.height;
			this._container.width = this.actualWidth;
			this._container.height = this.actualHeight - this._container.y;
			this._spacer.height = (this.actualHeight - this._container.y - this._emailInput.height - this._passwordInput.height - this._submitButton.height-this.initalImage.height -(VERTICAL_GAP*6))*0.5;
			
		}
		
		override protected function run():void
		{
			
		}
		
		
		override protected function initialize():void
		{
			super.initialize();
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = VERTICAL_GAP;
			layout.paddingTop = PADDING_TOP;
			layout.paddingRight = PADDING_RIGHT;
			layout.paddingBottom = PADDING_BOTTOM;
			layout.paddingLeft = PADDING_LEFT;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			
			this._container = new ScrollContainer();
			this._container.layout = layout;
			this._container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._container.scrollerProperties.snapScrollPositionsToPixels = true;
			
			this._emailInput = new IconnedTextInput();
			this._emailInput.height = GlobalAppValues.TEXT_INPUT_HEIGHT;
			var iconImage:Image = new Image(R.getAtlas().getTexture("Icon_Human"));
			iconImage.color = GlobalAppValues.FEATURED_COLOR_ONE;
			iconImage.scaleX = iconImage.scaleY = Math.min(1, ((GlobalAppValues.TEXT_INPUT_HEIGHT-10)/iconImage.height));
			this._emailInput.icon = iconImage;
			//http://feathersui.com/documentation/feathers/controls/text/StageTextTextEditor.html
			
			this._emailInput.textEditorProperties = {softKeyboardType:SoftKeyboardType.EMAIL, color:GlobalAppValues.FEATURED_COLOR_ONE, autoCorrect:false};
			this._passwordInput = new IconnedTextInput();
			this._passwordInput.textEditorProperties = {displayAsPassword:true, color:GlobalAppValues.FEATURED_COLOR_ONE, autoCorrect:false};
			this._passwordInput.height = GlobalAppValues.TEXT_INPUT_HEIGHT;
			iconImage = new Image(R.getAtlas().getTexture("Icon_Password"));
			iconImage.color = GlobalAppValues.FEATURED_COLOR_ONE;
			iconImage.scaleX = iconImage.scaleY = Math.min(1, ((GlobalAppValues.TEXT_INPUT_HEIGHT-10)/iconImage.height));
			this._passwordInput.icon = iconImage;
			this._submitButton = new Button();
			this._submitButton.defaultIcon = new Image(R.getAtlas().getTexture("LoginIcon"));
			this._submitButton.height = GlobalAppValues.TEXT_INPUT_HEIGHT;;
			this._spacer = new Spacer();
			this._container.addChild(_emailInput);
			this._container.addChild(_passwordInput);
			this._container.addChild(_submitButton);
			this._container.addChild(_spacer);
			addChild(header);
			this.header.rightItems = new <DisplayObject>
				[
				];
			this.header.leftItems = new <DisplayObject>
			[
				
			];
			this.addChild(this._container);
			this._submitButton.addEventListener(starling.events.Event.TRIGGERED, _submitButton_triggeredHandler);
			
			if(retrieveLogin() && autoLogin){
				_submitButton_triggeredHandler();
			}
		}
		private function retrieveLogin():Boolean{
			var storedUser:ByteArray = EncryptedLocalStore.getItem(LOCAL_STORAGE_USER_KEY); 
			var storedPass:ByteArray = EncryptedLocalStore.getItem(LOCAL_STORAGE_PASSWORD_KEY);
			if(storedUser){
				if(storedUser.toString()){
					_emailInput.text = storedUser.toString();
				}
			}
			if(storedPass){
				if(storedPass.toString()){
					this._passwordInput.text = storedPass.toString();
				}
			}
			var hasRecords:Boolean = (this._emailInput.text != "" && this._passwordInput.text != "");
			return hasRecords
		}
		private function _submitButton_triggeredHandler(event:starling.events.Event = null):void
		{
			if(_emailInput.text != "" && _passwordInput.text != ""){
				if(ValidationCheck.validateEmail(_emailInput.text)){
					var bytes:ByteArray = new ByteArray(); 
					bytes.writeUTFBytes(_emailInput.text); 
					EncryptedLocalStore.setItem(LOCAL_STORAGE_USER_KEY, bytes, false);
					bytes = null;
					bytes = new ByteArray();
					bytes.writeUTFBytes(_passwordInput.text); 
					EncryptedLocalStore.setItem(LOCAL_STORAGE_PASSWORD_KEY, bytes, false); 
					busyIndicator.start();
					authenticator.addEventListener(AuthenticationEvent.LOGIN_SUCCESS, onLoginSuccess, false, 0 , true);
					authenticator.addEventListener(AuthenticationEvent.LOGIN_FAILURE, onLoginFailure, false, 0 , true);
					authenticator.checkLogin(_emailInput.text, _passwordInput.text);
					disableLoginUI();
				}else{
					alertCallout(INVALIDE_EMAIL);
				}
			}else{
				alertCallout(UNCOMPLETE_FORM);
			}
		}
		protected function disableLoginUI():void{
			_submitButton.isEnabled = false;
			_emailInput.isEnabled = false;
			_passwordInput.isEnabled = false;
		}
		protected function enableLoginUI():void{
			_submitButton.isEnabled = true;
			_emailInput.isEnabled = true;
			_passwordInput.isEnabled = true;
		}
		private function alertCallout(message:String):void{
			const content:Label = new Label();
			content.text = message;
			var direction:String;
			if(actualWidth < actualHeight){
				direction = Callout.DIRECTION_UP;
			}else{
				direction = Callout.ARROW_POSITION_LEFT;
			}
			Callout.show(DisplayObject(content), _submitButton, direction);
		}
		private function onLoginFailure(e:AuthenticationEvent):void
		{
			trace("onLoginFailure: "+e.toString());
			busyIndicator.stop();
			if(e.message.length>0){
				alertCallout(e.message);
			}
			enableLoginUI();
		}
		private function onLoginSuccess(e:AuthenticationEvent):void
		{
			token = e.token;
			busyIndicator.stop(dispatchSuccess);
			
		}
		private function dispatchSuccess():void{
			
			this.dispatchEventWith("loginSuccess", false, {token:token});
		}
	}
}