package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.resources.R;
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	import flash.geom.Rectangle;
	import starling.display.Sprite;
	import starling.textures.Texture;

	public class LightPad extends FeathersControl
	{
		private var _image:Scale9Image;
		public function LightPad()
		{
			
		}
		override protected function initialize():void
		{
			const textures:Scale9Textures = new Scale9Textures(R.getAtlas().getTexture("Pad"), new Rectangle(7, 7, 7, 7));
			this._image = new Scale9Image(textures);
			this.addChild(this._image);
		}
		override protected function draw():void
		{
			this._image.width = this.actualWidth;
			this._image.height = this.actualHeight;
		}
	}
}