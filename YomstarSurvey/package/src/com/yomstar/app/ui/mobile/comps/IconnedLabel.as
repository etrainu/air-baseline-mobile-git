package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.utils.ObjectDigger;
	
	import feathers.controls.Label;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalLayout;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	public class IconnedLabel extends ScrollContainer
	{
		private var _icon:FeathersControl;
		private var _label:Label;
		private var _textRendererProperties:Object;
		private var _paddingLeft:int = 0;
		private var _paddingRight:int = 0;
		private var _text:String;
		private var _gap:int = 10;
		private var _initialized:Boolean;
		public function IconnedLabel()
		{
			_initialized = false;
			super();
		}
		
		public function get gap():int
		{
			return _gap;
		}

		public function set gap(value:int):void
		{
			_gap = value;
		}

		public function get text():String
		{
			return _text;
		}

		public function set text(value:String):void
		{
			_text = value;
			if(this._label){
				this._label.text = _text;
				this._label.validate();
			}
			draw();
		}

		public function get paddingRight():int
		{
			return _paddingRight;
			draw();
		}

		public function set paddingRight(value:int):void
		{
			_paddingRight = value;
			draw();
		}

		public function get paddingLeft():int
		{
			return _paddingLeft;
			draw();
		}

		public function set paddingLeft(value:int):void
		{
			_paddingLeft = value;
			draw();
		}

		public function get textRendererProperties():Object
		{
			if(!_textRendererProperties){
				if(this._label){
					_textRendererProperties = this._label.textRendererProperties;
				}else{
					_textRendererProperties = {};
				}
			}
			return _textRendererProperties;
		}

		public function set textRendererProperties(value:Object):void
		{
			_textRendererProperties = value;
			this._label.textRendererProperties = _textRendererProperties;
			this._label.invalidate(INVALIDATION_FLAG_SIZE);
			this._label.invalidate(INVALIDATION_FLAG_STYLES);
			this.invalidate(INVALIDATION_FLAG_SIZE);
			this.invalidate(INVALIDATION_FLAG_STYLES);
		}

		public function get icon():FeathersControl
		{
			return _icon;
		}

		public function set icon(value:FeathersControl):void
		{
			this._icon = value;
			trace("this._icon: "+this._icon);
			this.setSizeInternal(actualWidth, Math.max(this._icon.height, this._label.height), true);
			this.invalidate(INVALIDATION_FLAG_DATA);
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
			this.invalidate(INVALIDATION_FLAG_SIZE);
		}
		
		override public function get height():Number
		{
			// TODO Auto Generated method stub
			var _height:Number;
			if(_icon){
				_height = Math.max((this._label.y+this._label.height), (this._icon.y+this._icon.height));
			}else{
				_height = this._label.y+this._label.height;
			}
			
			return Math.max(_height,super.height);
		}
		
		override protected function draw():void
		{
			super.draw();
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const styleInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STYLES);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			if(dataInvalid)
			{
				this.commitData();
			}
			if(this._label){
				this._label.validate();
			}
			if(this._icon)
			{
				this._icon.validate();
			}
			if(styleInvalid || stateInvalid)
			{
				this.applyStyle();
			}
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			if(sizeInvalid){
				if(this.parent is FeathersControl){
					FeathersControl(this.parent).invalidate(FeathersControl.INVALIDATION_FLAG_SIZE);
				}
			}
			
			if(!_initialized){
				_initialized = true;
				this._label.textRendererProperties= _textRendererProperties;
				this._label.validate();
				this.flatten();
			}
			if(this._icon){
				this._icon.validate();
			}
			if(_label){
				if(this._icon){
					this._label.width = actualWidth-gap-this._icon.width;
				}else{
					this._label.width = actualWidth;
				}
				_label.validate();
			}
			this.flatten();
		}
		
		override protected function autoSizeIfNeeded():Boolean
		{
			super.autoSizeIfNeeded();
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			if(this._label){
				this._label.width = NaN;
				this._label.height = NaN;
				this._label.validate();
			}
			if(this._icon)
			{
				this._icon.width = NaN;
				this._icon.height = NaN;
				this._icon.validate();
			}
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				if(this._label && this._icon){
					newWidth = this._label.width+gap+this._icon.width;
				}else if(this._label){
					newWidth = this._label.width;
				}else if(this._icon){
					newWidth = this._icon.width;
				}
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
				if(this._label && this._icon){
					newHeight = Math.max(this._label.height, this._icon.height);
				}else if(this._label){
					newHeight = this._label.height;
				}else if(this._icon){
					newHeight = this._icon.height;
				}
			}
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		
		protected function applyStyle():void
		{
			if(this._label){
				this._label.textRendererProperties= textRendererProperties;
			}
		}
		protected function commitData():void
		{
			if(this._icon)
			{
				this.addChildAt(this._icon, 0);
			}
		}
		
		
		override protected function initialize():void
		{
			super.initialize();
			this.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			this.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this.scrollerProperties.snapScrollPositionsToPixels = true;
			this.touchable = false;
			
			var layout:HorizontalLayout = new HorizontalLayout();
			layout.gap = gap;
			layout.paddingTop = 0;
			layout.paddingRight = 0;
			layout.paddingBottom = 0;
			layout.paddingLeft = 0;
			layout.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_LEFT;
			layout.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_MIDDLE;
			this.layout = layout;
			
			_label = new Label();
			this._label.textRendererProperties.wordWrap = true;
			if(_text){
				this._label.text = _text;
			}
			this._label.textRendererProperties= textRendererProperties;
			addChild(_label);
		}
		
		
	}
}