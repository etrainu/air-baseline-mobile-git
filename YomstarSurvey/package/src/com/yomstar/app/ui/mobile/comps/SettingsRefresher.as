package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.DateFormat;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.core.FeathersControl;
	
	import flash.text.TextFormatAlign;
	
	import starling.display.Image;
	import starling.events.Event;
	
	public class SettingsRefresher extends FeathersControl
	{
		public static const REFRESH_REQUEST:String = "refreshRequest";
		private const REFRESH_LABEL_TEXT:String = "Refresh data";
		private const HORIZENTAL_GAP:uint = 10;
		private const VERTICAL_GAP:uint = 3;
		private var _refreshBtn:Button;
		private var _refreshLabel:Label;
		private var _initialized:Boolean;
		private var _dateText:Label;
		public function SettingsRefresher()
		{
			super();
			_initialized = false;
		}
		
		override protected function draw():void
		{
			super.draw();
			if(!_initialized){
				_initialized = true;
				this._refreshLabel.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._dateText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT)};
				this._dateText.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
			}
			
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const styleInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STYLES);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			if(dataInvalid)
			{
				this.commitData();
			}
			this._refreshBtn.validate();
			this._refreshLabel.validate();
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			if(dataInvalid || sizeInvalid)
			{
				this.layout();
			}
		}
		protected function layout():void{
			this._dateText.x = this._refreshLabel.x = this._refreshBtn.x+this._refreshBtn.width+HORIZENTAL_GAP;
			this._dateText.width = this._refreshLabel.width = this.actualWidth-this._refreshLabel.x;
			this._refreshLabel.validate();
			this._dateText.y = _refreshLabel.y+_refreshLabel.height+VERTICAL_GAP;
		}
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			this._refreshBtn.width = NaN;
			this._refreshBtn.height = NaN;
			this._refreshBtn.validate();
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
					newWidth = this._refreshBtn.width;
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
					newHeight = Math.max(this._refreshBtn.height, 1);
			}
			trace("newHeight: "+newHeight);
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		protected function commitData():void
		{
			var currentDate:Date = new Date();
			_dateText.text = "("+DateFormat.stringify(currentDate.date)+"/"+DateFormat.stringify(Number(currentDate.month+1))+"/"+DateFormat.stringify(currentDate.fullYear)+" "+DateFormat.stringify(currentDate.hours)+":"+DateFormat.stringify(currentDate.minutes)+")";
		}
		override protected function initialize():void
		{
			// TODO Auto Generated method stub
			super.initialize();
			this._refreshBtn = new Button();
			this._refreshBtn.defaultIcon = new Image(R.getAtlas().getTexture("RefreshIcon"));
			this._refreshBtn.addEventListener(Event.TRIGGERED, refresh_triggeredHandler);
			this._refreshBtn.width = this._refreshBtn.height = GlobalAppValues.USER_AVATAR_SIZE;
			this.addChild(this._refreshBtn);
			
			this._refreshLabel = new Label();
			this._refreshLabel.text = REFRESH_LABEL_TEXT;
			this.addChild(this._refreshLabel);
			this._dateText = new Label();
			this.addChild(this._dateText);
		}
		private function refresh_triggeredHandler(e:Event):void{
			this.invalidate(INVALIDATION_FLAG_DATA);
			this.dispatchEventWith(REFRESH_REQUEST);
		}
		
	}
}