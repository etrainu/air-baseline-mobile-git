package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.textures.Texture;

	public class HighLightPad extends FeathersControl
	{
		private var _image:Scale9Image;
		private var _color:Number;
		public function HighLightPad(color:Number = GlobalAppValues.FEATURED_COLOR_ONE)
		{
			this._color = color;
		}
		override protected function initialize():void
		{
			const textures:Scale9Textures = new Scale9Textures(R.getAtlas().getTexture("Pad"), new Rectangle(7, 7, 7, 7));
			this._image = new Scale9Image(textures);
			this._image.color = this._color;
			this.addChild(this._image);
		}
		override protected function draw():void
		{
			this._image.width = this.actualWidth;
			this._image.height = this.actualHeight;
		}
	}
}