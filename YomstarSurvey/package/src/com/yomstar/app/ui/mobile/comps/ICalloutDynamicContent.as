package com.yomstar.app.ui.mobile.comps
{
	import starling.display.DisplayObject;

	public interface ICalloutDynamicContent
	{
		function get disposable():Boolean;
		function set disposable(value:Boolean):void;
		function get orig():DisplayObject;
		function set orig(value:DisplayObject):void;
		function dispatchCompResize():void;
	}
}