package com.yomstar.app.ui.mobile.comps
{
	import feathers.controls.TextInput;
	
	import starling.display.DisplayObject;
	
	public class IconnedTextInput extends TextInput
	{
		private var _icon:DisplayObject;
		public function IconnedTextInput()
		{
			super();
		}
		
		public function get icon():DisplayObject
		{
			return _icon;
		}

		public function set icon(value:DisplayObject):void
		{
			this._icon = value;
			if(this._icon){
				addChild(this._icon);
				this.initialize();
			}
		}
		override protected function draw():void
		{
			super.draw();
			if(this._icon){
				this._icon.x = this._icon.y = (actualHeight-this._icon.height)*0.5;
				this.paddingLeft = this._icon.x+this._icon.width +this.paddingRight;
			}
			this.paddingTop = 0.5*(actualHeight-this.textEditorProperties.fontSize);
		}
		
		override protected function initialize():void
		{
			super.initialize();
		}
		
		
	}
}