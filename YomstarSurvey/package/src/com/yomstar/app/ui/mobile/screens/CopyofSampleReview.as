package com.yomstar.app.ui.mobile.screens
{
	import com.yomstar.app.config.QuestionType;
	import com.yomstar.app.config.ResponseType;
	import com.yomstar.app.dataObjects.FreeTextQuestionData;
	import com.yomstar.app.dataObjects.FreeTextResponseData;
	import com.yomstar.app.dataObjects.MultiChoiceQuestionData;
	import com.yomstar.app.dataObjects.MultiChoiceResponseData;
	import com.yomstar.app.dataObjects.QuestionData;
	import com.yomstar.app.dataObjects.ResponseData;
	import com.yomstar.app.dataObjects.SampleDetail;
	import com.yomstar.app.dataObjects.ScaledQuestionData;
	import com.yomstar.app.dataObjects.ScaledResponseData;
	import com.yomstar.app.services.BaselineDetails;
	import com.yomstar.app.services.SampleDetails;
	import com.yomstar.app.services.UserInfo;
	import com.yomstar.app.services.UserSurvey;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.comps.HighLightPad;
	import com.yomstar.app.ui.mobile.comps.ListSurveysCalloutButton;
	import com.yomstar.app.ui.mobile.comps.QuestionFreeTextPreview;
	import com.yomstar.app.ui.mobile.comps.QuestionMultiChoicePreview;
	import com.yomstar.app.ui.mobile.comps.QuestionScaledPreview;
	import com.yomstar.app.ui.mobile.comps.SamplePageHeader;
	import com.yomstar.app.ui.mobile.comps.TransButton;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.MatchResponse;
	import com.yomstar.app.utils.ObjectDigger;
	
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.PageIndicator;
	import feathers.controls.Screen;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	import feathers.skins.StandardIcons;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class CopyfSampleReview extends YomScreen
	{
		private const VERTICAL_GAP:uint = 10;
		private const PADDING_TOP:uint = 20;
		private const PADDING_RIGHT:uint = 20;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 20;
		private const CALLOUT_WIDTH:uint = 350;
		private const CALLOUT_HEIGHT:uint = 500;
		
		private var _container:ScrollContainer;
		private var _pageIndicator:PageIndicator;
		public var sampleObject:Object;
		private var _sampleDetails:SampleDetail;
		private var _baselineObject:Object;
		private var _backButton:TransButton;
		
		private var _surveyQuestions:Vector.<QuestionData>;
		private var _surveyResponses:Vector.<ResponseData>;
		public function CopyfSampleReview()
		{
			super();
			_surveyQuestions = new Vector.<QuestionData>();
			_surveyResponses = new Vector.<ResponseData>();
		}
		
		override protected function draw():void
		{
			super.draw();
			this._pageIndicator.width = actualWidth;
			this._pageIndicator.validate();
			this._pageIndicator.y = actualHeight-_pageIndicator.height-PADDING_BOTTOM;
			this._container.y = this.header.height;
			this._container.width = this.actualWidth;
			this._container.height = this.actualHeight-this._container.y-(this.actualHeight - _pageIndicator.y)-VERTICAL_GAP;
			this._container.validate();
			const layout:VerticalLayout = VerticalLayout(this._container.layout);
			var realEstateWidth:int = this._container.width - layout.paddingLeft - layout.paddingRight;
			var len:uint = this._container.numChildren;
			for(var i:int = 0; i<len; i++){
				this._container.getChildAt(i).width = realEstateWidth;
			}
		}
		
		override protected function initialize():void
		{
			super.initialize();
			_surveyQuestions.length = 0;
			_surveyResponses.length = 0;
			
			
			this._backButton = new TransButton();
			var beckImage:Image = new Image(R.getAtlas().getTexture("back"));
			this._backButton.defaultIcon = beckImage;
			this._backButton.defaultSelectedIcon = beckImage;
			this._backButton.autoFlatten = true;
			//ObjectDeigger.digg("	defaultLabelProperties: "+this._backButton.	defaultLabelProperties);
			this._backButton.addEventListener(starling.events.Event.TRIGGERED, backButton_triggeredHandler);
			this.addChild(this.header);
			this.header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
			this.backButtonHandler = this.onBackButton;
			
			this._container = new ScrollContainer();
			//this._container.backgroundSkin = new HighLightPad();
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = VERTICAL_GAP;
			layout.paddingTop = PADDING_TOP;
			layout.paddingRight = PADDING_RIGHT;
			layout.paddingBottom = PADDING_BOTTOM;
			layout.paddingLeft = PADDING_LEFT;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_CENTER;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			this._container.layout = layout;
			this._container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._container.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this._container.scrollerProperties.snapScrollPositionsToPixels = true;
			
			this.addChild(this._container);
			
			this._pageIndicator = new PageIndicator();
			this._pageIndicator.pageCount = 1;
			this._pageIndicator.addEventListener(starling.events.Event.CHANGE, pageIndicator_changeHandler);
			this.addChild(this._pageIndicator);
		}
		override protected function run():void{
			super.run();
			var sampleDetails:SampleDetails = new SampleDetails();
			sampleDetails.addEventListener(flash.events.Event.COMPLETE, onSampleDetails, false, 0, true);
			sampleDetails.getDetails(Main.data.token, sampleObject.externalId);
		}
		private function pageIndicator_changeHandler(event:starling.events.Event):void
		{
			_container.removeChildren(0, -1, true);
			addPage(this._pageIndicator.selectedIndex);
		}
		private function onSampleDetails(event:flash.events.Event):void
		{
			var sampleDetails:SampleDetails = event.target as SampleDetails;
			sampleDetails.removeEventListener(flash.events.Event.COMPLETE, onSampleDetails);
			//sample response json: {"id":"XqKNipO8Q0qNIlGzd8iBAg","baseline":"kCYdTJbTZKdMjdMiCxBvQ","versionNumber":"1","latitude":"-27.623404435","longitude":"153.115903435","isGeolocated":"true","ipAddress":"127.0.0.1","dateCreated":"2012-12-18T09:11:37","averagePercentageScore":"50","answers":[{"type":"freetextanswer","question":{"id":"I0NT7RoEQaOF1FY2M1j7lA","text":"Free Text Question"},"score":"null","response":"Dv"},{"type":"slideranswer","question":{"id":"lybgUD9kTx2a72Z1QqxPEQ","text":"Scaled Question"},"score":"50","response":{"id":"JUzIuJgzSByTSaxW7JiJWg","desc":"Acceptable"}},{"type":"multichoiceanswer","question":{"id":"Sovi5e3WTCu1inxEWxch4Q","text":"MultiChoice Quetion"},"score":"null","responses":[{"id":"Nwsk2JdATOmTQvoPSz2MSw","desc":"Choice 2"}]}]}
			//Sample rewarded json: {"id":"VAV3vizLS1OEdjXNnaYRyQ","theme":"darkGreenPalette","participant":"uqqfv04WReklgw7eUT6mQ","rewardCode":"DLV8EN0N","rewardScheme":"A5xUf6llS5CDfCZ8U745xA","baseline":"efO5bI9SA6WLUZdY04Dtg","versionNumber":"2","latitude":"-27.623291","longitude":"153.1157756","isGeolocated":"true","ipAddress":"127.0.0.1","dateCreated":"2013-01-11T17:59:00","answers":[{"type":"freetextanswer","question":{"id":"56g05SjZSjqWkRE3Vy23GA","text":"Logo?"},"score":"null","response":""}]}
			//sample response: baseline:WJGosSJaRU2KuPBank1bCg, isGeolocated:true, id:woluGR1hSlOJoGN3pEi9Ng, answers:[object Object],[object Object], latitude:-27.470933, dateCreated:2012-12-11T16:57:14, longitude:153.02350199999998, averagePercentageScore:54, versionNumber:1, ipAddress:127.0.0.1
			trace("SampleDetails.sampleDetails: "+SampleDetails.sampleDetails);
			_sampleDetails = SampleDetails.sampleDetails;
			getBaselineDetails();
		}
		private function getBaselineDetails():void{
			var baselineDetails:BaselineDetails = new BaselineDetails();
			baselineDetails.addEventListener(flash.events.Event.COMPLETE, onBaselineDetails, false, 0, true);
			baselineDetails.addEventListener(ErrorEvent.ERROR, onBaselineError, false, 0, true);
			baselineDetails.getSurveyById(Main.data.token, SampleDetails.sampleDetails.baseline);
			
		}
		private function onBaselineError(event:flash.events.Event):void
		{
			var baselineDetails:BaselineDetails = event.target as BaselineDetails;
			baselineDetails.removeEventListener(flash.events.Event.COMPLETE, onBaselineDetails);
			baselineDetails.removeEventListener(ErrorEvent.ERROR, onBaselineError);
			alert.title = "Survey not found";
			alert.message = "The survey you are asking for is not accessible. It might have been deleted by the owner.";
			alert.closeLabel = "OK";
			alert.show();
		}
		private function onBaselineDetails(event:flash.events.Event):void
		{
			var baselineDetails:BaselineDetails = event.target as BaselineDetails;
			baselineDetails.removeEventListener(flash.events.Event.COMPLETE, onBaselineDetails);
			baselineDetails.removeEventListener(ErrorEvent.ERROR, onBaselineError);
			//sample response json:  {"id":"kCYdTJbTZKdMjdMiCxBvQ","createdByAccountName":"Amir's company","name":"Survey 2","dateCreated":"2012-12-17T12:32:28","createdBy":{"id":"1yTw4Y5nTwGJOUY7Ib0Cdw","name":"Amir Zahedi"},"versionNumber":"1","isPublic":"false","isReleased":"true","allowAnonymous":"true","account":{"id":"bjS7OO13TiKrOe5OFioxKw","name":"Amir's company"},"hasRecommendations":"false","publicResults":"true","shareURI":"kCYdTJbTZKdMjdMiCxBvQ","shortURL":"http://goo.gl/vVsyj","captureParticipants":"false","notifyResults":"false","notifyThresholdPercentage":"100","pages":[{"id":"ai0oVtDkRDyCitjRRkWeCg","pageTitle":"Full Survey","order":"1","componentGroups":[{"id":"FSMHKOdVRwKzQYZwpEjA","groupName":"","order":"0","components":[{"type":"freetextquestion","id":"I0NT7RoEQaOF1FY2M1j7lA","questionText":"Free Text Question","order":"0","isAutoMark":"false","isRequired":"false","allowMediaCapture":"false","options":[],"metrics":[]},{"type":"sliderquestion","id":"lybgUD9kTx2a72Z1QqxPEQ","questionText":"Scaled Question","order":"0","isRequired":"false","allowMediaCapture":"false","options":[{"type":"slideroption","id":"8dytkhXeQoCLkJgGHpAmw","description":"Very Bad","value":"0","isDefault":"false"},{"type":"slideroption","id":"xMuGpRkrRfanAc28SgorXw","description":"Bad","value":"25","isDefault":"false"},{"type":"slideroption","id":"JUzIuJgzSByTSaxW7JiJWg","description":"Acceptable","value":"50","isDefault":"false"},{"type":"slideroption","id":"LjzR3np2SaY9fFb7YpTJQ","description":"Good","value":"75","isDefault":"false"},{"type":"slideroption","id":"JukeMjNPSjyCsQt0HhPkNw","description":"Very Good","value":"100","isDefault":"false"}],"metrics":[]},{"type":"multichoicequestion","id":"Sovi5e3WTCu1inxEWxch4Q","questionText":"MultiChoice Quetion","order":"0","isMulti":"false","isRequired":"false","allowOther":"false","allowMediaCapture":"false","scoreCalculationMethod":"AVERAGE","options":[{"type":"multichoiceoption","id":"AWlf1kvsTEqWbPvQv6oHcg","description":"Choice 1","value":"0"},{"type":"multichoiceoption","id":"Nwsk2JdATOmTQvoPSz2MSw","description":"Choice 2"},{"type":"multichoiceoption","id":"f7TQ0HoXTCoQbfoQI0Wrg","description":"Choice 3","value":"75"},{"type":"multichoiceoption","id":"x9vVX00eQ0qte4tuGDiIfQ","description":"Choice 4","value":"100"}],"metrics":[]}]}]}]}
			//sample response: createdBy:[object Object], hasRecommendations:false, publicResults:true, account:[object Object], name:Survey 2, captureParticipants:false, isPublic:false, id:kCYdTJbTZKdMjdMiCxBvQ, notifyResults:false, isReleased:true, versionNumber:1, shortURL:http://goo.gl/vVsyj, allowAnonymous:true, notifyThresholdPercentage:100, dateCreated:2012-12-17T12:32:28, createdByAccountName:Amir's company, pages:[object Object], shareURI:kCYdTJbTZKdMjdMiCxBvQ
			_baselineObject = BaselineDetails.baselineInfo;
			//trace("onBaselineDetails: "+ObjectDeigger.digg(baselineObject));
			
			generateAnswereObjects();
			diplayBaselinePreview();
		}
		private function generateAnswereObjects():void{
			_surveyResponses.length = 0;
			var answers:Array = _sampleDetails.answers as Array;
			var len:int = answers.length;
			var freeResponseData:FreeTextResponseData;
			var multiChoiceResponseData:MultiChoiceResponseData;
			var scaledResponseData:ScaledResponseData;
			for(var i:int = 0; i<len; i++){
				switch(answers[i].type){
					case ResponseType.FREE_TEXT_QUESTION_RESPONSE:
						freeResponseData = new FreeTextResponseData(answers[i]);
						_surveyResponses.push(freeResponseData);
						
						break
					case ResponseType.MULTI_CHOICE_QUESTION_RESPONSE:
						multiChoiceResponseData = new MultiChoiceResponseData(answers[i]);
						_surveyResponses.push(multiChoiceResponseData);
						break
					case ResponseType.SLIDER_QUESTION_RESPONSE:
						scaledResponseData = new ScaledResponseData(answers[i]);
						_surveyResponses.push(scaledResponseData);
						break
					case ResponseType.MEDIA_QUESTION_RESPONSE:
						trace("A MEDIA RESPONSE");
						break
					default:
						throw new Error(answers[i].type+" is not a recognized question type");
						break
				}
			}
		}
		private function diplayBaselinePreview():void{
			header.title = String(_baselineObject.name);
			this._pageIndicator.pageCount = _baselineObject.pages.length;
			addPage(0);
		}
		private function addPage(pageNumber:uint):void{
			generateQuestionObjectsForPage(pageNumber);
			var sampleHeader:SamplePageHeader = new SamplePageHeader(_sampleDetails, String(_baselineObject.name), _baselineObject.pages[pageNumber].pageTitle, pageNumber);
			_container.addChild(sampleHeader);
			var freeTextQ:QuestionFreeTextPreview;
			var multiChoiceQ:QuestionMultiChoicePreview;
			var scaledQ:QuestionScaledPreview;
			var freeResponseData:FreeTextResponseData;
			var multiChoiceResponseData:MultiChoiceResponseData;
			var scaledResponseData:ScaledResponseData;
			var responseDataIndex:int;
			var len:int = _surveyQuestions.length;
			for(var i:int = 0; i<len; i++){
				switch(_surveyQuestions[i].type){
					case QuestionType.FREE_TEXT_QUESTION:
						responseDataIndex  = MatchResponse.getAnsIndexById(_surveyQuestions[i].id, _surveyResponses);
						if(responseDataIndex > -1){
							freeResponseData = _surveyResponses[responseDataIndex] as FreeTextResponseData;
						}else{
							freeResponseData = null;
						}
						freeTextQ = new QuestionFreeTextPreview(_surveyQuestions[i] as FreeTextQuestionData, freeResponseData, true);
						_container.addChild(freeTextQ);
						break
					case QuestionType.MULTI_CHOICE_QUESTION:
						responseDataIndex  = MatchResponse.getAnsIndexById(_surveyQuestions[i].id, _surveyResponses);
						if(responseDataIndex > -1){
							multiChoiceResponseData = _surveyResponses[responseDataIndex] as MultiChoiceResponseData;
						}else{
							multiChoiceResponseData = null;
						}
						multiChoiceQ = new QuestionMultiChoicePreview(_surveyQuestions[i] as MultiChoiceQuestionData, multiChoiceResponseData, true);
						_container.addChild(multiChoiceQ);
						break
					case QuestionType.SLIDER_QUESTION:
						responseDataIndex  = MatchResponse.getAnsIndexById(_surveyQuestions[i].id, _surveyResponses);
						if(responseDataIndex > -1){
							scaledResponseData = _surveyResponses[responseDataIndex] as ScaledResponseData;
						}else{
							scaledResponseData = null;
						}
						scaledQ = new QuestionScaledPreview(_surveyQuestions[i] as ScaledQuestionData, scaledResponseData, true);
						_container.addChild(scaledQ);
						break
					default:
						throw new Error(_surveyQuestions[i].type+" is not a recognized question type");
						break
				}
			}
			draw();
		}
		private function generateQuestionObjectsForPage(pageNumber:uint):void{
			_surveyQuestions.length = 0;
			var page:Object = _baselineObject.pages[pageNumber];
			var components:Array = page.componentGroups[0].components as Array;
			var len:int = components.length;
			var freeQuestionData:FreeTextQuestionData;
			var multiChoiceQuestionData:MultiChoiceQuestionData;
			var scaledQuestionData:ScaledQuestionData;
			for(var i:int = 0; i<len; i++){
				switch(components[i].type){
					case QuestionType.FREE_TEXT_QUESTION:
						freeQuestionData = new FreeTextQuestionData(components[i]);
						_surveyQuestions.push(freeQuestionData);
						
						break
					case QuestionType.MULTI_CHOICE_QUESTION:
						multiChoiceQuestionData = new MultiChoiceQuestionData(components[i]);
						_surveyQuestions.push(multiChoiceQuestionData);
						break
					case QuestionType.SLIDER_QUESTION:
						scaledQuestionData = new ScaledQuestionData(components[i]);
						_surveyQuestions.push(scaledQuestionData);
						break
					default:
						throw new Error(components[i].type+" is not a recognized question type");
						break
				}
			}
		}
		private function addQuestion():void{
			
		}
		private function onBackButton():void
		{
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function backButton_triggeredHandler(event:starling.events.Event):void
		{
			this.onBackButton();
		}
		
	}
}