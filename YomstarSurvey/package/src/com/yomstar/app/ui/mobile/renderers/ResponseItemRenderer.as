package com.yomstar.app.ui.mobile.renderers
{
	
	import com.yomstar.app.config.QuestionType;
	import com.yomstar.app.dataObjects.ResponseCellData;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.comps.HighLightPad;
	import com.yomstar.app.ui.mobile.comps.LightPad;
	import com.yomstar.app.ui.mobile.comps.SampleDetailsExtension;
	import com.yomstar.app.ui.mobile.renderers.ResponseCells.MediaResponseCell;
	import com.yomstar.app.ui.mobile.renderers.ResponseCells.ResponseCaptureCell;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.Mood;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.ScrollContainer;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.FeathersControl;
	import feathers.core.IFeathersControl;
	import feathers.skins.StateWithToggleValueSelector;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class ResponseItemRenderer extends FeathersControl implements IListItemRenderer
	{
		private static const HELPER_POINT:Point = new Point();
		protected static const DOWN_STATE_DELAY_MS:int = 250;
		private const VERTICAL_GAP:uint = 5;
		private const HORIZENTAL_GAP:uint = 10;
		private const PADDING_TOP:uint = 10;
		private const PADDING_RIGHT:uint = 10;
		private const PADDING_LEFT:uint = 10;
		private const PADDING_BOTTOM:uint = 10;
		//private const BACKGROUND_COLOR:Number = 0x13171a;
		private const BACKGROUND_COLOR:Number = 0x330000;
		protected var _defaultLabelProperties:Object;
		protected var _defaultDetailsLabelProperties:Object;
		protected var currentCellModule:FeathersControl;
		protected var captureCell:ResponseCaptureCell;
		protected var mediaCell:MediaResponseCell;
		protected var _index:int = -1;
		protected var touchPointID:int;
		protected var ownerScrolled:Boolean;
		protected var _owner:List;
		protected var _isSelected:Boolean;
		protected var _data:ResponseCellData;
		protected var _cellBG:Quad;
		public function ResponseItemRenderer()
		{
			super();
			this._defaultLabelProperties = new Object();
			this._defaultDetailsLabelProperties = new Object();
			this.touchPointID = -1;
			this.addEventListener(TouchEvent.TOUCH, touchHandler);
			this.addEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
			ownerScrolled = false;
		}
		public function get index():int
		{
			return this._index;
		}
		
		public function set index(value:int):void
		{
			if(this._index == value)
			{
				return;
			}
			this._index = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		public function get owner():List
		{
			return List(this._owner);
		}
		
		public function set owner(value:List):void
		{
			if(this._owner == value)
			{
				return;
			}
			if(this._owner)
			{
				this._owner.removeEventListener(Event.SCROLL, owner_scrollHandler);
			}
			this._owner = value;
			if(this._owner)
			{
				this._owner.addEventListener(Event.SCROLL, owner_scrollHandler);
			}
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		
		public function get data():Object
		{
			return this._data;
		}
		
		public function set data(value:Object):void
		{
			if(this._data == value)
			{
				return;
			}
			this._data = value as ResponseCellData;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		public function get isSelected():Boolean
		{
			return this._isSelected;
		}
		
		public function set isSelected(value:Boolean):void
		{
			if(this._isSelected == value)
			{
				return;
			}
			this._isSelected = value;
			this.invalidate(INVALIDATION_FLAG_SELECTED);
			this.dispatchEventWith(Event.CHANGE);
		}
		
		
		
		override protected function initialize():void
		{
			super.initialize();
			
			if(!this._cellBG){
				this._cellBG = new Quad(10, 10, BACKGROUND_COLOR);
				this._cellBG.touchable = true;
				this.addChild(this._cellBG);
			}
			this.invalidate();
		}
		
		override protected function draw():void
		{
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const selectionInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SELECTED);
			const styleInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STYLES);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			if(dataInvalid)
			{
				this.commitData();
			}
			//this.itemLabel.validate();
			//this.itemDetailLabel.validate();
			if(styleInvalid || stateInvalid || selectionInvalid)
			{
				//this.applyStyle();
			}
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			if(dataInvalid || sizeInvalid)
			{
				this.layout();
			}
			
		}
		
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			//this.itemLabel.width = NaN;
			//this.itemLabel.height = NaN;
			//this.itemLabel.validate();
			//this.itemDetailLabel.width = NaN;
			//this.itemDetailLabel.height = NaN;
			//this.itemDetailLabel.validate();
			
			if(currentCellModule)
			{
				currentCellModule.validate();
			}

			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				if(currentCellModule){
					if(currentCellModule.width){
						newWidth = currentCellModule.width;
					}else{
						newWidth = _cellBG.width;
					}
				}else{
					newWidth = _cellBG.width;
				}
				
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
				if(currentCellModule){
					trace("currentCellModule.height: "+currentCellModule.height);
					if(currentCellModule.height){
						newHeight = currentCellModule.height;
					}else{
						newHeight = _cellBG.height;
					}
					
				}else{
					newHeight = _cellBG.height;
				}
			}
			trace("newHeight: "+newHeight);
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		protected function commitData():void
		{
			if(this._data)
			{
				if(this._data.capture){
					setupCaptureCell();
					currentCellModule = captureCell;
					addChild(currentCellModule);
					this.invalidate(INVALIDATION_FLAG_SIZE);
				}else if(this._data.question){
					switch(this._data.question.type){
						case QuestionType.FREE_TEXT_QUESTION:
							
							break
						case QuestionType.MULTI_CHOICE_QUESTION:
							
							break
						case QuestionType.SLIDER_QUESTION:
							
							break
						case QuestionType.MEDIA_QUESTION:
							setupMediaCell();
							currentCellModule = mediaCell;
							addChild(currentCellModule);
							this.invalidate(INVALIDATION_FLAG_SIZE);
							break
						default:
							throw new Error(this._data.question+" is not a recognized question type");
							break
					}
				}
			}
			else
			{
				if(captureCell){
					captureCell.reset();
				}
				if(mediaCell){
					mediaCell.reset();
				}
				//this.itemLabel.text = "";
				
				this._cellBG.color = BACKGROUND_COLOR;
			}
		}
		protected function setupCaptureCell():void{
			if(this.captureCell){
				this.captureCell.reset();
			}else{
				this.captureCell = new ResponseCaptureCell();
			}
			this.captureCell.captureData = this._data.capture;
		}
		protected function setupMediaCell():void{
			if(this.mediaCell){
				this.mediaCell.reset();
			}else{
				this.mediaCell = new MediaResponseCell();
			}
			this.mediaCell.questionData = this._data.question;
			this.mediaCell.responseData = this._data.ans;
		}
		protected function layout():void
		{
			this._cellBG.height = this.actualHeight;
			this._cellBG.width = this.actualWidth;
			if(this.currentCellModule){
				this.currentCellModule.width = this.actualWidth;
			}
			//this.itemDetailLabel.x = this.itemLabel.x = PADDING_LEFT+this._cellBG.width+HORIZENTAL_GAP;
			//this.itemLabel.y = PADDING_TOP;
			//this.itemLabel.validate();
			//this.itemDetailLabel.y = this.itemLabel.y+this.itemLabel.height+VERTICAL_GAP;
		}
		protected function removedFromStageHandler(event:Event):void
		{
			this.touchPointID = -1;
		}
		protected function owner_scrollHandler(event:Event):void
		{
			this.ownerScrolled = true;
		}
		protected function touchHandler(event:TouchEvent):void
		{
			const touches:Vector.<Touch> = event.getTouches(this);
			
			if(touches.length == 0)
			{
				
				return;
			}
			var touch:Touch = touches[0];
			/*if(touch.phase == TouchPhase.BEGAN)
			{
			trace("TouchPhase.BEGAN");
			this.ownerScrolled = false;
			trace("ownerScrolled: "+ownerScrolled);
			return;
			}*/
			if(this.touchPointID >= 0)
			{
				touch.getLocation(this, HELPER_POINT);
				var isInBounds:Boolean = this.hitTest(HELPER_POINT, true) != null;
				for each(var currentTouch:Touch in touches)
				{
					if(currentTouch.id == this.touchPointID)
					{
						touch = currentTouch;
						break;
					}
				}
				
				if(!touch)
				{
					return;
				}
				if(touch.phase == TouchPhase.BEGAN)
				{
					this.touchPointID = touch.id;
					this.ownerScrolled = false;
					
					return;
				}
				if(touch.phase == TouchPhase.ENDED)
				{
					this.touchPointID = -1;
					
					if(!this.ownerScrolled)
					{
						//check if the touch is still over the target
						//also, only change it if we're not selected. we're not a toggle.
						if(isInBounds && !this._isSelected)
						{
							this.isSelected = true;
						}
					}
					if(!isInBounds)
					{
					}
					return;
				}
			}
			else
			{
				for each(touch in touches)
				{
					if(touch.phase == TouchPhase.BEGAN)
					{
						this.ownerScrolled = false;
						this.touchPointID = touch.id;
						return;
					}
					else if(touch.phase == TouchPhase.HOVER)
					{
						break;
					}
				}
			}
		}
		
		override public function dispose():void
		{
			//if(this.extensionContainer){
			//	this.extensionContainer.dispose();
			//}
			super.dispose();
		}
		
		
	}
}