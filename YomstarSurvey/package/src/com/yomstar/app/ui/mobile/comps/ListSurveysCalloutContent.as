package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.dataObjects.Baseline;
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.events.ImageLoaderEvent;
	import com.yomstar.app.services.StaticMapManager;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.layout.VerticalLayout;
	
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	

	
	
	public class ListSurveysCalloutContent extends FeathersControl implements ICalloutDynamicContent
	{
		private var _orig:DisplayObject;
		private var _disposable:Boolean = true;
		private const VERTICAL_GAP:uint = 10;
		private const PADDING_TOP:uint = 20;
		private const PADDING_RIGHT:uint = 10;
		private const PADDING_LEFT:uint = 10;
		private const PADDING_BOTTOM:uint = 10;
		private var _baseline:Baseline;
		private var _detailsTxt:Label = new Label();
		private var _container:ScrollContainer;
		private var _w:int;
		private var _h:int;
		private var _preInitialized:Boolean;
		private var _image:Image;
		public function ListSurveysCalloutContent()
		{
			super();
			_preInitialized = false;
		}

		public function get disposable():Boolean
		{
			return _disposable;
		}

		public function set disposable(value:Boolean):void
		{
			_disposable = value;
		}

		public function get orig():DisplayObject
		{
			return _orig;
		}

		public function set orig(value:DisplayObject):void
		{
			_orig = value;
		}
		public function dispatchCompResize():void{
			this.dispatchEventWith("compResized");
		}
		public function init(baseline:Baseline, w:int, h:int):void{
			this._w = w;
			this._h = h;
			this._baseline = baseline;
			if(_preInitialized){
				this.unflatten();
				initialize();
				this.flatten();
			}
		}
		override protected function draw():void
		{
			_detailsTxt.width = this.actualWidth - PADDING_LEFT - PADDING_RIGHT;
			_detailsTxt.validate();
			this._container.width = this.actualWidth;
			this._container.height = this.actualHeight;
			layOut();
		}
		private function layOut():void{
			this._container.validate();
			const layout:VerticalLayout = VerticalLayout(this._container.layout);
			var contentHeight:uint = layout.paddingTop;
			for(var i:int = 0; i<_container.numChildren; i++){
				contentHeight += _container.getChildAt(i).height+layout.gap;
			}
			contentHeight += layout.paddingTop-layout.gap;
			this.height = this._container.height = Math.min(maxHeight, contentHeight);
		}
		override protected function initialize():void
		{
			_preInitialized = true;
			this.setSize(this._w, this._h);
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = VERTICAL_GAP;
			layout.paddingTop = PADDING_TOP;
			layout.paddingRight = PADDING_RIGHT;
			layout.paddingBottom = PADDING_BOTTOM;
			layout.paddingLeft = PADDING_LEFT;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_CENTER;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_MIDDLE;
			
			this._container = new ScrollContainer();
			this._container.layout = layout;
			this._container.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._container.scrollerProperties.snapScrollPositionsToPixels = true;
			this._container.addChild(this._detailsTxt);
			this._detailsTxt.textRendererProperties.wordWrap = true;
			this._detailsTxt.textRendererProperties.isHTML = true;
			this._detailsTxt.textRendererProperties.textFormat= TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT);
			var detailsStr:String = this._baseline.name;
			detailsStr = detailsStr.concat("\rCreated by: "+this._baseline.creatorName);
			detailsStr = detailsStr.concat("\rCreated on: "+String(this._baseline.dateCreated).replace("T", " at "));
			detailsStr = detailsStr.concat("\rURL: "+this._baseline.shortURL);
			detailsStr = detailsStr.concat("\rTotal responses: "+this._baseline.sampleCount);
			detailsStr = detailsStr.concat("\rLast response received on : "+String(this._baseline.lastSample).replace("T", " at "));
			detailsStr = detailsStr.concat("\rFirst response received on : "+String(this._baseline.firstSample).replace("T", " at "));
			this._detailsTxt.text = detailsStr;
			addChild(this._container);
		}
		private function onCalloutClosed(e:starling.events.Event):void{
			if(disposable){
			trace("onCalloutClosed");
				_image.dispose();
			}
		}
	}
}