package com.yomstar.app.ui.mobile.resources
{
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	
	import starling.core.Starling;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class R
	{
		private static var textures:Dictionary=new Dictionary();
		private static var appTextureAtlas:TextureAtlas;
		//spritesheet image class
		[Embed(source="../../../../../../assets/ui/uiSheet.png")]
		public static const AppAtlas:Class;
		
		//xml/data class
		[Embed(source="../../../../../../assets/ui/uiSheet.xml", mimeType="application/octet-stream")]
		public static const AppAtlasXML:Class;
		
		//return a texture atlas, or create one if none exists
		public static function getAtlas(scale:Number=1):TextureAtlas{
			if(appTextureAtlas==null){
				var texture:Texture=getTexture("AppAtlas", scale);
				var xml:XML=XML(new AppAtlasXML());
				appTextureAtlas=new TextureAtlas(texture, xml);
			}
			return appTextureAtlas;
		}
		//return a named texture using the textures dictionary, or create the named texture if none exists
		public static function getTexture(name:String, scale:Number=1):Texture{
			if (R[name] != undefined)
			{
				if(textures[name]==undefined){
					var bitmap:Bitmap=new R[name]();
					textures[name]=Texture.fromBitmap(bitmap, true, false, Math.min(Starling.current.contentScaleFactor, scale));
				}
				return textures[name];
			}else throw new Error("Resource not defined.");
		}
		
	}
}