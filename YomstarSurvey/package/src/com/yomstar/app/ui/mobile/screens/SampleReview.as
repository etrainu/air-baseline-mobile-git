package com.yomstar.app.ui.mobile.screens
{
	import com.yomstar.app.config.QuestionType;
	import com.yomstar.app.config.ResponseType;
	import com.yomstar.app.dataObjects.FreeTextQuestionData;
	import com.yomstar.app.dataObjects.FreeTextResponseData;
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.dataObjects.MediaQuestionData;
	import com.yomstar.app.dataObjects.MediaResponseData;
	import com.yomstar.app.dataObjects.MultiChoiceQuestionData;
	import com.yomstar.app.dataObjects.MultiChoiceResponseData;
	import com.yomstar.app.dataObjects.Participant;
	import com.yomstar.app.dataObjects.QuestionData;
	import com.yomstar.app.dataObjects.ResponseCaptureData;
	import com.yomstar.app.dataObjects.ResponseCellData;
	import com.yomstar.app.dataObjects.ResponseData;
	import com.yomstar.app.dataObjects.Reward;
	import com.yomstar.app.dataObjects.RewardScheme;
	import com.yomstar.app.dataObjects.SampleDetail;
	import com.yomstar.app.dataObjects.ScaledQuestionData;
	import com.yomstar.app.dataObjects.ScaledResponseData;
	import com.yomstar.app.services.BaselineDetails;
	import com.yomstar.app.services.ParticipantDetails;
	import com.yomstar.app.services.RewardDetails;
	import com.yomstar.app.services.RewardSchemes;
	import com.yomstar.app.services.SampleDetails;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.comps.BottomShadowedLine;
	import com.yomstar.app.ui.mobile.comps.FeathersImage;
	import com.yomstar.app.ui.mobile.comps.IconnedLabel;
	import com.yomstar.app.ui.mobile.comps.TransButton;
	import com.yomstar.app.ui.mobile.renderers.ResponseItemRenderer;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.MatchResponse;
	import com.yomstar.app.utils.ObjectDigger;
	
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Scroller;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	
	import flash.events.ErrorEvent;
	import flash.text.TextFormatAlign;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	
	public class SampleReview extends YomScreen
	{
		private const TEXTLINE_VERTICAL_GAP:uint = 5;
		private const VERTICAL_GAP:uint = 20;
		private const HORIZENTAL_GAP:uint = 20;
		private const PADDING_TOP:uint = 20;
		private const PADDING_RIGHT:uint = 20;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 20;
		private const SURVEY_NAME_BG_COLOR:Number = 0xcb2028;
		private const INVALIDATION_FLAG_SAMPLE_DETAIL:String = "sampleDetailInvalid";
		private const INVALIDATION_FLAG_BASELINE_DETAIL:String = "baselineDetailInvalid";
		private const INVALIDATION_FLAG_AVATAR:String = "avatarInvalid";
		private const INVALIDATION_FLAG_MAP:String = "mapInvalid";
		private const INVALIDATION_FLAG_PARTICIPANT:String = "participantInvalid";
		private const INVALIDATION_FLAG_REWARDSCHEME:String = "rewardSchemeInvalid";
		private var _initialized:Boolean;
		private var _surveyNameText:Label;
		private var _surveyNameBg:Quad;

		private var _backButton:TransButton;
		private var _list:List;
		private var _responseCells:Vector.<ResponseCellData>;
		private var _surveyQuestions:Vector.<QuestionData>;
		private var _surveyResponses:Vector.<ResponseData>;
		private var _listdb:ListCollection;
		public var sampleObject:Object;
		private var _sampleDetails:SampleDetail;
		private var _baselineObject:Object;
		private var _participantDetails:Participant;
		private var _rewardScheme:RewardScheme;
		private var _reward:Reward;
		public function SampleReview()
		{
			super();
			_initialized = false;
		}
		override protected function draw():void
		{
			super.draw();
			if(!_initialized){
				_initialized = true;
				this._surveyNameText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.DEFAULT_TEAXT_FORMAT_BOLD)};
				this._surveyNameText.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
			}
			
			var realestateHight:int = this.actualHeight;
			var realestateWidth:int = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const sampleInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SAMPLE_DETAIL);
			const baselineInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_BASELINE_DETAIL);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			if(dataInvalid)
			{
				this.commitData();
			}
			if(sampleInvalid){
				commitSampleData();
			}
			if(baselineInvalid){
				commitBaselineData();
			}
			this.header.validate();
			this._surveyNameText.validate();
			if(dataInvalid || sizeInvalid || baselineInvalid)
			{
				this.layout();
			}
		}
		protected function commitData():void
		{
			trace("commitData");
			
		}
		protected function commitSampleData():void{
			trace("commitSampleData");
		}
		protected function commitBaselineData():void{
			trace("commitBaselineData");
			if(this._baselineObject){
				_surveyNameText.text = String(_baselineObject.name);
			}
		}
		protected function layout():void{
			trace("Laying out");
			this._surveyNameBg.x = 0;
			this._surveyNameBg.y = this.header.height;
			this._surveyNameBg.width = actualWidth;
			this._surveyNameBg.height = this.header.height*0.8;
			this._surveyNameText.width = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			this._surveyNameText.validate();
			this._surveyNameText.x = actualWidth-PADDING_RIGHT-_surveyNameText.width;
			this._surveyNameText.y = this._surveyNameBg.y+(0.5*(this._surveyNameBg.height-this._surveyNameText.height));
			this._surveyNameText.validate();
			this._list.width = actualWidth;
			this._list.y = this._surveyNameBg.y + this._surveyNameBg.height;
			this._list.height = actualHeight-this._list.y;
		}
		override protected function initialize():void
		{
			super.initialize();
			this._backButton = new TransButton();
			var beckImage:Image = new Image(R.getAtlas().getTexture("back"));
			this._backButton.defaultIcon = beckImage;
			this._backButton.defaultSelectedIcon = beckImage;
			this._backButton.autoFlatten = true
			this._backButton.addEventListener(starling.events.Event.TRIGGERED, backButton_triggeredHandler);
			this.addChild(this.header);
			this.header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
			this.backButtonHandler = this.onBackButton;
			this._surveyNameText = new Label();
			this._surveyNameBg = new Quad(10, 10, SURVEY_NAME_BG_COLOR, true);
			this.addChild(this._surveyNameBg);
			this.addChild(this._surveyNameText);
			if(_surveyQuestions){
				_surveyQuestions.length = 0;
			}else{
				_surveyQuestions = new Vector.<QuestionData>();
			}
			if(_surveyResponses){
				_surveyResponses.length = 0;
			}else{
				_surveyResponses = new Vector.<ResponseData>();
			}
			_list = new List();
			this._list.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._list.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this._list.scrollerProperties.snapScrollPositionsToPixels = true;
			//this._list.addEventListener(starling.events.Event.CHANGE, listChanged);
			this.addChild(this._list);
			this._list.itemRendererType = ResponseItemRenderer;
		}
		override protected function run():void{
			super.run();
			busyIndicator.start();
			refresh();
		}
		override public function refresh():void
		{
			
			super.refresh();
			getSampleDetails();
		}
		private function getSampleDetails():void{
			var sampleDetails:SampleDetails = new SampleDetails();
			sampleDetails.addEventListener(flash.events.Event.COMPLETE, onSampleDetails, false, 0, true);
			sampleDetails.getDetails(Main.data.token, sampleObject.externalId);
		}
		private function onSampleDetails(event:flash.events.Event):void
		{
			var sampleDetails:SampleDetails = event.target as SampleDetails;
			sampleDetails.removeEventListener(flash.events.Event.COMPLETE, onSampleDetails);
			//sample response json: {"id":"XqKNipO8Q0qNIlGzd8iBAg","baseline":"kCYdTJbTZKdMjdMiCxBvQ","versionNumber":"1","latitude":"-27.623404435","longitude":"153.115903435","isGeolocated":"true","ipAddress":"127.0.0.1","dateCreated":"2012-12-18T09:11:37","averagePercentageScore":"50","answers":[{"type":"freetextanswer","question":{"id":"I0NT7RoEQaOF1FY2M1j7lA","text":"Free Text Question"},"score":"null","response":"Dv"},{"type":"slideranswer","question":{"id":"lybgUD9kTx2a72Z1QqxPEQ","text":"Scaled Question"},"score":"50","response":{"id":"JUzIuJgzSByTSaxW7JiJWg","desc":"Acceptable"}},{"type":"multichoiceanswer","question":{"id":"Sovi5e3WTCu1inxEWxch4Q","text":"MultiChoice Quetion"},"score":"null","responses":[{"id":"Nwsk2JdATOmTQvoPSz2MSw","desc":"Choice 2"}]}]}
			//Sample rewarded json: {"id":"VAV3vizLS1OEdjXNnaYRyQ","theme":"darkGreenPalette","participant":"uqqfv04WReklgw7eUT6mQ","rewardCode":"DLV8EN0N","rewardScheme":"A5xUf6llS5CDfCZ8U745xA","baseline":"efO5bI9SA6WLUZdY04Dtg","versionNumber":"2","latitude":"-27.623291","longitude":"153.1157756","isGeolocated":"true","ipAddress":"127.0.0.1","dateCreated":"2013-01-11T17:59:00","answers":[{"type":"freetextanswer","question":{"id":"56g05SjZSjqWkRE3Vy23GA","text":"Logo?"},"score":"null","response":""}]}
			//sample response: baseline:WJGosSJaRU2KuPBank1bCg, isGeolocated:true, id:woluGR1hSlOJoGN3pEi9Ng, answers:[object Object],[object Object], latitude:-27.470933, dateCreated:2012-12-11T16:57:14, longitude:153.02350199999998, averagePercentageScore:54, versionNumber:1, ipAddress:127.0.0.1
			trace("SampleDetails.sampleDetails: "+SampleDetails.sampleDetails);
			_sampleDetails = SampleDetails.sampleDetails;
			this.invalidate(INVALIDATION_FLAG_SAMPLE_DETAIL);
			getParticipantDetails(_sampleDetails.participant);
		}
		private function getParticipantDetails(participantId:String):void{
			trace("getParticipantDetails for "+participantId);
			var participantDetails:ParticipantDetails = new ParticipantDetails();
			participantDetails.addEventListener(flash.events.Event.COMPLETE, onParticipantDetails, false, 0, true);
			participantDetails.getParticipant(Main.data.token, participantId);
		}
		private function onParticipantDetails(event:flash.events.Event):void
		{
			var participantDetails:ParticipantDetails = event.target as ParticipantDetails;
			participantDetails.removeEventListener(flash.events.Event.COMPLETE, onSampleDetails);
			trace("participantDetails.participantDetails Ext: "+ParticipantDetails.participantDetails);
			//SAMPLE RESULT: {"id":"veO1JqtnSvCMJu5TR7sJyg","firstName":"no","lastName":"display","email":"tomj89%33@hotmail.com","directMarketing":false,"dateCreated":"2013-01-24T16:54:51","account":{"id":"AU9Rp2qgT2OjccEINWJQ","name":"bree's company"}}
			this._participantDetails = ParticipantDetails.participantDetails;
			this.invalidate(INVALIDATION_FLAG_PARTICIPANT);
			getRewardScheme(SampleDetails.sampleDetails.rewardCode);
		}
		
		private function getRewardScheme(rewardSchemeId:String):void{
			trace("getRewardScheme for "+rewardSchemeId);
			var rewardScheme:RewardSchemes = new RewardSchemes();
			rewardScheme.addEventListener(flash.events.Event.COMPLETE, onRewardScheme, false, 0, true);
			rewardScheme.getRewardSchemeById(Main.data.token, rewardSchemeId);
		}
		private function onRewardScheme(event:flash.events.Event):void
		{
			var rewardScheme:RewardSchemes = event.target as RewardSchemes;
			rewardScheme.removeEventListener(flash.events.Event.COMPLETE, onRewardScheme);
			trace("RewardSchemes.rewardScheme: "+RewardSchemes.rewardScheme);
			//SAMPLE RESULT: {"id":"yWxtxIo1TK3V59kLRYA2Q","name":"Reward 240113","description":"get something for free....","termsAndConditions":"someting","termsAndConditionsURL":"http://www.etrainu.com/","redemptionCount":0,"rewardCount":5,"offerFrom":"2013-01-24T00:00:00","offerUntil":"2013-01-25T23:59:59","redeemFrom":"2013-01-24T00:00:00","redeemUntil":"2013-01-25T23:59:59","dateCreated":"2013-01-24T16:38:56","lastUpdated":"2013-01-24T16:38:56","lastRewardGenerated":"2013-01-24T16:54:00","baseline":"6HkmeFYJRYe2LzVavApb8g","baselineName":"Notifications","createdBy":"MnpPF0BdT065SCX06wSfSg","status":"ACTIVE"}
			this._rewardScheme = RewardSchemes.rewardScheme;
			getReward(SampleDetails.sampleDetails.rewardCode);
			//getReward("NY77I255");
		}
		
		private function getReward(rewardId:String):void{
			trace("getReward for "+rewardId);
			var rewardDetails:RewardDetails = new RewardDetails();
			rewardDetails.addEventListener(flash.events.Event.COMPLETE, onReward, false, 0, true);
			rewardDetails.getRewardDetailsById(Main.data.token, rewardId);
		}
		private function onReward(event:flash.events.Event):void
		{
			var rewardDetails:RewardDetails = event.target as RewardDetails;
			rewardDetails.removeEventListener(flash.events.Event.COMPLETE, onReward);
			trace("RewardDetails.rewardDetails: "+RewardDetails.rewardDetails);
			//SAMPLE RESULT REDEEMED: {"code":"NY77I255","activatedDate":"2013-01-21T16:47:22","dateCreated":"2013-01-21T16:47:03","redeemedDate":"2013-01-22T11:17:31","rewardScheme":"wgg2bCO1QeyFEPYukaUHSQ","sample":"TTHu5ZjQaOg3GSUY1qhg","requiresPin":true,"activatedBy":{"id":"uqqfv04WReklgw7eUT6mQ","firstName":"tom","lastName":"jerry","email":"breesimmons@etrainu.com"},"redeemedBy":{"id":"uqqfv04WReklgw7eUT6mQ","firstName":"tom","lastName":"jerry","email":"breesimmons@etrainu.com"}}
			//SAMPLE RESULT NOT REDEEMED: {"code":"NXJG9A65","activatedDate":"2013-01-24T16:54:51","dateCreated":"2013-01-24T16:54:00","rewardScheme":"yWxtxIo1TK3V59kLRYA2Q","sample":"TWmwey3zT6G5j2ejYM6Bg","requiresPin":true,"activatedBy":{"id":"veO1JqtnSvCMJu5TR7sJyg","firstName":"no","lastName":"display","email":"tomj89%33@hotmail.com"}}
			this._reward = RewardDetails.rewardDetails;
			this.invalidate(INVALIDATION_FLAG_REWARDSCHEME);
			getBaselineDetails();
		}
		private function getBaselineDetails():void{
			var baselineDetails:BaselineDetails = new BaselineDetails();
			baselineDetails.addEventListener(flash.events.Event.COMPLETE, onBaselineDetails, false, 0, true);
			baselineDetails.addEventListener(ErrorEvent.ERROR, onBaselineError, false, 0, true);
			baselineDetails.getSurveyById(Main.data.token, SampleDetails.sampleDetails.baseline);
			
		}
		private function onBaselineError(event:flash.events.Event):void
		{
			var baselineDetails:BaselineDetails = event.target as BaselineDetails;
			baselineDetails.removeEventListener(flash.events.Event.COMPLETE, onBaselineDetails);
			baselineDetails.removeEventListener(ErrorEvent.ERROR, onBaselineError);
			alert.title = "Survey not found";
			alert.message = "The survey you are asking for is not accessible. It might have been deleted by the owner.";
			alert.closeLabel = "OK";
			alert.show();
		}
		private function onBaselineDetails(event:flash.events.Event):void
		{
			var baselineDetails:BaselineDetails = event.target as BaselineDetails;
			baselineDetails.removeEventListener(flash.events.Event.COMPLETE, onBaselineDetails);
			baselineDetails.removeEventListener(ErrorEvent.ERROR, onBaselineError);
			//sample response json:  {"id":"kCYdTJbTZKdMjdMiCxBvQ","createdByAccountName":"Amir's company","name":"Survey 2","dateCreated":"2012-12-17T12:32:28","createdBy":{"id":"1yTw4Y5nTwGJOUY7Ib0Cdw","name":"Amir Zahedi"},"versionNumber":"1","isPublic":"false","isReleased":"true","allowAnonymous":"true","account":{"id":"bjS7OO13TiKrOe5OFioxKw","name":"Amir's company"},"hasRecommendations":"false","publicResults":"true","shareURI":"kCYdTJbTZKdMjdMiCxBvQ","shortURL":"http://goo.gl/vVsyj","captureParticipants":"false","notifyResults":"false","notifyThresholdPercentage":"100","pages":[{"id":"ai0oVtDkRDyCitjRRkWeCg","pageTitle":"Full Survey","order":"1","componentGroups":[{"id":"FSMHKOdVRwKzQYZwpEjA","groupName":"","order":"0","components":[{"type":"freetextquestion","id":"I0NT7RoEQaOF1FY2M1j7lA","questionText":"Free Text Question","order":"0","isAutoMark":"false","isRequired":"false","allowMediaCapture":"false","options":[],"metrics":[]},{"type":"sliderquestion","id":"lybgUD9kTx2a72Z1QqxPEQ","questionText":"Scaled Question","order":"0","isRequired":"false","allowMediaCapture":"false","options":[{"type":"slideroption","id":"8dytkhXeQoCLkJgGHpAmw","description":"Very Bad","value":"0","isDefault":"false"},{"type":"slideroption","id":"xMuGpRkrRfanAc28SgorXw","description":"Bad","value":"25","isDefault":"false"},{"type":"slideroption","id":"JUzIuJgzSByTSaxW7JiJWg","description":"Acceptable","value":"50","isDefault":"false"},{"type":"slideroption","id":"LjzR3np2SaY9fFb7YpTJQ","description":"Good","value":"75","isDefault":"false"},{"type":"slideroption","id":"JukeMjNPSjyCsQt0HhPkNw","description":"Very Good","value":"100","isDefault":"false"}],"metrics":[]},{"type":"multichoicequestion","id":"Sovi5e3WTCu1inxEWxch4Q","questionText":"MultiChoice Quetion","order":"0","isMulti":"false","isRequired":"false","allowOther":"false","allowMediaCapture":"false","scoreCalculationMethod":"AVERAGE","options":[{"type":"multichoiceoption","id":"AWlf1kvsTEqWbPvQv6oHcg","description":"Choice 1","value":"0"},{"type":"multichoiceoption","id":"Nwsk2JdATOmTQvoPSz2MSw","description":"Choice 2"},{"type":"multichoiceoption","id":"f7TQ0HoXTCoQbfoQI0Wrg","description":"Choice 3","value":"75"},{"type":"multichoiceoption","id":"x9vVX00eQ0qte4tuGDiIfQ","description":"Choice 4","value":"100"}],"metrics":[]}]}]}]}
			//sample response: createdBy:[object Object], hasRecommendations:false, publicResults:true, account:[object Object], name:Survey 2, captureParticipants:false, isPublic:false, id:kCYdTJbTZKdMjdMiCxBvQ, notifyResults:false, isReleased:true, versionNumber:1, shortURL:http://goo.gl/vVsyj, allowAnonymous:true, notifyThresholdPercentage:100, dateCreated:2012-12-17T12:32:28, createdByAccountName:Amir's company, pages:[object Object], shareURI:kCYdTJbTZKdMjdMiCxBvQ
			//sample response json 2: {"id":"vnmWNRdTeiFPEFlFB0bg","createdByAccountName":"Testing -in DEV","accountTermsAndConditions":"htERE ARE SOME SPEC T&c'S CUSTOM.","accountTermsAndConditionsURL":"https://www.google.com.au/","name":"Image capture test","dateCreated":"2013-03-06T18:32:51","versionNumber":"1","isPublic":"false","isReleased":"true","allowAnonymous":"true","hasRecommendations":"false","publicResults":"true","shortURL":"http://goo.gl/iT5nJ","captureParticipants":"false","createdBy":{"id":"MnpPF0BdT065SCX06wSfSg","name":"bree dev test"},"account":{"id":"AU9Rp2qgT2OjccEINWJQ","name":"Testing -in DEV"},"pages":[{"id":"xGo7KAFoSUmDlbZJFUzQ","pageTitle":"Image","order":"1","componentGroups":[{"id":"MVhlOMXdRMOjEml5JV9WVQ","groupName":"","order":"0","components":[{"type":"mediaquestion","id":"joI0c0dtRwyzhyQdDibZTQ","questionText":"Test","order":"0","isRequired":"false","allowMediaCapture":"true","mediaSize":"SMALL","options":[]},{"type":"multichoicequestion","id":"UjvrJ5f1TaR3ZrwP4V5QQ","questionText":"Test","order":"0","isMulti":"false","isRequired":"false","allowOther":"false","allowMediaCapture":"false","scoreCalculationMethod":"AVERAGE","mediaSize":"SMALL","options":[{"type":"multichoiceoption","id":"EIiZX8vRo2YfCbgJhHf4w","description":"tTest","value":"75"},{"type":"multichoiceoption","id":"czwbsKOYReS6ddIvZZ77lQ","description":"etest","value":"25"}]}]}]}]}
			_baselineObject = BaselineDetails.baselineInfo;
			this.invalidate(INVALIDATION_FLAG_BASELINE_DETAIL);
			//trace("onBaselineDetails: "+ObjectDeigger.digg(baselineObject));
			busyIndicator.stop();
			generateQuestionObjects();
			generateAnswereObjects();
			generateListData();
			displayResponse();
		}
		private function generateQuestionObjects():void{
			_surveyQuestions.length = 0;
			var components:Array = new Array();
			var page:Object;
			var pageNumber:int = _baselineObject.pages.length-1;
			while(pageNumber >=0){
				page = _baselineObject.pages[pageNumber];
				components = components.concat(page.componentGroups[0].components as Array);
				pageNumber--;
			}
			var len:int = components.length;
			var freeQuestionData:FreeTextQuestionData;
			var multiChoiceQuestionData:MultiChoiceQuestionData;
			var scaledQuestionData:ScaledQuestionData;
			var mediaQuestionData:MediaQuestionData;
			for(var i:int = 0; i<len; i++){
				switch(components[i].type){
					case QuestionType.FREE_TEXT_QUESTION:
						freeQuestionData = new FreeTextQuestionData(components[i]);
						_surveyQuestions.push(freeQuestionData);
						
						break
					case QuestionType.MULTI_CHOICE_QUESTION:
						multiChoiceQuestionData = new MultiChoiceQuestionData(components[i]);
						_surveyQuestions.push(multiChoiceQuestionData);
						break
					case QuestionType.SLIDER_QUESTION:
						scaledQuestionData = new ScaledQuestionData(components[i]);
						_surveyQuestions.push(scaledQuestionData);
						break
					case QuestionType.MEDIA_QUESTION:
						mediaQuestionData = new MediaQuestionData(components[i]);
						_surveyQuestions.push(mediaQuestionData);
						break
					default:
						throw new Error(components[i].type+" is not a recognized question type");
						break
				}
			}
			
		}
		private function generateAnswereObjects():void{
			_surveyResponses.length = 0;
			var answers:Array = _sampleDetails.answers as Array;
			var len:int = answers.length;
			var freeResponseData:FreeTextResponseData;
			var multiChoiceResponseData:MultiChoiceResponseData;
			var scaledResponseData:ScaledResponseData;
			var mediaResponseData:MediaResponseData;
			for(var i:int = 0; i<len; i++){
				switch(answers[i].type){
					case ResponseType.FREE_TEXT_QUESTION_RESPONSE:
						freeResponseData = new FreeTextResponseData(answers[i]);
						_surveyResponses.push(freeResponseData);
						
						break
					case ResponseType.MULTI_CHOICE_QUESTION_RESPONSE:
						multiChoiceResponseData = new MultiChoiceResponseData(answers[i]);
						_surveyResponses.push(multiChoiceResponseData);
						break
					case ResponseType.SLIDER_QUESTION_RESPONSE:
						scaledResponseData = new ScaledResponseData(answers[i]);
						_surveyResponses.push(scaledResponseData);
						break
					case ResponseType.MEDIA_QUESTION_RESPONSE:
						mediaResponseData = new MediaResponseData(answers[i]);
						_surveyResponses.push(mediaResponseData);
						break
					default:
						throw new Error(answers[i].type+" is not a recognized question type");
						break
				}
			}
		}
		private function generateListData():void{
			var cellData:ResponseCellData;
			if(_responseCells){
				if(_responseCells.length>0){
					_responseCells.splice(0, _responseCells.length);
				}
			}else{
				_responseCells = new Vector.<ResponseCellData>();
			}
			var geoData:GeoData;
			if(_sampleDetails.isGeolocated){
				geoData= new GeoData();
				geoData.init(this._sampleDetails.latitude, this._sampleDetails.longitude, GeoData.TILE_FORMAT_PNG, 5);
			}
			cellData = new ResponseCellData(new ResponseCaptureData(this._participantDetails.firstName+" "+this._participantDetails.lastName, this._participantDetails.email, this._participantDetails.phone, this._participantDetails.dateCreated.replace("T", " at "), geoData));
			_responseCells.push(cellData);
			
			var len:uint = this._surveyQuestions.length;
			var matchedResponseIndex:int;
			for(var i:int = 0; i<len; i++){
				matchedResponseIndex = MatchResponse.getAnsIndexById(this._surveyQuestions[i].id, this._surveyResponses);
				trace("matchedResponseIndex: "+matchedResponseIndex);
				if(matchedResponseIndex <0){
					cellData = new ResponseCellData(null, this._surveyQuestions[i], null);
				}else{
					cellData = new ResponseCellData(null, this._surveyQuestions[i], this._surveyResponses[matchedResponseIndex]);
				}
				
				_responseCells.push(cellData);
			}
		}
		private function displayResponse():void{//http://feathersui.com/documentation/feathers/controls/renderers/BaseDefaultItemRenderer.html
			_listdb = new ListCollection(_responseCells);
			trace("_listdb: "+_listdb);
			trace("_listdb.length: "+_listdb.length);
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = 0;
			layout.paddingTop = 0;
			layout.paddingRight = 0;
			layout.paddingBottom = 0;
			layout.paddingLeft = 0;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			layout.hasVariableItemDimensions = true;
			this._list.layout = layout;
			
			//this._list.itemRendererProperties.detailsLabelFunction = listItemDetailsFunction;
			this._list.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._list.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this._list.scrollerProperties.snapScrollPositionsToPixels = true;
			_list.dataProvider = _listdb;
		}
		override public function dispose():void
		{
			super.dispose();
		}
		private function onBackButton():void
		{
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		private function backButton_triggeredHandler(event:starling.events.Event):void
		{
			this.onBackButton();
		}
		
	}
}