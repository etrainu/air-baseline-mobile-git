package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	public class BorderedImage extends FeathersControl
	{
		private var _texture:Texture;
		private var _image:Image;
		private var _borderThickness:uint;
		private var _borderColor:Number
		private var _pad:Scale9Image;
		private var _initialWidth:uint;
		private var _initialHeight:uint;
		public function BorderedImage(texture:Texture = null, borderThickness:uint = 5, borderColor:Number = 0xFFFFFF, initialWidth:uint = 18, initialHeight:uint = 18)
		{
			super();
			this._borderColor = borderColor;
			this._borderThickness = borderThickness;
			this._initialWidth = initialWidth;
			this._initialHeight = initialHeight;
			this._texture = texture;
		}
		
		override protected function draw():void
		{
			super.draw();
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			if(dataInvalid)
			{
				this.commitData();
			}
			
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			if(dataInvalid || sizeInvalid)
			{
				this.layout();
			}
		}
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			//this._image.width = NaN;
			//this._image.height = NaN;
			//this._image.validate();
			
			
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				if(this._image){
					newWidth = this._image.width+(2*this._borderThickness);
				}
				
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
				if(this._image){
					newHeight = this._image.height+(2*this._borderThickness);
				}
			}
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		override protected function initialize():void
		{
			// TODO Auto Generated method stub
			
			super.initialize();
			this.touchable = false;
			const padTextures:Scale9Textures = new Scale9Textures(R.getAtlas().getTexture("Pad"), new Rectangle(5, 5, 20, 20));
			this._pad = new Scale9Image(padTextures);
			this._pad.width= this._initialWidth;
			this._pad.height= this._initialHeight;
			this._pad.color = this._borderColor;
			addChildAt(_pad, 0);
			if(this._texture){
				texture = this._texture;
			}
		}
		
		protected function commitData():void{
			if(this._texture){
				if(!this._image){
					this._image = new Image(this._texture);
				}else{
					this._image.texture= this._texture;
					this._image.readjustSize();
				}
				this._image.x = this._image.y = _borderThickness;
				this._pad.width= this._image.width+(2*this._borderThickness);
				this._pad.height= this._image.height+(2*this._borderThickness);
				addChild(_image);
			}else{
				if(_image){
					if(_image.parent){
						_image.removeFromParent(true);
					}else{
						_image.dispose();
					}
				}
			}
		}
		protected function layout():void{
			this._pad.width = actualWidth;
			this._pad.height = actualHeight;
			if(this._image){
				this._image.x = this._image.y = this._borderThickness;
			}
			
		}
		public function get texture():Texture
		{
			return this._texture;
		}

		public function set texture(value:Texture):void
		{
			if(this._texture){
				if(this._texture!=value){
					this._texture.dispose();
				}
			}
			this._texture = value;
			//this._pad.width = this._pad.height = this._borderThickness;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		override public function dispose():void
		{
			if(this._texture){
				this._texture.dispose();
			}
			super.dispose();
		}
		
		
	}
}