package com.yomstar.app.ui.mobile.comps
{
	import starling.animation.IAnimatable;
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	
	public class YommedBusyArt extends Sprite implements IAnimatable
	{
		public function YommedBusyArt()
		{
			super();
		}
		override public function set rotation(value:Number):void
		{
			super.rotation = deg2rad(value);
		}
		public function advanceTime(time:Number):void
		{
		}
	}
}