package com.yomstar.app.ui.mobile.renderers
{
	
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.comps.HighLightPad;
	import com.yomstar.app.ui.mobile.comps.LightPad;
	import com.yomstar.app.ui.mobile.comps.SampleDetailsExtension;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.Mood;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.ScrollContainer;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.FeathersControl;
	import feathers.core.IFeathersControl;
	import feathers.skins.StateWithToggleValueSelector;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class SurveyItemRenderer extends FeathersControl implements IListItemRenderer
	{
		public static const INVALIDATION_FLAG_EXPANDED:String = "expanded";
		private static const HELPER_POINT:Point = new Point();
		protected static const DOWN_STATE_DELAY_MS:int = 250;
		public static const STATE_UP:String = "up";
		public static const STATE_DOWN:String = "down";
		public static const STATE_HOVER:String = "hover";
		public static const STATE_DISABLED:String = "disabled";
		
		protected var _delayedCurrentState:String;
		protected var _stateDelayTimer:Timer;
		protected var _useStateDelayTimer:Boolean = true;
		
		private const EXPANSION_HIGHT:uint = 200;
		private const VERTICAL_GAP:uint = 5;
		private const HORIZENTAL_GAP:uint = 10;
		private const PADDING_TOP:uint = 10;
		private const PADDING_RIGHT:uint = 10;
		private const PADDING_LEFT:uint = 10;
		private const PADDING_BOTTOM:uint = 10;
		protected var _stateToSkinFunction:Function;
		protected var _defaultLabelProperties:Object;
		protected var _defaultDetailsLabelProperties:Object;
		protected var itemLabel:Label;
		protected var itemDetailLabel:Label;
		protected var _index:int = -1;
		protected var _labelField:String;
		protected var _labelFunction:Function;
		protected var _detailsLabelField:String;
		protected var _detailsLabelFunction:Function;
		protected var touchPointID:int;
		protected var ownerScrolled:Boolean;
		protected var _owner:List;
		protected var _isSelected:Boolean;
		
		protected var currentSkin:DisplayObject;
		protected var _currentState:String = STATE_UP;
		protected var _skinSelector:StateWithToggleValueSelector = new StateWithToggleValueSelector();
		protected var _data:Object;
		protected var accessory:FeathersControl;
		protected var _accessoryFunction:Function;
		protected var _accessoryField:String = "accessory";
		protected var _stateNames:Vector.<String> = new <String>
			[
				STATE_UP, STATE_DOWN, STATE_HOVER, STATE_DISABLED
			];
		
		public function SurveyItemRenderer()
		{
			super();
			this._defaultLabelProperties = new Object();
			this._defaultDetailsLabelProperties = new Object();
			this._labelField = "label";
			this._detailsLabelField = "details";
			this.touchPointID = -1;
			this.addEventListener(TouchEvent.TOUCH, touchHandler);
			this.addEventListener(Event.REMOVED_FROM_STAGE, removedFromStageHandler);
			ownerScrolled = false;
		}
		public function get useStateDelayTimer():Boolean
		{
			return this._useStateDelayTimer;
		}
		public function set useStateDelayTimer(value:Boolean):void
		{
			this._useStateDelayTimer = value;
		}
		protected function get stateNames():Vector.<String>
		{
			return this._stateNames;
		}
		public function get labelFunction():Function
		{
			return this._labelFunction;
		}
		public function set labelFunction(value:Function):void
		{
			if(this._labelFunction == value)
			{
				return;
			}
			this._labelFunction = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		public function get labelField():String
		{
			return this._labelField;
		}
		public function set labelField(value:String):void
		{
			if(this._labelField == value)
			{
				return;
			}
			this._labelField = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		public function get detailsLabelFunction():Function
		{
			return this._detailsLabelFunction;
		}
		public function set detailsLabelFunction(value:Function):void
		{
			if(this._detailsLabelFunction == value)
			{
				return;
			}
			this._detailsLabelFunction = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		public function get detailsLabelField():String
		{
			return this._detailsLabelField;
		}
		public function set detailsLabelField(value:String):void
		{
			if(this._detailsLabelField == value)
			{
				return;
			}
			this._detailsLabelField = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		public function get defaultLabelProperties():Object
		{
			return _defaultLabelProperties;
		}
		
		public function set defaultLabelProperties(value:Object):void
		{
			_defaultLabelProperties = value;
		}
		public function get defaultDetailsLabelProperties():Object
		{
			return _defaultDetailsLabelProperties;
		}
		
		public function set defaultDetailsLabelProperties(value:Object):void
		{
			_defaultDetailsLabelProperties = value;
		}
		
		
		public function get stateToSkinFunction():Function
		{
			return this._stateToSkinFunction;
		}
		public function set stateToSkinFunction(value:Function):void
		{
			if(this._stateToSkinFunction == value)
			{
				return;
			}
			this._stateToSkinFunction = value;
			this.invalidate(INVALIDATION_FLAG_STYLES);
		}
		public function get index():int
		{
			return this._index;
		}
		
		public function set index(value:int):void
		{
			if(this._index == value)
			{
				return;
			}
			this._index = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		public function get owner():List
		{
			return List(this._owner);
		}
		
		public function set owner(value:List):void
		{
			if(this._owner == value)
			{
				return;
			}
			if(this._owner)
			{
				this._owner.removeEventListener(Event.SCROLL, owner_scrollHandler);
			}
			this._owner = value;
			if(this._owner)
			{
				this._owner.addEventListener(Event.SCROLL, owner_scrollHandler);
			}
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		protected function get currentState():String
		{
			return this._currentState;
		}
		
		protected function set currentState(value:String):void
		{
			if(this._currentState == value || !value)
			{
				return;
			}
			if(this.stateNames.indexOf(value) < 0)
			{
				throw new ArgumentError("Invalid state: " + value + ".");
			}
			if(this._useStateDelayTimer)
			{
				if(this._stateDelayTimer && this._stateDelayTimer.running)
				{
					this._delayedCurrentState = value;
					return;
				}
				if(value == STATE_DOWN)
				{
					if(this._currentState == value)
					{
						return;
					}
					this._delayedCurrentState = value;
					if(this._stateDelayTimer)
					{
						this._stateDelayTimer.reset();
					}
					else
					{
						this._stateDelayTimer = new Timer(DOWN_STATE_DELAY_MS, 1);
						this._stateDelayTimer.addEventListener(TimerEvent.TIMER_COMPLETE, stateDelayTimer_timerCompleteHandler);
					}
					this._stateDelayTimer.start();
					return;
				}
			}else{
				this._currentState = value;
			}
			
			this.invalidate(INVALIDATION_FLAG_STATE);
		}
		protected function stateDelayTimer_timerCompleteHandler(event:TimerEvent):void
		{
			currentState = this._delayedCurrentState;
			this._delayedCurrentState = null;
		}
		
		public function get data():Object
		{
			return this._data;
		}
		
		public function set data(value:Object):void
		{
			if(this._data == value)
			{
				return;
			}
			this._data = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		public function get isSelected():Boolean
		{
			return this._isSelected;
		}
		
		public function set isSelected(value:Boolean):void
		{
			if(this._isSelected == value)
			{
				return;
			}
			this._isSelected = value;
			this.invalidate(INVALIDATION_FLAG_SELECTED);
			this.dispatchEventWith(Event.CHANGE);
		}
		
		
		
		override protected function initialize():void
		{
			super.initialize();
			if(!this.itemLabel)
			{
				this.itemLabel = new Label();
				this.itemLabel.touchable = false;
				this.addChild(this.itemLabel);
			}
			if(!this.itemDetailLabel)
			{
				this.itemDetailLabel = new Label();
				this.itemDetailLabel.touchable = false;
				this.addChild(this.itemDetailLabel);
			}
			
			this.invalidate();
		}
		
		override protected function draw():void
		{
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const selectionInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SELECTED);
			const expansionInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_EXPANDED);
			const styleInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STYLES);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			if(dataInvalid)
			{
				this.commitData();
			}
			this.itemLabel.validate();
			this.itemDetailLabel.validate();
			if(styleInvalid || stateInvalid || selectionInvalid || expansionInvalid)
			{
				this.applyStyle();
				this.refreshSkin();
			}
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			if(dataInvalid || sizeInvalid || expansionInvalid)
			{
				this.layout();
			}
			if(styleInvalid || stateInvalid || selectionInvalid || sizeInvalid || expansionInvalid)
			{
				this.scaleSkin();
			}
			
		}
		protected function scaleSkin():void
		{
			if(!this.currentSkin)
			{
				return;
			}
			if(this.currentSkin.width != this.actualWidth)
			{
				this.currentSkin.width = this.actualWidth;
			}
			if(this.currentSkin.height != this.actualHeight)
			{
				this.currentSkin.height = this.actualHeight;
			}
		}
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			this.itemLabel.width = NaN;
			this.itemLabel.height = NaN;
			this.itemLabel.validate();
			this.itemDetailLabel.width = NaN;
			this.itemDetailLabel.height = NaN;
			this.itemDetailLabel.validate();
			if(this.accessory is IFeathersControl)
			{
				IFeathersControl(this.accessory).validate();
			}
			
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				newWidth = this.itemLabel.width+PADDING_LEFT+PADDING_RIGHT;
				if(this.accessory){
					newWidth += HORIZENTAL_GAP+this.accessory.width;
				}
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
				newHeight = this.itemLabel.height+PADDING_TOP+PADDING_BOTTOM+VERTICAL_GAP+itemDetailLabel.height;
			}
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		
		protected function applyStyle():void
		{
			if(this.itemLabel){
				this.itemLabel.textRendererProperties = this._defaultLabelProperties;
			}
			if(this.itemDetailLabel){
				this.itemDetailLabel.textRendererProperties = this._defaultDetailsLabelProperties;
			}
		}
		protected function refreshSkin():void
		{
			const oldSkin:DisplayObject = this.currentSkin;
			if(this._stateToSkinFunction != null)
			{
				this.currentSkin = DisplayObject(this._stateToSkinFunction(this, this._currentState, oldSkin));
			}
			else
			{
				this.currentSkin = DisplayObject(this._skinSelector.updateValue(this, this._currentState, this.currentSkin));
			}
			if(this.currentSkin != oldSkin)
			{
				if(oldSkin)
				{
					this.removeChild(oldSkin, false);
				}
				if(this.currentSkin)
				{
					this.addChildAt(this.currentSkin, 0);
				}
			}
		}
		protected function commitData():void
		{
			if(this._data)
			{
				this.itemLabel.text = this.itemToLabel(this._data);
				this.itemDetailLabel.text = this.itemToDetailsLabel(this._data);
				const newAccessory:FeathersControl = this.itemToAccessory(this._data);
				this.replaceAccessory(newAccessory);
				
			}
			else
			{
				this.itemLabel.text = "";

				
				if(this.accessory)
				{
					this.replaceAccessory(null);
				}

			}
		}
		
		protected function layout():void
		{
			this.itemDetailLabel.x = this.itemLabel.x = PADDING_LEFT;
			this.itemLabel.y = PADDING_TOP;
			this.itemLabel.validate();
			this.itemDetailLabel.y = this.itemLabel.y+this.itemLabel.height+VERTICAL_GAP;
			if(this.accessory)
			{
				this.accessory.x = this.actualWidth-this.accessory.width-PADDING_RIGHT;
				this.itemDetailLabel.width = this.itemLabel.width = this.actualWidth-this.accessory.width-HORIZENTAL_GAP-PADDING_LEFT-PADDING_RIGHT;
				this.itemDetailLabel.validate();
				this.accessory.validate();
				this.accessory.y = 0.5*(this.actualHeight-this.accessory.height);
			}else{
				this.itemDetailLabel.width = this.itemLabel.width = this.actualWidth-PADDING_LEFT-PADDING_RIGHT;
			}
		}
		public function itemToLabel(item:Object):String
		{
			if(this._labelFunction != null)
			{
				return this._labelFunction(item) as String;
			}
			else if(this._labelField != null && item && item.hasOwnProperty(this._labelField))
			{
				return item[this._labelField] as String;
			}
			else if(item is Object)
			{
				return item.toString();
			}
			return "";
		}
		public function itemToDetailsLabel(item:Object):String
		{
			if(this._detailsLabelFunction != null)
			{
				return this._detailsLabelFunction(item) as String;
			}
			else if(this._detailsLabelField != null && item && item.hasOwnProperty(this._detailsLabelField))
			{
				return item[this._detailsLabelField] as String;
			}
			else if(item is Object)
			{
				return item.toString();
			}
			return "";
		}
		public function get accessoryField():String
		{
			return this._accessoryField;
		}
		
		public function set accessoryField(value:String):void
		{
			if(this._accessoryField == value)
			{
				return;
			}
			this._accessoryField = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		public function get accessoryFunction():Function
		{
			return this._accessoryFunction;
		}
		public function set accessoryFunction(value:Function):void
		{
			if(this._accessoryFunction == value)
			{
				return;
			}
			this._accessoryFunction = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		protected function itemToAccessory(item:Object):FeathersControl
		{
			if(this._accessoryFunction != null)
			{
				return this._accessoryFunction(item) as FeathersControl;
			}
			else if(this._accessoryField != null && item && item.hasOwnProperty(this._accessoryField))
			{
				return item[this._accessoryField] as FeathersControl;
			}
			
			return null;
		}
		protected function replaceAccessory(newAccessory:FeathersControl):void
		{
			if(this.accessory == newAccessory)
			{
				return;
			}
			
			if(this.accessory)
			{
				this.accessory.removeEventListener(TouchEvent.TOUCH, accessory_touchHandler);
				//the accessory may have come from outside of this class. it's
				//up to that code to dispose of the accessory. in fact, if we
				//disposed of it here, we will probably screw something up, so
				//let's just remove it.
				this.accessory.removeFromParent();
			}
			
			
			this.accessory = newAccessory;
			
			if(this.accessory)
			{
				if(this.accessory is IFeathersControl && !(this.accessory is BitmapFontTextRenderer))
				{
					this.accessory.addEventListener(TouchEvent.TOUCH, accessory_touchHandler);
				}
				this.addChild(this.accessory);
			}
		}
		protected function accessory_touchHandler(event:TouchEvent):void
		{
			event.stopPropagation();
		}
		protected function removedFromStageHandler(event:Event):void
		{
			this.touchPointID = -1;
		}
		protected function owner_scrollHandler(event:Event):void
		{
			this.ownerScrolled = true;
			if(this.currentState != STATE_UP)
			{
				this.currentState = Button.STATE_UP;
			}
		}
		protected function touchHandler(event:TouchEvent):void
		{
			const touches:Vector.<Touch> = event.getTouches(this);
			
			if(touches.length == 0)
			{
				this.currentState = STATE_UP;
				
				return;
			}
			var touch:Touch = touches[0];
			/*if(touch.phase == TouchPhase.BEGAN)
			{
			trace("TouchPhase.BEGAN");
			this.ownerScrolled = false;
			trace("ownerScrolled: "+ownerScrolled);
			return;
			}*/
			if(this.touchPointID >= 0)
			{
				touch.getLocation(this, HELPER_POINT);
				var isInBounds:Boolean = this.hitTest(HELPER_POINT, true) != null;
				for each(var currentTouch:Touch in touches)
				{
					if(currentTouch.id == this.touchPointID)
					{
						touch = currentTouch;
						break;
					}
				}
				
				if(!touch)
				{
					return;
				}
				if(touch.phase == TouchPhase.BEGAN)
				{
					this.touchPointID = touch.id;
					this.ownerScrolled = false;
					
					return;
				}
				if(touch.phase == TouchPhase.ENDED)
				{
					this.touchPointID = -1;
					
					if(!this.ownerScrolled)
					{
						//check if the touch is still over the target
						//also, only change it if we're not selected. we're not a toggle.
						if(isInBounds && !this._isSelected)
						{
							this.isSelected = true;
						}
					}
					if(!isInBounds)
					{
						this.currentState = STATE_UP;
					}
					return;
				}
			}
			else
			{
				for each(touch in touches)
				{
					if(touch.phase == TouchPhase.BEGAN)
					{
						this.currentState = STATE_DOWN;
						this.ownerScrolled = false;
						this.touchPointID = touch.id;
						return;
					}
					else if(touch.phase == TouchPhase.HOVER)
					{
						this.currentState = STATE_HOVER;
						break;
					}
				}
			}
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		
	}
}