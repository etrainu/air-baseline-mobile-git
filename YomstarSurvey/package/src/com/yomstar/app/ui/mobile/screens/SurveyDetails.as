package com.yomstar.app.ui.mobile.screens
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	import com.greensock.easing.Expo;
	import com.yomstar.app.config.SurveySorting;
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.dataObjects.Sample;
	import com.yomstar.app.dataObjects.SurveyStat;
	import com.yomstar.app.services.SurveyResponses;
	import com.yomstar.app.services.SurveyStats;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.comps.BottomShadowedLine;
	import com.yomstar.app.ui.mobile.comps.FeathersImage;
	import com.yomstar.app.ui.mobile.comps.IconnedLabel;
	import com.yomstar.app.ui.mobile.comps.ListDetailsCalloutButton;
	import com.yomstar.app.ui.mobile.comps.ListDetailsCalloutContent;
	import com.yomstar.app.ui.mobile.comps.PercentGraphCircular;
	import com.yomstar.app.ui.mobile.comps.TransButton;
	import com.yomstar.app.ui.mobile.renderers.SampleItemRenderer;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.Mood;
	import com.yomstar.app.utils.ObjectDigger;
	
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollText;
	import feathers.controls.Scroller;
	import feathers.controls.TabBar;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	
	import flash.events.Event;
	import flash.text.TextFormatAlign;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class SurveyDetails extends YomScreen
	{
		private const INVALIDATION_FLAG_SURVEY:String = "surveyInvalid";
		private const VERTICAL_GAP:uint = 20;
		private const HORIZENTAL_GAP:uint = 20;
		private const PADDING_TOP:uint = 20;
		private const PADDING_RIGHT:uint = 20;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 20;
		private var _initialized:Boolean;
		
		public var surveyObject:Object;
		private var _surveyStats:SurveyStat;
		private var _backButton:TransButton;
		private var _list:List;
		private var _listdb:ListCollection;
		private var _surveyText:IconnedLabel;
		private var _seperator:BottomShadowedLine;
		private var _bounceRateGraph:PercentGraphCircular;
		private var _activationGraph:PercentGraphCircular;
		private var _redemptionGraph:PercentGraphCircular;
		public function SurveyDetails()
		{
			super();
			_initialized = false;
		}
		override protected function draw():void
		{
			super.draw();
			if(!_initialized){
				_initialized = true;
				this._surveyText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.HIGHLIGHT_TEAXT_FORMAT)};
				this._surveyText.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
				
				this._bounceRateGraph.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.GRAPH_FEATURED_FOUR_TEAXT_FORMAT)};
				this._bounceRateGraph.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
				
				this._activationGraph.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.GRAPH_FEATURED_TWO_TEAXT_FORMAT)};
				this._activationGraph.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
				
				this._redemptionGraph.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.GRAPH_FEATURED_FIVE_TEAXT_FORMAT)};
				this._redemptionGraph.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
			}
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			const surveyInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SURVEY);
			if(dataInvalid)
			{
				this.commitData();
			}
			if(surveyInvalid){
				commitSurveyData();
			}
			this.header.validate();
			this._surveyText.validate();
			if(dataInvalid || sizeInvalid)
			{
				this.layout();
			}
		}
		private function layout():void{
			this._surveyText.x = this.PADDING_LEFT;
			this._surveyText.y = this.header.height+this.PADDING_TOP;
			this._surveyText.width = this.actualWidth-this.PADDING_LEFT-this.PADDING_RIGHT;
			this._surveyText.validate();
			
			this._seperator.width = actualWidth;
			this._seperator.y = this._surveyText.y + this._surveyText.height+VERTICAL_GAP;
			this._seperator.validate();
			this._bounceRateGraph.validate();
			this._bounceRateGraph.x = (this.actualWidth-((this._bounceRateGraph.width*3)+(HORIZENTAL_GAP*2)))*0.5;
			this._bounceRateGraph.y = this._seperator.y+VERTICAL_GAP;
			this._bounceRateGraph.validate();
			this._activationGraph.x = this._bounceRateGraph.x + this._bounceRateGraph.width + HORIZENTAL_GAP;
			this._activationGraph.y = this._bounceRateGraph.y;
			this._activationGraph.validate();
			this._redemptionGraph.x = this._activationGraph.x + this._activationGraph.width + HORIZENTAL_GAP;
			this._redemptionGraph.y = this._bounceRateGraph.y;
			
			this._list.width = actualWidth;
			this._list.y = this._bounceRateGraph.y + this._bounceRateGraph.height+VERTICAL_GAP;
			this._list.height = actualHeight-this._list.y;
		}
		override protected function initialize():void
		{
			super.initialize();
			this._backButton = new TransButton();
			var beckImage:Image = new Image(R.getAtlas().getTexture("back"));
			this._backButton.defaultIcon = beckImage;
			this._backButton.defaultSelectedIcon = beckImage;
			this._backButton.autoFlatten = true
			this._backButton.addEventListener(starling.events.Event.TRIGGERED, backButton_triggeredHandler);
			this.addChild(this.header);
			this.header.leftItems = new <DisplayObject>
				[
					this._backButton
				];
			this.backButtonHandler = this.onBackButton;
			_list = new List();
			this._list.itemRendererType = SampleItemRenderer;
			this._list.itemRendererProperties.labelField = "baselineName";
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = 0;
			layout.paddingTop = 0;
			layout.paddingRight = 0;
			layout.paddingBottom = 0;
			layout.paddingLeft = 0;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			layout.hasVariableItemDimensions = true;
			this._list.layout = layout;
			this._list.itemRendererProperties.accessoryFunction  = listItemAccessoryFunction;
			this._list.itemRendererProperties.detailsLabelFunction = listItemDetailsFunction;
			this._list.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._list.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this._list.scrollerProperties.snapScrollPositionsToPixels = true;
			this._list.addEventListener(starling.events.Event.CHANGE, listChanged);
			this.addChild(this._list);
			
			if(!_surveyText){
				_surveyText = new IconnedLabel();
			}
			this.addChild(_surveyText);
			_surveyText.icon = new FeathersImage(R.getAtlas().getTexture("SurveysIcon"), GlobalAppValues.FEATURED_COLOR_TWO);
			this._seperator = new BottomShadowedLine();
			this.addChild(this._seperator);
			this._bounceRateGraph = new PercentGraphCircular("Bounce-rate", 0 ,GlobalAppValues.FEATURED_COLOR_FOUR);
			this.addChild(this._bounceRateGraph);
			
			this._activationGraph = new PercentGraphCircular("Activation", 0 ,GlobalAppValues.FEATURED_COLOR_TWO);
			this.addChild(this._activationGraph);
			
			this._redemptionGraph = new PercentGraphCircular("Redemption", 0 ,GlobalAppValues.FEATURED_COLOR_FIVE);
			this.addChild(this._redemptionGraph);
		}
		override protected function run():void{
			super.run();
			busyIndicator.start();
			var surveyStats:SurveyStats = new SurveyStats();
			surveyStats.addEventListener(flash.events.Event.COMPLETE, onSurveyStats, false, 0, true);
			surveyStats.getStats(Main.data.token, surveyObject.id);
		}
		private function commitData():void{
			_surveyText.text = surveyObject.name;
			if(_listdb){
				this._list.dataProvider = _listdb;
			}
		}
		private function commitSurveyData():void{
			if(this._surveyStats){
				if(this._surveyStats.viewCount>0){
					//TweenLite.to(this._bounceRateGraph, 1, {percent:Number(100-((this._surveyStats.sampleCount/this._surveyStats.viewCount)*100)), ease:Expo.easeOut, delay:1});
					this._bounceRateGraph.percent = Math.round(Number(100-((this._surveyStats.sampleCount/this._surveyStats.viewCount)*100)));
				}else{
					this._bounceRateGraph.percent = 0;
				}
				
				if(this._surveyStats.sampleWithRewardCount>0){
					this._activationGraph.percent = Math.round(Number((this._surveyStats.sampleWithActivatedRewardCount/this._surveyStats.sampleWithRewardCount)*100));
					this._redemptionGraph.percent = Math.round(Number((this._surveyStats.sampleWithRedeemedRewardCount/this._surveyStats.sampleWithRewardCount)*100));
				}else{
					this._activationGraph.percent = 0;
					this._redemptionGraph.percent = 0;
				}
				
			}
			
		}
		private function onSurveyStats(event:flash.events.Event):void
		{
			var surveyStats:SurveyStats = event.target as SurveyStats;
			//SAMPLE DATA: sampleWithActivatedRewardCount:4, sampleWithRewardCount:6, sampleWithRedeemedRewardCount:2, sampleWithParticipantCount:6, sampleCount:12, viewCount:33
			trace("SurveyStats.surveyStats: "+SurveyStats.surveyStats);
			this._surveyStats = SurveyStats.surveyStats;
			surveyStats.removeEventListener(flash.events.Event.COMPLETE, onSurveyStats);
			getLatestResponses();
		}
		/*private function updateStats():void{
			statsStr = "";
			var statsObj:Object = SurveyStats.surveyStats;
			for(var id:String in statsObj) {
				var value:Object = statsObj[id];
				detailsStr += id + " = " + value + "\r";
			}
			showDetails();
			busyIndicator.stop();
		}*/
		
		
		private function getLatestResponses():void{
			var latestResponses:SurveyResponses = new SurveyResponses();
			latestResponses.addEventListener(flash.events.Event.COMPLETE, onLatestResponses, false, 0, true);
			latestResponses.addEventListener(flash.events.ErrorEvent.ERROR, onLatestResponsesError, false, 0, true);
			trace("surveyObject: "+ObjectDigger.digg(surveyObject));
			latestResponses.getResponsesForSurvey(Main.data.token, 0, SurveyStats.surveyStats.sampleCount, surveyObject.id, SurveySorting.DESCENDING);
		}
		private function onLatestResponsesError(event:flash.events.ErrorEvent):void
		{
			var latestResponses:SurveyResponses = event.currentTarget as SurveyResponses;
			latestResponses.removeEventListener(flash.events.Event.COMPLETE, onLatestResponses);
			latestResponses.removeEventListener(flash.events.ErrorEvent.ERROR, onLatestResponsesError);
			if(alert){
				alert.message = event.text;
				alert.closeLabel = "OK";
				alert.title = "Error";
				alert.show();
			}
		}
		private function onLatestResponses(event:flash.events.Event):void
		{
			var latestResponses:SurveyResponses = event.currentTarget as SurveyResponses;
			latestResponses.removeEventListener(flash.events.Event.COMPLETE, onLatestResponses);
			latestResponses.removeEventListener(flash.events.ErrorEvent.ERROR, onLatestResponsesError);
			//sample result [{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"woluGR1hSlOJoGN3pEi9Ng","version":1,"latitude":-27.470933,"longitude":153.02350199999998,"averagePercentageScore":54,"isGeolocated":true,"dateCaptured":"2012-12-11T16:57:14","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":75,"responses":["option 2"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"CRHHD3pRy6zeZ1pxFhebA","version":1,"averagePercentageScore":41,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:37","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":50,"responses":["option 3"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"HbcyoJTgqOuoKwn4FOg","version":1,"averagePercentageScore":12,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:45","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":0,"responses":["option 1"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":25,"responses":["option 4"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"LeDRxjFQcqQKLQKT4tJVw","version":1,"averagePercentageScore":58,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:50","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":50,"responses":["option 3"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":50,"responses":["option 3"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"lkc5f39QAm1C4pFXELO4Q","version":1,"averagePercentageScore":100,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:56","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":75,"responses":["option 4"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":100,"responses":["option 1"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"jkKlDhWxTcuTKX5Dp4M2pg","version":1,"averagePercentageScore":66,"isGeolocated":false,"dateCaptured":"2012-12-11T16:59:01","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":100,"responses":["option 1"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"6tePcDq3RNO1CwCtd0zCw","version":1,"averagePercentageScore":41,"isGeolocated":false,"dateCaptured":"2012-12-11T16:59:07","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":50,"responses":["option 3"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"iKQfUMHKRc6KdViQU02M1A","version":1,"averagePercentageScore":66,"isGeolocated":false,"dateCaptured":"2012-12-11T16:59:17","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":100,"responses":["option 1"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"SXWOaJPoRsevJJqL7pPww","version":1,"latitude":-27.470933,"longitude":153.02350199999998,"averagePercentageScore":70,"isGeolocated":true,"dateCaptured":"2012-12-11T16:59:34","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":50,"responses":["option 3"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":75,"responses":["option 2"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"A4SuNuCIRh6ezMNQDE58Lw","version":1,"latitude":-27.470933,"longitude":153.02350199999998,"averagePercentageScore":62,"isGeolocated":true,"dateCaptured":"2012-12-11T16:59:51","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":75,"responses":["option 4"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":25,"responses":["option 4"]}]}]
			busyIndicator.stop();
			_listdb = new ListCollection(SurveyResponses.surveyResponses.samples);
			this.invalidate(INVALIDATION_FLAG_DATA);
			this.invalidate(INVALIDATION_FLAG_SURVEY);
		}
		private function listItemDetailsFunction(o:Sample):String{
			var detailsStr:String = String(o.dateCaptured).replace("T", " at ");
			return detailsStr;
		}
		private function listItemAccessoryFunction(o:Sample):DisplayObject{
			var accessoryBtn:Button = new Button();
			accessoryBtn.defaultIcon = new Image(R.getAtlas().getTexture("Icon_Plus"));
			return accessoryBtn;
		}
		private function listChanged(e:starling.events.Event):void
		{
			dispatchEventWith("sampleSelected", false, this._list.selectedItem);
		}
		private function onBackButton():void
		{
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function backButton_triggeredHandler(event:starling.events.Event):void
		{
			this.onBackButton();
		}
	}
}