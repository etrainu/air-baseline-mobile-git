package com.yomstar.app.ui.mobile.comps
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Expo;
	import com.yomstar.app.dataObjects.Baseline;
	import com.yomstar.app.events.ImageLoaderEvent;
	import com.yomstar.app.services.AccountDetails;
	import com.yomstar.app.services.AccountStats;
	import com.yomstar.app.services.AvatarImageLoader;
	import com.yomstar.app.services.PlansDetails;
	import com.yomstar.app.services.UserInfo;
	import com.yomstar.app.services.UserSurvey;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.DateFormat;
	import com.yomstar.app.utils.ObjectDigger;
	
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.core.FeathersControl;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	import feathers.skins.StandardIcons;
	
	import flash.display.BitmapData;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	
	import pl.mateuszmackowiak.nativeANE.NativeDialogEvent;
	import pl.mateuszmackowiak.nativeANE.alert.NativeAlert;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class Settings  extends FeathersControl
	{
		private const TEXTLINE_VERTICAL_GAP:uint = 5;
		private const GAP:uint = 20;
		private const PADDING_TOP:uint = 15;
		private const PADDING_RIGHT:uint = 10;
		private const PADDING_LEFT:uint = 30;
		private const PADDING_BOTTOM:uint = 15;
		private const LEFT_SHADOW_WIDTH:uint = 12;
		private const REFRESHER_GAP:uint = 10;
		public var parrentScreenSnapshot:BitmapData;
		
		
		private var _initialized:Boolean;
		private var _logoutBtn:Button;

		private var _surveysText:IconnedLabel;
		private var _responsesText:IconnedLabel;
		private var _qrCodesText:IconnedLabel;
		private var _scannedQrCodesText:IconnedLabel;
		private var _rewardsText:IconnedLabel;
		private var _avatar:BorderedImage;
		private var _welcomeText:Label;
		private var _planText:Label;
		private var _bg:LefShadowedPad;
		private var refreshSpacer:Spacer;
		private var _seperator:BottomShadowedLine;
		private var _settingsContainer:ScrollContainer;
		private var _refresher:SettingsRefresher;
		protected var alert:NativeAlert;
		public function Settings()
		{
			_initialized = false;
			if(NativeAlert.isSupported){
				alert = NativeAlert(Main.alertPool.getObj());
				alert.closeHandler = onAlertClosed;
			}
		}
		protected function onAlertClosed(event:NativeDialogEvent):void{
			Main.alertPool.returnObj(alert);
		}
		override protected function draw():void
		{
			super.draw();
			
			var realestateHight:int = this.actualHeight;
			var realestateWidth:int = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const styleInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STYLES);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			const sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			if(!_initialized){
				_initialized = true;
				this._surveysText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._responsesText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._qrCodesText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._scannedQrCodesText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._rewardsText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				
				this._planText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.LIST_DETAILS_HIGHLIGHT_TEXT_FORMAT)};
				this._planText.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
				
				this._welcomeText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.HIGHLIGHT_TEAXT_FORMAT)};
				this._welcomeText.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
			}
			if(dataInvalid)
			{
				this.commitData();
			}
			this._welcomeText.validate();
			this._logoutBtn.validate();
			if(dataInvalid || sizeInvalid)
			{
				this.layout();
			}
		}
		protected function commitData():void
		{
			trace("commiting data");
			if(UserInfo.userInfo){
				this._welcomeText.text = "Logged in as: "+UserInfo.userInfo.firstName+"!";
				trace("displaying user info");
				if(AccountStats.accountStats && PlansDetails.plansDetails && AccountDetails.accountDetails){
					trace("displaying rest");
					displayStats();
					refreshAvatar();
				}
			}
		}
		protected function layout():void{
			this._bg.width = actualWidth;
			this._bg.height = actualHeight;
			
			this._logoutBtn.width = actualWidth-(PADDING_LEFT*2)-(PADDING_RIGHT*2);
			this._logoutBtn.x = PADDING_LEFT*2;
			this._logoutBtn.y = actualHeight-PADDING_BOTTOM-this._logoutBtn.height;
			
			if(this._avatar){
				this._avatar.x = PADDING_LEFT;
				this._avatar.y = PADDING_TOP;
				this._avatar.validate();
				this._welcomeText.width = actualWidth-this._avatar.x-this._avatar.width-GAP-PADDING_RIGHT;
			}else{
				this._welcomeText.width = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			}
			this._welcomeText.validate();
			this._welcomeText.x = actualWidth-PADDING_RIGHT-_welcomeText.width;
			this._welcomeText.y = PADDING_TOP;
			this._welcomeText.validate();
			this._planText.x = this._welcomeText.x;
			this._planText.width = this._welcomeText.width;
			this._planText.y = this._welcomeText.y+this._welcomeText.height+TEXTLINE_VERTICAL_GAP;
			
			this._seperator.width = actualWidth;
			this._seperator.x = LEFT_SHADOW_WIDTH;
			if(this._avatar){
				this._seperator.y = Math.max((this._avatar.y+this._avatar.height), (this._planText.y+this._planText.height))+PADDING_TOP;
			}else{
				this._seperator.y = (this._planText.y+this._planText.height)+PADDING_TOP;
			}
			this._seperator.validate();
			this._settingsContainer.width = actualWidth;
			this._settingsContainer.y = this._seperator.y+this._seperator.height;
			this._settingsContainer.height = this._logoutBtn.y-PADDING_BOTTOM-this._settingsContainer.y;
		}
		override protected function initialize():void
		{
			super.initialize();
			this._settingsContainer = new ScrollContainer();
			this._settingsContainer.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._settingsContainer.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this._settingsContainer.scrollerProperties.snapScrollPositionsToPixels = true;
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = GAP;
			layout.paddingTop = PADDING_TOP;
			layout.paddingRight = PADDING_RIGHT;
			layout.paddingBottom = PADDING_BOTTOM;
			layout.paddingLeft = PADDING_LEFT;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			this._settingsContainer.layout = layout;
			this.addChild(this._settingsContainer);
			
			this.refreshSpacer = new Spacer();
			this._welcomeText = new Label();
			this.addChildAt(this._welcomeText, 0);
			this._planText = new Label();
			this.addChild(this._planText);
			
			
			this._refresher = new SettingsRefresher();
			this._settingsContainer.addChild(this._refresher);
			this._refresher.addEventListener(SettingsRefresher.REFRESH_REQUEST, onRefreshRequest);
			this.refreshSpacer.height = REFRESHER_GAP;
			this._settingsContainer.addChild(this.refreshSpacer);
			
			this._surveysText = new IconnedLabel();
			this._settingsContainer.addChild(this._surveysText);
			this._surveysText.icon = new FeathersImage(R.getAtlas().getTexture("IconSpeechBuble"), GlobalAppValues.WHITE_COLOR);
			this._surveysText.visible = false;
			
			this._responsesText = new IconnedLabel();
			this._settingsContainer.addChild(this._responsesText);
			this._responsesText.icon = new FeathersImage(R.getAtlas().getTexture("Icon_Human"), GlobalAppValues.WHITE_COLOR);
			this._responsesText.visible = false;
			
			this._qrCodesText = new IconnedLabel();
			this._settingsContainer.addChild(this._qrCodesText);
			this._qrCodesText.icon = new FeathersImage(R.getAtlas().getTexture("Icon_QRCode"), GlobalAppValues.WHITE_COLOR);
			this._qrCodesText.visible = false;
			
			this._scannedQrCodesText = new IconnedLabel();
			this._settingsContainer.addChild(this._scannedQrCodesText);
			this._scannedQrCodesText.icon = new FeathersImage(R.getAtlas().getTexture("Icon_QRCode"), GlobalAppValues.WHITE_COLOR);
			this._scannedQrCodesText.visible = false;
			
			this._rewardsText = new IconnedLabel();
			this._settingsContainer.addChild(this._rewardsText);
			this._rewardsText.icon = new FeathersImage(R.getAtlas().getTexture("Icon_Reward"), GlobalAppValues.WHITE_COLOR);
			this._rewardsText.visible = false;
			
			
			
			this._logoutBtn = new Button();
			this._logoutBtn.defaultIcon = new Image(R.getAtlas().getTexture("LogoutIcon"));
			this._logoutBtn.addEventListener(Event.TRIGGERED, logout_triggeredHandler);
			this._bg = new LefShadowedPad();
			this.addChildAt(this._bg, 0);
			this.addChild(this._logoutBtn);
			
			this._seperator = new BottomShadowedLine();
			this.addChild(this._seperator);
			onRefreshRequest();
		}
		private function onRefreshRequest(e:Event = null):void{
			if(e){
				this.dispatchEvent(e);
			}
			getUserInfo();
		}
		private function getUserInfo():void{
			var userInfo:UserInfo = new UserInfo();
			userInfo.addEventListener(flash.events.Event.COMPLETE, onUserInfo, false, 0, true);
			userInfo.addEventListener(flash.events.ErrorEvent.ERROR, onUserInfoError, false, 0, true);
			userInfo.getInfo(Main.data.token);
		}
		private function onUserInfoError(event:flash.events.ErrorEvent):void
		{
			var userInfo:UserInfo = event.currentTarget as UserInfo;
			userInfo.removeEventListener(flash.events.Event.COMPLETE, onUserInfo);
			userInfo.removeEventListener(flash.events.ErrorEvent.ERROR, onUserInfoError);
			if(alert){
				alert.message = event.text;
				alert.closeLabel = "OK";
				alert.title = "Error";
				alert.show();
			}
		}
		private function onUserInfo(event:flash.events.Event):void
		{
			var userInfo:UserInfo = event.target as UserInfo;
			userInfo.removeEventListener(flash.events.Event.COMPLETE, onUserInfo);
			userInfo.removeEventListener(flash.events.ErrorEvent.ERROR, onUserInfoError);
			this.invalidate(INVALIDATION_FLAG_DATA);
			getAccountDetails();
		}
		private function getAccountDetails():void{
			var accountDetails:AccountDetails = new AccountDetails();
			accountDetails.addEventListener(flash.events.Event.COMPLETE, onAccountDetails, false, 0, true);
			accountDetails.addEventListener(flash.events.ErrorEvent.ERROR, onAccountDetailsError, false, 0, true);
			accountDetails.getDetails(Main.data.token);
		}
		private function onAccountDetailsError(event:flash.events.ErrorEvent):void
		{
			trace("onAccountStatsError: "+event.text);
			var accountDetails:AccountDetails = event.currentTarget as AccountDetails;
			accountDetails.removeEventListener(flash.events.Event.COMPLETE, onAccountDetails);
			accountDetails.removeEventListener(flash.events.ErrorEvent.ERROR, onAccountDetailsError);
			if(alert){
				alert.message = event.text;
				alert.closeLabel = "OK";
				alert.title = "Error";
				alert.show();
			}
		}
		private function onAccountDetails(event:flash.events.Event):void
		{
			var accountDetails:AccountDetails = event.currentTarget as AccountDetails;
			accountDetails.removeEventListener(flash.events.Event.COMPLETE, onAccountDetails);
			accountDetails.removeEventListener(flash.events.ErrorEvent.ERROR, onAccountDetailsError);
			//sample result id:bjS7OO13TiKrOe5OFioxKw, subscriberToken:d55cbe0ff9029fd3bc1be52303ceab67d3ab9b79, country:Australia, planId:21476, industry:elearning, dateCreated:2012-12-04T15:57:14, planName:YomStar Now, active:true, name:Amir's company
			//trace("Account Details: "+ObjectDeigger.digg(AccountDetails.accountDetails));
			getAccountStats();
		}
		private function getAccountStats():void{
			var accountStats:AccountStats = new AccountStats();
			accountStats.addEventListener(flash.events.Event.COMPLETE, onAccountStats, false, 0, true);
			accountStats.addEventListener(flash.events.ErrorEvent.ERROR, onAccountStatsError, false, 0, true);
			accountStats.getStats(Main.data.token);
		}
		private function onAccountStatsError(event:flash.events.ErrorEvent):void
		{
			trace("onAccountStatsError: "+event.text);
			var accountStats:AccountStats = event.currentTarget as AccountStats;
			accountStats.removeEventListener(flash.events.Event.COMPLETE, onAccountStats);
			accountStats.removeEventListener(flash.events.ErrorEvent.ERROR, onAccountStatsError);
			if(alert){
				alert.message = event.text;
				alert.closeLabel = "OK";
				alert.title = "Error";
				alert.show();
			}
			
		}
		private function onAccountStats(event:flash.events.Event):void
		{
			trace("new stats received");
			var accountStats:AccountStats = event.currentTarget as AccountStats;
			accountStats.removeEventListener(flash.events.Event.COMPLETE, onAccountStats);
			accountStats.removeEventListener(flash.events.ErrorEvent.ERROR, onAccountStatsError);
			getPlanDetails();
		}
		private function getPlanDetails():void{
			var plansDetails:PlansDetails = new PlansDetails();
			plansDetails.addEventListener(flash.events.Event.COMPLETE, onPlansDetails, false, 0, true);
			plansDetails.addEventListener(flash.events.ErrorEvent.ERROR, onPlansDetailsError, false, 0, true);
			plansDetails.getPlans();
		}
		private function onPlansDetailsError(event:flash.events.ErrorEvent):void
		{
			trace("onAccountStatsError: "+event.text);
			var plansDetails:PlansDetails = event.currentTarget as PlansDetails;
			plansDetails.removeEventListener(flash.events.Event.COMPLETE, onPlansDetails);
			plansDetails.removeEventListener(flash.events.ErrorEvent.ERROR, onPlansDetailsError);
		}
		private function onPlansDetails(event:flash.events.Event):void
		{
			var plansDetails:PlansDetails = event.currentTarget as PlansDetails;
			plansDetails.removeEventListener(flash.events.Event.COMPLETE, onPlansDetails);
			plansDetails.removeEventListener(flash.events.ErrorEvent.ERROR, onPlansDetailsError);
			trace("data invalidated");
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		private function refreshAvatar():void{
			var avatarLoader:AvatarImageLoader = Main.avatarImageLoaderPool.getObj() as AvatarImageLoader;
			avatarLoader.addEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
			avatarLoader.addEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
			avatarLoader.getAvatar(UserInfo.userInfo.email);
		}
		private function onAvatarLoadError(e:IOErrorEvent):void{
			var avatarLoader:AvatarImageLoader = AvatarImageLoader(e.currentTarget);
			avatarLoader.removeEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
			avatarLoader.removeEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
			Main.staticMapManagerPool.returnObj(avatarLoader);
			trace("Avatar load error: "+e.text);
		}
		private function onAvatarLoaded(e:ImageLoaderEvent):void{
			trace("onAvatarRecieved");
			var avatarLoader:AvatarImageLoader = AvatarImageLoader(e.currentTarget);
			avatarLoader.removeEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
			avatarLoader.removeEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
			if(!this._avatar){
				this._avatar = new BorderedImage(e.imageTexture, 5, GlobalAppValues.FEATURED_COLOR_TWO);
			}else{
				this._avatar.texture.dispose();
				this._avatar.texture= e.imageTexture;
			}
			this.addChild(this._avatar);
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
			this.invalidate(INVALIDATION_FLAG_SIZE);
			Main.staticMapManagerPool.returnObj(avatarLoader);
		}
		private function displayStats():void{
			
			_planText.text = "Plan: "+AccountDetails.accountDetails.planName;
			
			PlansDetails.getPlanById(AccountDetails.accountDetails.planId, true);
			var maxActiveSurveyStr:String;
			if(Number(PlansDetails.activePlan.maxActiveSurvey) > -1){
				maxActiveSurveyStr = String(PlansDetails.activePlan.maxActiveSurvey);
			}else{
				maxActiveSurveyStr = "unlimited";
			}
			
			var sampleLimitStr:String;
			if(Number(PlansDetails.activePlan.sampleLimit) > -1){
				sampleLimitStr = String(PlansDetails.activePlan.sampleLimit);
			}else{
				sampleLimitStr = "unlimited";
			}
			
			var maxQRCodeStr:String;
			if(Number(PlansDetails.activePlan.maxQRCode) > -1){
				maxQRCodeStr = String(PlansDetails.activePlan.maxQRCode);
			}else{
				maxQRCodeStr = "unlimited";
			}
			
			
			_surveysText.visible = true;
			this._surveysText.text = AccountStats.accountStats.activeBaseline+" of "+maxActiveSurveyStr+" surveys published.";
			_responsesText.visible = true;
			this._responsesText.text = AccountStats.accountStats.totalSampleCount+" of "+sampleLimitStr+" responses received.";
			_qrCodesText.visible = true;
			this._qrCodesText.text = AccountStats.accountStats.qrCodeCount+" of "+maxQRCodeStr+" QR Codes created.";
			_scannedQrCodesText.visible = true;
			this._scannedQrCodesText.text = "QR Codes scanned: "+AccountStats.accountStats.totalQRCodeScans;
			_rewardsText.visible = true;
			this._rewardsText.text = "Rewards redeemed: "+AccountStats.accountStats.totalRedemption;
		}
		private function logout_triggeredHandler(e:Event):void
		{
			this.dispatchEventWith("logout");
		}
		private function onBackButton():void
		{
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		
	}
}