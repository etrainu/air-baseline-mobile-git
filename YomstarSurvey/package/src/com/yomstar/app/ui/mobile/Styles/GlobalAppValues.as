package com.yomstar.app.ui.mobile.Styles
{
	public class GlobalAppValues
	{
		//Colors
		public static const WHITE_COLOR:Number = 0xFFFFFF;
		public static const LIGHT_GRAY_COLOR:Number = 0xe5e5e5;
		public static const MID_GRAY_COLOR:Number = 0x7c7c7c;
		public static const FEATURED_COLOR_ONE:Number = 0x0099b2;//cyan
		public static const FEATURED_COLOR_TWO:Number = 0xf68124;//Orange
		public static const FEATURED_COLOR_THREE:Number = 0x13171a;//Black
		public static const FEATURED_COLOR_FOUR:Number = 0x14a700;//Green
		public static const FEATURED_COLOR_FIVE:Number = 0xcb2026;//Red
		
		//Values
		public static const SETIINGS_BUTTON_SIZE:uint = 70;
		public static const SHADOW_OVERLAP:uint = 10;
		public static const LIST_ACCESSORY_BUTTON_SIZE:uint = 70;
		public static const TEXT_INPUT_HEIGHT:uint = 60;
		public static const TEXT_INPUT_FONT_SIZE:uint = 35;
		public static const USER_AVATAR_SIZE:uint = 80;
		public static const DEFAULT_USER_AVATAR:String = "http://yomstar.s3.amazonaws.com/static/static/a-icon-white-cropped.png";
	}
}