package com.yomstar.app.ui.mobile.comps
		{
			import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
			import com.yomstar.app.ui.mobile.resources.R;
			
			import feathers.controls.ScrollContainer;
			import feathers.controls.Scroller;
			import feathers.display.Scale9Image;
			import feathers.textures.Scale9Textures;
			
			import flash.geom.Rectangle;
			
			import starling.display.Image;
			import starling.textures.Texture;
			import starling.utils.Color;
			
			public class FeathersImage extends ScrollContainer
			{
				private var _texture:Texture;
				private var _image:Image;
				private var _color:Number;
				public function FeathersImage(texture:Texture = null, color:Number=-1)
				{
					super();
					if(color != -1){
						this._color = color;
					}
					this._texture = texture;
				}
				
				public function get color():Number
				{
					return _color;
				}

				public function set color(value:Number):void
				{
					_color = value;
					if(this._image && this._color){
						this._image.color = this._color;
					}
				}

				override protected function draw():void
				{
					this.unflatten();
					super.draw();
					this.flatten();
				}
				
				override protected function initialize():void
				{
					// TODO Auto Generated method stub
					
					super.initialize();
					this.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
					this.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
					this.scrollerProperties.snapScrollPositionsToPixels = true;
					this.touchable = false;
					const padTextures:Scale9Textures = new Scale9Textures(R.getAtlas().getTexture("Pad"), new Rectangle(7, 7, 7, 7));
					if(this._texture){
						texture = this._texture;
					}
					this.flatten();
				}
				
				public function get texture():Texture
				{
					return this._texture;
				}
				
				public function set texture(value:Texture):void
				{
					this._texture = value;
					if(this._texture){
						if(!this._image){
							this._image = new Image(this._texture);
						}else{
							this._image.texture= this._texture;
							this._image.readjustSize();
						}
						this._image.x = this._image.y = 0;
						if(this._color){
							this._image.color = this._color;
						}
						addChild(_image);
					}else{
						if(_image){
							if(_image.parent){
								_image.removeFromParent(true);
							}else{
								_image.dispose();
							}
						}
					}
				}
				
			}
		}