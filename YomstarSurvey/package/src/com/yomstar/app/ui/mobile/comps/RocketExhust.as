package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.resources.R;
	
	import starling.extensions.PDParticleSystem;
	import starling.textures.Texture;
	
	public class RocketExhust extends PDParticleSystem
	{
		//xml/data class
		[Embed(source="../../../../../../assets/particle/particle.pex", mimeType="application/octet-stream")]
		public static const exhustConfig:Class;
		public function RocketExhust()
		{
			var psConfig:XML = XML(new exhustConfig());
			super(psConfig, R.getAtlas().getTexture("exhustTexture"));
		}
		
		override public function get x():Number
		{
			return this.emitterX;
		}
		
		override public function set x(value:Number):void
		{
			this.emitterX = value;
		}
		override public function get y():Number
		{
			return this.emitterY;
		}
		
		override public function set y(value:Number):void
		{
			this.emitterY = value;
		}
		
	}
}