package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.dataObjects.Baseline;
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	
	import feathers.controls.Button;
	import feathers.core.FeathersControl;
	
	import org.gestouch.core.GestureState;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.LongPressGesture;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	
	public class ListSurveysCalloutButton extends FeathersControl
	{
		private var _label:String;
		
		public var baseline:Baseline;
		private var _button:Button;
		private var _defaultIcon:DisplayObject;
		private var _preventTrigger:Boolean = false;
		
		public function ListSurveysCalloutButton()
		{
			super();
			_label = "";
		}

		public function get defaultIcon():DisplayObject
		{
			this._defaultIcon = this._button.defaultIcon;
			return _defaultIcon;
		}

		public function set defaultIcon(value:DisplayObject):void
		{
			this._defaultIcon = value;
			if(this._button){
				this._button.defaultIcon = this._defaultIcon;
			}
			
		}

		public function get label():String
		{
			_label = _button.label;
			return _label;
		}

		public function set label(value:String):void
		{
			_label = value;
			if(this._button){
				this._button.label = _label;
			}
		}
		
		override protected function draw():void
		{
			this._button.validate();
			this._button.x = -1*this._button.width;
			this._button.y = this._button.height*-0.5;
		}
		
		override protected function initialize():void
		{
			this._button = new Button();
			this._button.addEventListener(Event.TRIGGERED, onResponseDetailsRequest);
			this._button.defaultLabelProperties.textFormat = TextFormats.getTextFormat(TextFormats.ACCESSORY_BUTTON_TEXT_FORMAT);
			
			var longPress:LongPressGesture = new LongPressGesture(this._button);
			longPress.minPressDuration = 1000;
			longPress.addEventListener(GestureEvent.GESTURE_STATE_CHANGE, onLongPressed);
			this._button.label = this._label;
			if(this._defaultIcon){
				this._button.defaultIcon = this._defaultIcon;
			}
			addChild(_button);
		}
		private function onLongPressed(event:GestureEvent):void
		{
			const gesture:LongPressGesture = event.target as LongPressGesture;
			
			if (event.newState != GestureState.BEGAN)
				return;
			_preventTrigger = true;
			this.dispatchEventWith(Event.OPEN);
			
		}
		private function onResponseDetailsRequest(e:Event):void{
			if(_preventTrigger){
				_preventTrigger = false;
			}else{
				this.dispatchEvent(e);
			}
		}
	}
}