package com.yomstar.app.ui.mobile
{

	public class Config
	{
		public static const PRODUCTION:Boolean = false;
		public static const SCREEN_TRANSITION:Boolean = true;
		public static const TRANSITION_DURATION:Number = 0.4;
		public static const SLIDE_MENU_MARGIN:uint = 85;
	}
}