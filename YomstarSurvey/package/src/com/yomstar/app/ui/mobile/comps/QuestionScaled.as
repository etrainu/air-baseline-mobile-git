package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.config.YomstarGlobalConsts;
	import com.yomstar.app.dataObjects.FreeTextQuestionData;
	import com.yomstar.app.dataObjects.FreeTextResponseData;
	import com.yomstar.app.dataObjects.MultiChoiceQuestionData;
	import com.yomstar.app.dataObjects.MultiChoiceResponseData;
	import com.yomstar.app.dataObjects.ScaledQuestionData;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.MatchChoices;
	
	import feathers.controls.Label;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.controls.Slider;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.core.ToggleGroup;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	
	import flash.text.TextFormatAlign;
	
	import starling.display.Image;
	import starling.events.Event;
	
	public class QuestionScaled extends ScrollContainer
	{
		
		private const GAP:uint = 20;
		private const PADDING_TOP:uint = 10;
		private const PADDING_RIGHT:uint = 10;
		private const PADDING_LEFT:uint = 10;
		private const PADDING_BOTTOM:uint = 10;
		
		private var _questionTypeLabel:Label;
		private var _questionTextLabel:Label;
		
		private var _question:ScaledQuestionData;
		
		private var _artImage:Image;
		private var _initialized:Boolean;
		private var _headerColumn:ScrollContainer;
		private var _contentColumn:ScrollContainer;
		private var _selectedMoodTxt:Label;
		private var _slider:Slider;
		private var _len:int;
		public function QuestionScaled(question:ScaledQuestionData)
		{
			super();
			_initialized = false;
			
			
			
			this._question = question;
			this.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			this.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this.scrollerProperties.snapScrollPositionsToPixels = true;
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = GAP;
			layout.paddingTop = PADDING_TOP;
			layout.paddingRight = PADDING_RIGHT;
			layout.paddingBottom = PADDING_BOTTOM;
			layout.paddingLeft = PADDING_LEFT;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_LEFT;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			this.layout = layout;
		}
		override protected function draw():void
		{
			if(!_initialized){
				this._questionTypeLabel.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT);
				this._questionTypeLabel.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
				this._questionTypeLabel.textRendererProperties.wordWrap = true;
				this._questionTextLabel.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT_DARK);
				this._questionTextLabel.textRendererProperties.wordWrap = true;
				
				this._selectedMoodTxt.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.SUB_HEADER_HIGHLIGHTED_TEAXT_FORMAT);
				this._initialized = true;
			}
			var realEstateWidth:int = this.actualWidth - PADDING_LEFT - PADDING_RIGHT;
			this._contentColumn.width = realEstateWidth;
			this._headerColumn.width = realEstateWidth;
			_questionTextLabel.width = realEstateWidth-_artImage.width-GAP;
			_contentColumn.invalidate(FeathersControl.INVALIDATION_FLAG_ALL);
			_contentColumn.validate();
			_headerColumn.invalidate(FeathersControl.INVALIDATION_FLAG_ALL);
			_headerColumn.validate();
			super.draw();
		}
		
		override protected function initialize():void
		{
			
			this.backgroundSkin = new LightPad();
			this._artImage = new Image(R.getAtlas().getTexture("question_scaled"));
			this._artImage.touchable = false;
			_headerColumn = new ScrollContainer();
			const headerColumnLayout:HorizontalLayout = new HorizontalLayout();
			headerColumnLayout.gap = GAP;
			headerColumnLayout.paddingTop = 0;
			headerColumnLayout.paddingRight = 0;
			headerColumnLayout.paddingBottom = 0;
			headerColumnLayout.paddingLeft = 0;
			headerColumnLayout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			headerColumnLayout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_MIDDLE;
			_headerColumn.layout = headerColumnLayout;
			_headerColumn.addChild(this._artImage);
			_headerColumn.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			_headerColumn.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			
			_contentColumn = new ScrollContainer();
			_contentColumn.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_AUTO;
			_contentColumn.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			
			const contentColumnLayout:VerticalLayout = new VerticalLayout();
			contentColumnLayout.gap = GAP;
			contentColumnLayout.paddingTop = 0;
			contentColumnLayout.paddingRight = 0;
			contentColumnLayout.paddingBottom = 10;
			contentColumnLayout.paddingLeft = 0;
			contentColumnLayout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			contentColumnLayout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			this._contentColumn.layout = contentColumnLayout;
			this.addChild(_contentColumn);
			this._contentColumn.addChild(_headerColumn);
			this._questionTypeLabel = new Label();
			this._questionTypeLabel.text = YomstarGlobalConsts.SCALED_QUESTION_TYPE_TITLE;
			this._contentColumn.addChild(this._questionTypeLabel);
			
			this._questionTextLabel = new Label();
			this._questionTextLabel.text = this._question.questionText;
			this._headerColumn.addChild(this._questionTextLabel);
			
			this._selectedMoodTxt = new Label();
			this._contentColumn.addChild(this._selectedMoodTxt);
			
			_len = _question.options.length;
			
			this._slider = new Slider();
			this._slider.minimum = 0;
			this._slider.maximum = (_len-1)*10;
			this._slider.value = 0;
			this._slider.direction = Slider.DIRECTION_HORIZONTAL;
			this._slider.liveDragging = true;
			this._slider.addEventListener(Event.CHANGE, slider_changeHandler);
			this._contentColumn.addChild(this._slider);
			slider_changeHandler();
			this.touchable = true;
			super.initialize();
		}
		private function slider_changeHandler(event:Event = null):void
		{
			if(_len>1){
				this._selectedMoodTxt.text = _question.options[Math.round(this._slider.value/10)].description;
			}else{
				this._selectedMoodTxt.text = _question.options[0].description;
			}
			
		}
	}
}