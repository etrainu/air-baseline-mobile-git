package com.yomstar.app.ui.mobile.screens
{
	import com.greensock.TweenLite;
	import com.greensock.easing.Expo;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.comps.BusyIndicator;
	import com.yomstar.app.ui.mobile.comps.BusyIndicatorYommed;
	import com.yomstar.app.ui.mobile.comps.QuedObject;
	import com.yomstar.app.ui.mobile.comps.Spacer;
	import com.yomstar.app.ui.mobile.comps.TouchLayer;
	import com.yomstar.app.ui.mobile.comps.TransButton;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.ui.mobile.utils.SnapshotUtils;
	import com.yomstar.app.utils.ScaleUtils;
	
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Screen;
	import feathers.controls.popups.VerticalCenteredPopUpContentManager;
	
	import flash.display.BitmapData;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.system.System;
	import flash.utils.Timer;
	
	import pl.mateuszmackowiak.nativeANE.NativeDialogEvent;
	import pl.mateuszmackowiak.nativeANE.alert.NativeAlert;
	
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	
	public class YomScreen extends Screen
	{
		protected const ORIENTATION_PORTRAIT:String = "orientationPortrait";
		protected const ORIENTATION_LANDSCAPE:String = "orientationLandscape";
		protected var alert:NativeAlert;
		protected var popUpManager:VerticalCenteredPopUpContentManager;
		protected var busyIndicator:BusyIndicatorYommed = new BusyIndicatorYommed();
		protected var initalImage:Image;
		protected var headerLogoImage:Image;
		protected var header:Header;
		protected var settingsBtn:TransButton;
		protected var quedchildrenList:Vector.<QuedObject>;
		private var _snapshotView:Boolean;
		private var _touchLayer:TouchLayer;
		private var _snapshotImage:Image;
		private var _initialized:Boolean;
		public function YomScreen()
		{
			super();
			popUpManager = new VerticalCenteredPopUpContentManager();
			_snapshotView = false;
			_initialized= false;
			if(NativeAlert.isSupported){
				alert = NativeAlert(Main.alertPool.getObj());
				alert.closeHandler = onAlertClosed;
			}
		}

		public function get orientation():String
		{
			if(actualWidth>actualHeight){
				return ORIENTATION_LANDSCAPE;
			}else{
				return ORIENTATION_PORTRAIT;
			}
		}

		override protected function draw():void
		{
			header.width = actualWidth;
			header.height = 1.3*GlobalAppValues.SETIINGS_BUTTON_SIZE;
			header.validate();
			if(!_initialized){
				_initialized = true;		
				headerLogoImage.scaleX = headerLogoImage.scaleY =  ScaleUtils.getConstrainedScaleRatio(header.width*0.5,header.height*0.5, headerLogoImage);
				settingsBtn.width = settingsBtn.height = header.height-(0.3*header.height);
				settingsBtn.width = settingsBtn.height = GlobalAppValues.SETIINGS_BUTTON_SIZE;
				var settingsImage:Image = settingsBtn.defaultIcon as Image;
				settingsImage.scaleX = settingsImage.scaleY = ScaleUtils.getConstrainedScaleRatio(settingsBtn.width*0.7, settingsBtn.height*0.7, settingsImage);
			}
			headerLogoImage.x = (header.width- headerLogoImage.width)*0.5;
			headerLogoImage.y = (header.height- headerLogoImage.height)*0.5;
			settingsBtn.y = (header.height- settingsBtn.height)*0.5;
			initalImage.scaleX = initalImage.scaleY = ScaleUtils.getConstrainedScaleRatio(actualWidth*0.5, actualHeight*0.5, initalImage);
			initalImage.x = (actualWidth-initalImage.width)*0.5;
			initalImage.y = header.height+((actualHeight-header.height-initalImage.height)*0.7);
			if(_touchLayer){
				_touchLayer.width = actualWidth;
				_touchLayer.height = actualHeight;
			}
		}
		override protected function initialize():void
		{
			initalImage = new Image(R.getAtlas().getTexture("BG_Rocket"));
			initalImage.touchable = false;
			addChild(initalImage);
			header = new Header();
			headerLogoImage = new Image(R.getAtlas().getTexture("Logo"));
			headerLogoImage.touchable = false;
			header.addChild(headerLogoImage);
			
			this.settingsBtn = new TransButton();
			var settingsImage:Image = new Image(R.getAtlas().getTexture("settings"));
			this.settingsBtn.defaultIcon = settingsImage;
			this.settingsBtn.defaultSelectedIcon = settingsImage;
			this.settingsBtn.addEventListener(starling.events.Event.TRIGGERED, settingsTriggeredHandler);
			this.addChild(this.header);
			this.header.rightItems = new <DisplayObject>
				[
					this.settingsBtn
				];
			
			var transitionCompleteTimer:Timer = Timer(Main.timerPool.getObj());
			transitionCompleteTimer.delay = 1000;
			transitionCompleteTimer.repeatCount = 1;
			transitionCompleteTimer.addEventListener(TimerEvent.TIMER_COMPLETE, transitionComplete, false, 0, true);
			transitionCompleteTimer.start();
			
		}
		public function mergeLayers(useSnapshot:Boolean = false):void{
			if(useSnapshot){
				if(!_snapshotImage){
					_snapshotImage = new Image(Texture.fromBitmapData(SnapshotUtils.copyAsBitmapData(this, actualWidth, actualHeight)));
				}else{
					_snapshotImage.texture = Texture.fromBitmapData(SnapshotUtils.copyAsBitmapData(this, actualWidth, actualHeight));
					_snapshotImage.readjustSize();
				}
				queAndRemoveChildren(true);
				addChild(_snapshotImage);
			}else{
				queAndRemoveChildren(false);
			}
			
		}
		public function unmergeLayers():void{
			if(_snapshotImage){
				if(_snapshotImage.parent){
					_snapshotImage.removeFromParent(true);
				}
			}
			retieveQuedChildren();
		}
		public function slideSnapshotTo(destinationX:int=-1, destinationY:int=-1, duration:Number = 0.7, delay:Number=0, onCompleteFunction:Function=null, onCompleteParams:Array=null):void{
				TweenLite.killTweensOf(this);
				if(destinationX == -1){
					destinationX = this.x;
				}
				if(destinationY == -1){
					destinationY = this.y;
				}
				TweenLite.to(this, duration, {x:destinationX, y:destinationY,  ease:Expo.easeOut, delay:delay, onComplete:onCompleteFunction, onCompleteParams:onCompleteParams});
		}
		public function dragSnapshotTo(travelX:int=0, travelY:int=0, duration:Number = 0.7, onCompleteFunction:Function=null, onCompleteParams:Array=null):void{
			TweenLite.killTweensOf(this);
			TweenLite.to(this, duration, {x:this.x+(travelX), y:this.y+travelY,  ease:Expo.easeOut, onComplete:onCompleteFunction, onCompleteParams:onCompleteParams});
		}
		
		protected function settingsTriggeredHandler(event:starling.events.Event):void
		{
			dispatchEventWith("settings", false);
		}
		protected function queAndRemoveChildren(remove:Boolean = false):void{
			if(!quedchildrenList){
				quedchildrenList = new Vector.<QuedObject>();
			}else{
				quedchildrenList.length = 0;
			}
			if(remove){
				while(this.numChildren > 0){
					quedchildrenList.push(new QuedObject(this.getChildAt(0)));
					this.removeChildAt(0);
				}
			}else{
				var len:uint = this.numChildren;
				for(var i:int=0; i<len; i++){
					quedchildrenList.push(new QuedObject(this.getChildAt(i)));
				}
			}
			if(!_touchLayer){
				_touchLayer = new TouchLayer();
			}
			_touchLayer.width = actualWidth;
			_touchLayer.height = actualHeight;
			addChild(_touchLayer);
		}
		protected function retieveQuedChildren():void{
			if(quedchildrenList){
				var len:uint = quedchildrenList.length;
				var quedObject:QuedObject;
				for(var i:int=0; i<len; i++){
					quedObject = quedchildrenList[i];
					quedObject.restoreTouchability();
					this.addChild(quedObject.displayObject);
				}
				quedchildrenList.length = 0;
			}
			if(_touchLayer.parent){
				_touchLayer.removeFromParent();
			}
		}
		protected function onAlertClosed(event:NativeDialogEvent):void{
			Main.alertPool.returnObj(alert);
		}
		private function transitionComplete(e:TimerEvent):void{
			var transitionCompleteTimer:Timer = e.target as Timer;
			transitionCompleteTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, transitionComplete);
			transitionCompleteTimer.stop();
			Main.timerPool.returnObj(transitionCompleteTimer);
			run();
		}
		protected function run():void{
			
		}
		protected function fadeInitialImage():void{
			TweenLite.to(initalImage, 1, {alpha:0, onComplete:removeInitalImage});
		}
		protected function removeInitalImage():void{
			if(initalImage.parent){
				initalImage.removeFromParent();
			}
		}
		protected function creationComplete():void{
			System.pauseForGCIfCollectionImminent();
		}
		public function refresh():void{
			trace("Screen Refresh Request");
		}
		public function copyAsBitmapData():BitmapData{
			var support:RenderSupport = new RenderSupport();
			RenderSupport.clear(stage.color, 1.0);
			support.setOrthographicProjection(0, 0, width, height);
			stage.render(support, 1.0);
			support.finishQuadBatch();
			
			var result:BitmapData = new BitmapData(width, height, true);
			Starling.context.drawToBitmapData(result);
			
			return result
		}
	}
}