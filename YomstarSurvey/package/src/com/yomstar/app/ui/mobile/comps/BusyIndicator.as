package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.resources.R;
	import feathers.core.PopUpManager;
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class BusyIndicator extends Sprite
	{
		private const STAGE_DIM_COLOR:Number = 0x000000;
		private const STAGE_DIM_ALPHA:Number = 0.6;
		private var busyArt:MovieClip;
		private var stageDimmer:Quad;
		public function BusyIndicator()
		{
			super();
			stageDimmer = new Quad(10, 10, STAGE_DIM_COLOR);
			stageDimmer.alpha = STAGE_DIM_ALPHA;
		}
		public function start(dimStage:Boolean = true):void{
			Starling.current.stage.addEventListener(Event.RESIZE, onStageResized);
			
			this.addChildAt(stageDimmer, 0);
			if(!dimStage){
				stageDimmer.visible = false;
			}else{
				stageDimmer.visible = true;
			}
			if(!busyArt){
				busyArt = new MovieClip(R.getAtlas().getTextures("Busy"), 60);
			}
			this.addChild(busyArt);
			onStageResized();
			Starling.juggler.add(busyArt);
			PopUpManager.addPopUp(this, false, true);
		}
		public function stop():void{
			Starling.current.stage.removeEventListener(Event.RESIZE, onStageResized);
			if(stageDimmer.parent){
				stageDimmer.removeFromParent();
			}
			Starling.juggler.remove(busyArt);
			busyArt.removeFromParent();
			PopUpManager.removePopUp(this);
		}
		private function onStageResized(e:Event = null):void{
			stageDimmer.width = Starling.current.stage.stageWidth;
			stageDimmer.height = Starling.current.stage.stageHeight;
			busyArt.x = (Starling.current.stage.stageWidth-busyArt.width)*0.5;
			busyArt.y = (Starling.current.stage.stageHeight-busyArt.height)*0.5;
		}
			
	}
}