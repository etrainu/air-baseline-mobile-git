package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.dataObjects.Participant;
	import com.yomstar.app.dataObjects.Reward;
	import com.yomstar.app.dataObjects.RewardScheme;
	import com.yomstar.app.dataObjects.SampleDetail;
	import com.yomstar.app.events.ImageLoaderEvent;
	import com.yomstar.app.services.AvatarImageLoader;
	import com.yomstar.app.services.ParticipantDetails;
	import com.yomstar.app.services.RewardDetails;
	import com.yomstar.app.services.RewardSchemes;
	import com.yomstar.app.services.SampleDetails;
	import com.yomstar.app.services.StaticMapManager;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import feathers.controls.Label;
	import feathers.core.FeathersControl;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	
	import starling.display.DisplayObject;
	
	public class SampleDetailsExtension extends FeathersControl
	{
		private const NOT_AVAILABLE_TEXT:String = "NA";
		private const TEXTLINE_VERTICAL_GAP:uint = 5;
		private const VERTICAL_GAP:uint = 20;
		private const HORIZENTAL_GAP:uint = 20;
		private const PADDING_TOP:uint = 20;
		private const PADDING_RIGHT:uint = 20;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 20;
		private const MAP_WIDTH:uint = 115;
		private const MAP_HEIGHT:uint = 100;
		private const AVATAR_WIDTH:uint = 60;
		private const AVATAR_HEIGHT:uint = 60;
		
		
		
		private const INVALIDATION_FLAG_AVATAR:String = "avatarInvalid";
		private const INVALIDATION_FLAG_MAP:String = "mapInvalid";
		private const INVALIDATION_FLAG_PARTICIPANT:String = "participantInvalid";
		private const INVALIDATION_FLAG_REWARDSCHEME:String = "rewardSchemeInvalid";
		private var _backgroundSkin:DisplayObject;
		private var _initialized:Boolean;
		private var _sampleDetails:SampleDetail;
		private var _participantDetails:Participant;
		private var _rewardScheme:RewardScheme;
		private var _reward:Reward;
		private var _map:BorderedImage;
		private var _externalId:String;
		private var _avatar:BorderedImage;
		private var _staticMapManager:StaticMapManager;
		private var _avatarLoader:AvatarImageLoader
		private var _respondantTxt:Label;
		private var _emailTxt:Label;
		private var _rewardTxt:Label;
		private var _activatedTxt:Label;
		private var _redeemedTxt:Label;
		public function SampleDetailsExtension()
		{
			super();
			_initialized = false;
		}
		
		public function get externalId():String
		{
			return _externalId;
		}
		
		public function set externalId(value:String):void
		{
			_externalId = value;
			getSampleDetails(_externalId);
		}
		private function getSampleDetails(externalId:String):void{
			var sampleDetails:SampleDetails = new SampleDetails();
			sampleDetails.addEventListener(flash.events.Event.COMPLETE, onSampleDetails, false, 0, true);
			sampleDetails.getDetails(Main.data.token, externalId);
		}
		private function onSampleDetails(event:flash.events.Event):void
		{
			var sampleDetails:SampleDetails = event.target as SampleDetails;
			sampleDetails.removeEventListener(flash.events.Event.COMPLETE, onSampleDetails);
			trace("SampleDetails.sampleDetails Ext: "+SampleDetails.sampleDetails);
			this._sampleDetails = SampleDetails.sampleDetails;
			trace("this._sampleDetails.participant: "+this._sampleDetails.participant);
			this.invalidate(INVALIDATION_FLAG_MAP);
			if(this._sampleDetails.participant && this._sampleDetails.participant != "" && this._sampleDetails.participant!= "undefined"){
				getParticipantDetails(this._sampleDetails.participant);
			}else{
				trace("Participant data not available");
				this.invalidate(INVALIDATION_FLAG_PARTICIPANT);
			}
		}
		private function getParticipantDetails(participantId:String):void{
			trace("getParticipantDetails for "+participantId);
			var participantDetails:ParticipantDetails = new ParticipantDetails();
			participantDetails.addEventListener(flash.events.Event.COMPLETE, onParticipantDetails, false, 0, true);
			participantDetails.getParticipant(Main.data.token, participantId);
		}
		private function onParticipantDetails(event:flash.events.Event):void
		{
			var participantDetails:ParticipantDetails = event.target as ParticipantDetails;
			participantDetails.removeEventListener(flash.events.Event.COMPLETE, onSampleDetails);
			trace("participantDetails.participantDetails Ext: "+ParticipantDetails.participantDetails);
			//SAMPLE RESULT: {"id":"veO1JqtnSvCMJu5TR7sJyg","firstName":"no","lastName":"display","email":"tomj89%33@hotmail.com","directMarketing":false,"dateCreated":"2013-01-24T16:54:51","account":{"id":"AU9Rp2qgT2OjccEINWJQ","name":"bree's company"}}
			this._participantDetails = ParticipantDetails.participantDetails;
			this.invalidate(INVALIDATION_FLAG_PARTICIPANT);
			getRewardScheme(SampleDetails.sampleDetails.rewardCode);
		}
		
		private function getRewardScheme(rewardSchemeId:String):void{
			trace("getRewardScheme for "+rewardSchemeId);
			var rewardScheme:RewardSchemes = new RewardSchemes();
			rewardScheme.addEventListener(flash.events.Event.COMPLETE, onRewardScheme, false, 0, true);
			rewardScheme.getRewardSchemeById(Main.data.token, rewardSchemeId);
		}
		private function onRewardScheme(event:flash.events.Event):void
		{
			var rewardScheme:RewardSchemes = event.target as RewardSchemes;
			rewardScheme.removeEventListener(flash.events.Event.COMPLETE, onRewardScheme);
			trace("RewardSchemes.rewardScheme: "+RewardSchemes.rewardScheme);
			//SAMPLE RESULT: {"id":"yWxtxIo1TK3V59kLRYA2Q","name":"Reward 240113","description":"get something for free....","termsAndConditions":"someting","termsAndConditionsURL":"http://www.etrainu.com/","redemptionCount":0,"rewardCount":5,"offerFrom":"2013-01-24T00:00:00","offerUntil":"2013-01-25T23:59:59","redeemFrom":"2013-01-24T00:00:00","redeemUntil":"2013-01-25T23:59:59","dateCreated":"2013-01-24T16:38:56","lastUpdated":"2013-01-24T16:38:56","lastRewardGenerated":"2013-01-24T16:54:00","baseline":"6HkmeFYJRYe2LzVavApb8g","baselineName":"Notifications","createdBy":"MnpPF0BdT065SCX06wSfSg","status":"ACTIVE"}
			this._rewardScheme = RewardSchemes.rewardScheme;
			//getReward(SampleDetails.sampleDetails.rewardCode);
			getReward("NY77I255");
		}
		
		private function getReward(rewardId:String):void{
			trace("getReward for "+rewardId);
			var rewardDetails:RewardDetails = new RewardDetails();
			rewardDetails.addEventListener(flash.events.Event.COMPLETE, onReward, false, 0, true);
			rewardDetails.getRewardDetailsById(Main.data.token, rewardId);
		}
		private function onReward(event:flash.events.Event):void
		{
			var rewardDetails:RewardDetails = event.target as RewardDetails;
			rewardDetails.removeEventListener(flash.events.Event.COMPLETE, onReward);
			trace("RewardDetails.rewardDetails: "+RewardDetails.rewardDetails);
			//SAMPLE RESULT REDEEMED: {"code":"NY77I255","activatedDate":"2013-01-21T16:47:22","dateCreated":"2013-01-21T16:47:03","redeemedDate":"2013-01-22T11:17:31","rewardScheme":"wgg2bCO1QeyFEPYukaUHSQ","sample":"TTHu5ZjQaOg3GSUY1qhg","requiresPin":true,"activatedBy":{"id":"uqqfv04WReklgw7eUT6mQ","firstName":"tom","lastName":"jerry","email":"breesimmons@etrainu.com"},"redeemedBy":{"id":"uqqfv04WReklgw7eUT6mQ","firstName":"tom","lastName":"jerry","email":"breesimmons@etrainu.com"}}
			//SAMPLE RESULT NOT REDEEMED: {"code":"NXJG9A65","activatedDate":"2013-01-24T16:54:51","dateCreated":"2013-01-24T16:54:00","rewardScheme":"yWxtxIo1TK3V59kLRYA2Q","sample":"TWmwey3zT6G5j2ejYM6Bg","requiresPin":true,"activatedBy":{"id":"veO1JqtnSvCMJu5TR7sJyg","firstName":"no","lastName":"display","email":"tomj89%33@hotmail.com"}}
			this._reward = RewardDetails.rewardDetails;
			this.invalidate(INVALIDATION_FLAG_REWARDSCHEME);
		}
		
		public function get backgroundSkin():DisplayObject
		{
			return _backgroundSkin;
		}
		
		public function set backgroundSkin(value:DisplayObject):void
		{
			_backgroundSkin = value;
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		
		override protected function draw():void
		{
			super.draw();
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			const mapInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_MAP);
			const avatarInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_AVATAR);
			const participantInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_PARTICIPANT);
			const rewardInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_REWARDSCHEME);
			var sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			
			if(!_initialized){
				_initialized = true;
				this._respondantTxt.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._emailTxt.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_TEAXT_FORMAT)};
				this._rewardTxt.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.LIST_DETAILS_TEXT_FORMAT)};
				this._activatedTxt.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.LIST_DETAILS_TEXT_FORMAT)};
				this._redeemedTxt.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.LIST_DETAILS_TEXT_FORMAT)};
			}
			if(dataInvalid)
			{
				this.commitData();
			}
			if(mapInvalid)
			{
				this.commitMapData();
			}
			if(avatarInvalid)
			{
				this.commitAvatar();
			}
			if(participantInvalid)
			{
				this.commitParticipantData();
			}
			if(rewardInvalid)
			{
				this.commitRewardData();
			}
			this._respondantTxt.validate();
			this._emailTxt.validate();
			this._rewardTxt.validate();
			this._activatedTxt.validate();
			this._redeemedTxt.validate();
			this._avatar.validate();
			this._map.validate();
			sizeInvalid = this.autoSizeIfNeeded() || sizeInvalid;
			
			if(dataInvalid || sizeInvalid || mapInvalid || avatarInvalid)
			{
				this.layout();
			}
		}
		protected function layout():void{
			super.draw();
			if(this._backgroundSkin){
				this._backgroundSkin.width = this.actualWidth;
				this._backgroundSkin.height = this.actualHeight;
			}
			this._map.x = this.actualWidth- PADDING_RIGHT-this._map.width;
			this._map.y = (this.actualHeight- this._map.height)*0.5;
			
			var leftSide:uint;
			if(this._avatar){
				this._avatar.x = PADDING_LEFT;
				this._avatar.y = PADDING_TOP;
				leftSide = PADDING_LEFT+this._avatar.width+HORIZENTAL_GAP;
			}else{
				leftSide = PADDING_LEFT;
			}
			var infoRealestate:int = this._map.x-leftSide-HORIZENTAL_GAP;
			this._respondantTxt.x = leftSide;
			this._respondantTxt.y = PADDING_TOP;
			this._respondantTxt.width = infoRealestate;
			this._respondantTxt.validate();
			this._emailTxt.x = leftSide;
			this._emailTxt.y = this._respondantTxt.y+this._respondantTxt.height;
			this._emailTxt.width = infoRealestate;
			this._emailTxt.validate();
			this._rewardTxt.x = leftSide;
			this._rewardTxt.y = this._emailTxt.y+this._emailTxt.height;
			this._rewardTxt.width = infoRealestate;
			this._rewardTxt.validate();
			this._activatedTxt.x = leftSide;
			this._activatedTxt.y = this._rewardTxt.y+this._rewardTxt.height;
			this._activatedTxt.width = infoRealestate;
			this._activatedTxt.validate();
			this._redeemedTxt.x = leftSide;
			this._redeemedTxt.y = this._activatedTxt.y+this._activatedTxt.height;
			this._redeemedTxt.width = infoRealestate;
			this._redeemedTxt.validate();
		}
		protected function autoSizeIfNeeded():Boolean
		{
			const needsWidth:Boolean = isNaN(this.explicitWidth);
			const needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			this._respondantTxt.width = NaN;
			this._respondantTxt.height = NaN;
			this._respondantTxt.validate();
			
			this._emailTxt.width = NaN;
			this._emailTxt.height = NaN;
			this._emailTxt.validate();
			
			this._rewardTxt.width = NaN;
			this._rewardTxt.height = NaN;
			this._rewardTxt.validate();
			
			this._activatedTxt.width = NaN;
			this._activatedTxt.height = NaN;
			this._activatedTxt.validate();
			
			this._redeemedTxt.width = NaN;
			this._redeemedTxt.height = NaN;
			this._redeemedTxt.validate();
			
			
			var newWidth:Number = this.explicitWidth;
			if(needsWidth)
			{
				if(this._backgroundSkin){
					newWidth = this._backgroundSkin.width;
				}
				
			}
			var newHeight:Number = this.explicitHeight;
			
			if(needsHeight)
			{
				if(this._redeemedTxt){
					newHeight = _redeemedTxt.height+_redeemedTxt.y+PADDING_BOTTOM;
				}
			}
			return this.setSizeInternal(newWidth, newHeight, false);
		}
		protected function commitData():void
		{
			this.reset();
			if(this._backgroundSkin){
				addChildAt(this._backgroundSkin, 0);
			}
		}
		protected function commitMapData():void{
			if(this._sampleDetails){
				if(this._sampleDetails.isGeolocated){
					this._map.texture = R.getAtlas().getTexture("locationloading");
					var geoData:GeoData = new GeoData();
					geoData.init(this._sampleDetails.latitude, this._sampleDetails.longitude, GeoData.TILE_FORMAT_PNG, 5, this._map.texture.width, this._map.texture.height);
					if(!_staticMapManager){
						_staticMapManager = new StaticMapManager();
					}
					
					_staticMapManager.addEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onMapRecieved, false, 0, true);
					_staticMapManager.getMapTile(geoData);
				}else{
					this._map.texture = R.getAtlas().getTexture("locationnotavailable");
				}
			}
		}
		protected function commitAvatar():void{
			if(this._avatar){
				this.addChild(this._avatar);
			}
		}
		protected function commitRewardData():void{
			if(this._rewardScheme){
				this._rewardTxt.text = "Reward: "+this._rewardScheme.name;
			}else{
				this._rewardTxt.text = "Reward: "+this.NOT_AVAILABLE_TEXT;
			}
			if(this._reward){
				if(this._reward.redeemedDate){
					this._redeemedTxt.text = "Redeemed: "+this._reward.redeemedDate;
				}else{
					this._redeemedTxt.text = "Redeemed: "+this.NOT_AVAILABLE_TEXT;
				}
			}else{
				this._redeemedTxt.text = "Redeemed: "+this.NOT_AVAILABLE_TEXT;
			}
		}
		private function removeMapListeners():void{
			if(_staticMapManager){
				if(_staticMapManager.hasEventListener(ImageLoaderEvent.IMAGE_RECIEVED)){
					_staticMapManager.removeEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onMapRecieved);
				}
			}
		}
		private function onMapRecieved(e:ImageLoaderEvent):void{
			trace("onMapRecieved");
			this._map.texture.dispose();
			removeMapListeners();
			this._map.texture = e.imageTexture;
		}
		
		protected function commitParticipantData():void{
			var respondatnEmail:String = "nomail@etrainu.com";
			if(this._participantDetails){
				respondatnEmail = this._participantDetails.email;
				this._respondantTxt.text = "Respondent: "+this._participantDetails.firstName+" "+this._participantDetails.lastName;
				this._emailTxt.text = "Email: "+this._participantDetails.email
				this._activatedTxt.text = "Activated: "+this._participantDetails.dateCreated.replace("T", " at ");
			}else{
				this._respondantTxt.text = "Respondent: "+this.NOT_AVAILABLE_TEXT;
				this._emailTxt.text = "Email: "+this.NOT_AVAILABLE_TEXT;
				this._activatedTxt.text = "Activated: "+this.NOT_AVAILABLE_TEXT;
			}
			if(!_avatarLoader){
				_avatarLoader = new AvatarImageLoader();
			}
			_avatarLoader.addEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
			_avatarLoader.addEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
			_avatarLoader.getAvatar(respondatnEmail, AVATAR_WIDTH);
		}
		private function removeAvatarListeners():void{
			if(this._avatarLoader){
				if(this._avatarLoader.hasEventListener(ImageLoaderEvent.IMAGE_RECIEVED)){
					_avatarLoader.removeEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
				}
				if(this._avatarLoader.hasEventListener(IOErrorEvent.IO_ERROR)){
					_avatarLoader.removeEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
				}
			}
		}
		private function onAvatarLoadError(e:IOErrorEvent):void{
			removeAvatarListeners();
			trace("Avatar load error: "+e.text);
		}
		private function onAvatarLoaded(e:ImageLoaderEvent):void{
			trace("onAvatarRecieved");
			removeAvatarListeners();
			this._avatar.texture= e.imageTexture;
			this.invalidate(INVALIDATION_FLAG_AVATAR);
		}
		override protected function initialize():void
		{
			super.initialize();
			if(!this._map){
				this._map = new BorderedImage(null, 3, GlobalAppValues.FEATURED_COLOR_TWO, MAP_WIDTH, MAP_HEIGHT);
			}
			if(!_avatar){
				this._avatar = new BorderedImage(null, 3, GlobalAppValues.FEATURED_COLOR_TWO, AVATAR_WIDTH, AVATAR_HEIGHT);
			}
			if(!this._respondantTxt){
				this._respondantTxt = new Label();
			}
			if(this._emailTxt){
				this._emailTxt.text = "";
			}else{
				this._emailTxt = new Label();
			}
			if(this._rewardTxt){
				this._rewardTxt.text = "";
			}else{
				this._rewardTxt = new Label();
			}
			if(!this._activatedTxt){
				this._activatedTxt = new Label();
			}
			if(!this._redeemedTxt){
				this._redeemedTxt = new Label();
			}
			reset();
			this.addChild(this._map);
			this.addChild(this._respondantTxt);
			this.addChild(this._emailTxt);
			this.addChild(this._rewardTxt);
			this.addChild(this._activatedTxt);
			this.addChild(this._redeemedTxt);
			this.invalidate();
		}
		public function reset():void{
			this._sampleDetails = null;
			this._participantDetails = null;
			this._respondantTxt.text = "";
			this._emailTxt.text = "";
			this._rewardTxt.text = "";
			this._activatedTxt.text = "";
			this._redeemedTxt.text = "";
			removeMapListeners();
			removeAvatarListeners();
			if(this._map){
				if(this._map.texture){
					this._map.texture.dispose();
				}
			}
			if(this._avatar){
				if(this._avatar.parent){
					this._avatar.removeFromParent();
				}
				if(this._avatar.texture){
					this._avatar.texture.dispose();
				}
			}
		}
		override public function dispose():void
		{
			// TODO Auto Generated method stub
			super.dispose();
			this._sampleDetails = null;
			this._participantDetails = null;
			if(this._backgroundSkin){
				this._backgroundSkin.dispose();
			}
			if(this._map){
				if(this._map.texture){
					this._map.texture.dispose();
				}
			}
			if(this._avatar){
				if(this._avatar.texture){
					this._avatar.texture.dispose();
				}
			}
		}
	}
}