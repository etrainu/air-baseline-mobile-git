package com.yomstar.app.ui.mobile.comps
{
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.Mood;
	import feathers.controls.Label;
	import feathers.core.FeathersControl;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	import flash.geom.Rectangle;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.ResizeEvent;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;

	public class ChoiceLine extends FeathersControl
	{
		private const NORMAL_PAD_COLOR:Number = 0xFFFFFF;
		private const SELECTED_PAD_COLOR:Number = 0x0099b2;
		private const PADDING:uint = 10;
		private var _pad:Quad;
		private var _optionTextLabel:Label;
		private var _initialized:Boolean;
		private var _isMooded:Boolean;
		private var _text:String;
		private var _selected:Boolean;
		private var _value:int;
		private var _moodImage:Image;
		
		public function ChoiceLine(text:String = "", selected:Boolean = false, value:int = -1, isMooded:Boolean = false)
		{
			this._text = text;
			this._selected = selected;
			this._isMooded = isMooded;
			this._value = value;
		}

		public function get value():int
		{
			return _value;
		}

		public function get selected():Boolean
		{
			return _selected;
		}

		public function set selected(value:Boolean):void
		{
			_selected = value;
			if(_selected){
				_pad.color = SELECTED_PAD_COLOR;
				this._optionTextLabel.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT);
			}else{
				_pad.color = NORMAL_PAD_COLOR;
				this._optionTextLabel.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT_DARK);
			}
			this.invalidate(INVALIDATION_FLAG_ALL);
			this.validate();
		}

		public function get text():String
		{
			this._text = this._optionTextLabel.text;
			return _text;
		}

		public function set text(value:String):void
		{
			this._text = value;
			this._optionTextLabel.text = this._text;
			this.invalidate(INVALIDATION_FLAG_ALL);
			this.validate();
		}

		override protected function initialize():void
		{
			this._pad = new Quad(10, 10, 0xFFFFFF);
			this.addChild(this._pad);
			this._optionTextLabel = new Label();
			this._optionTextLabel.text = this._text;
			this.addChild(this._optionTextLabel);
			
			if(_isMooded){
				switch(Mood.getMoodIdByValue(_value)){
					case -1:
						_moodImage =  new Image(R.getAtlas().getTexture("Mood0"));
						break
					case 0:
						_moodImage =  new Image(R.getAtlas().getTexture("Mood1"));
						break
					case 1:
						_moodImage =  new Image(R.getAtlas().getTexture("Mood2"));
						break
					case 2:
						_moodImage =  new Image(R.getAtlas().getTexture("Mood3"));
						break
					case 3:
						_moodImage =  new Image(R.getAtlas().getTexture("Mood4"));
						break
					case 4:
						_moodImage =  new Image(R.getAtlas().getTexture("Mood5"));
						break
					default:
						_moodImage =  new Image(R.getAtlas().getTexture("Mood0"));
				}
				_moodImage.smoothing = TextureSmoothing.BILINEAR;
				_moodImage.scaleX = _moodImage.scaleY = 0.5;
				this.addChild(_moodImage);
			}
			selected = _selected;
			super.initialize();
		}
		override protected function draw():void
		{
			if(!_initialized){
				//this._optionTextLabel.textRendererProperties.textFormat = TextFormats.getTextFormat(TextFormats.BLOCK_TEXT_FORMAT_DARK);
				this._optionTextLabel.textRendererProperties.wordWrap = true;
				this._initialized = true;
				this.invalidate();
			}
			var realEstateWidth:uint = this.actualWidth-(2*PADDING);
			if(this._moodImage){
				realEstateWidth = realEstateWidth-this._moodImage.width-PADDING;
				this._moodImage.x = PADDING+realEstateWidth;
			}
			this._optionTextLabel.width = realEstateWidth;
			this._optionTextLabel.x = PADDING;
			this._optionTextLabel.y = PADDING;
			this._optionTextLabel.validate();
			this._pad.x = this._pad.y = 0;
			this._pad.width = this.actualWidth;
			if(this._moodImage){
				this.height = this._pad.height = Math.max(this._optionTextLabel.height, this._moodImage.height)+(2*PADDING);
				this._moodImage.y = 0.5*(this._pad.height - this._moodImage.height);
			}else{
				this.height = this._pad.height = this._optionTextLabel.height+(2*PADDING);
			}
			this.dispatchEvent(new Event(Event.RESIZE));
			super.draw();
		}
		
	}
}