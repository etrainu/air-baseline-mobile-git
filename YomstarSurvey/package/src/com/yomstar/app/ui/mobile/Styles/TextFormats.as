package com.yomstar.app.ui.mobile.Styles
{
	import feathers.text.BitmapFontTextFormat;
	
	import flash.display.Bitmap;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import starling.text.BitmapFont;
	import starling.textures.Texture;
	
	public class TextFormats
	{
		public static const DEFAULT_TEAXT_FORMAT:String = "defaultTextFormat";
		public static const DEFAULT_TEAXT_FORMAT_BOLD:String = "defaultTextFormatBold";
		public static const DEFAULT_TEAXT_FORMAT_BOLD_HLT1:String = "defaultTextFormatBoldHLT1";
		public static const HIGHLIGHT_TEAXT_FORMAT:String = "highlightTextFormat";
		public static const HEADER_TEAXT_FORMAT:String = "headerTextFormat";
		public static const HEADER_BUTTON_TEXT_FORMAT:String = "headerButtonTextFormat";
		public static const ACCESSORY_BUTTON_TEXT_FORMAT:String = "accessoryButtonTextFormat";
		public static const SUB_HEADER_TEAXT_FORMAT:String = "subHeaderTextFormat";
		public static const SUB_HEADER_HIGHLIGHTED_TEAXT_FORMAT:String = "subHeaderHighlitedTextFormat";
		public static const GRAPH_FEATURED_TWO_TEAXT_FORMAT:String = "graphFeaturedTwoTextFormat";
		public static const GRAPH_FEATURED_FOUR_TEAXT_FORMAT:String = "graphFeaturedFourTextFormat";
		public static const GRAPH_FEATURED_FIVE_TEAXT_FORMAT:String = "graphFeaturedFiveTextFormat";
		public static const BLOCK_TEXT_FORMAT:String = "blockTextFormat";
		public static const BLOCK_TEXT_FORMAT_DARK:String = "blockTextFormatDark";
		public static const BLOCK_TEXT_FORMAT_HIGHLIGHT:String = "blockTextFormatHighlight";
		public static const LIST_TEXT_FORMAT:String = "listTextFormat";
		public static const LIST_DETAILS_TEXT_FORMAT:String = "listDetailsTextFormat";
		public static const LIST_DETAILS_HIGHLIGHT_TEXT_FORMAT:String = "listDetailsHighlightTextFormat";
		
		[Embed(source = "../../../../../../assets/fonts/OpenSans_0.png")]
		private static const OpenSansChars:Class;
		[Embed(source="../../../../../../assets/fonts/OpenSans.fnt", mimeType="application/octet-stream")]
		private static const OpenSansXML:Class;
		private static var openSansBitmapFontTexture:Texture;
		private static var openSansBitmapFont:BitmapFont;
		
		[Embed(source = "../../../../../../assets/fonts/OpenSansBold_0.png")]
		private static const OpenSansBoldChars:Class;
		[Embed(source="../../../../../../assets/fonts/OpenSansBold.fnt", mimeType="application/octet-stream")]
		private static const OpenSansBoldXML:Class;
		private static var OpenSansBoldBitmapFontTexture:Texture;
		private static var OpenSansBoldBitmapFont:BitmapFont;
		
		[Embed(source = "../../../../../../assets/fonts/OpenSans24_0.png")]
		private static const OpenSansChars24:Class;
		[Embed(source="../../../../../../assets/fonts/OpenSans24.fnt", mimeType="application/octet-stream")]
		private static const OpenSansXML24:Class;
		private static var openSansBitmapFontTexture24:Texture;
		private static var openSansBitmapFont24:BitmapFont;
		
		[Embed(source = "../../../../../../assets/fonts/OpenSansBold24_0.png")]
		private static const OpenSansBoldChars24:Class;
		[Embed(source="../../../../../../assets/fonts/OpenSansBold24.fnt", mimeType="application/octet-stream")]
		private static const OpenSansBoldXML24:Class;
		private static var OpenSansBoldBitmapFontTexture24:Texture;
		private static var OpenSansBoldBitmapFont24:BitmapFont;
		
		[Embed(source = "../../../../../../assets/fonts/OpenSansExtraBold35_0.png")]
		private static const OpenSansExtraBoldChars35:Class;
		[Embed(source="../../../../../../assets/fonts/OpenSansExtraBold35.fnt", mimeType="application/octet-stream")]
		private static const OpenSansExtraBoldXML35:Class;
		private static var OpenSansExtraBoldBitmapFontTexture35:Texture;
		private static var OpenSansExtraBoldBitmapFont35:BitmapFont;
		
		[Embed(source = "../../../../../../assets/fonts/OpenSansLight18_0.png")]
		private static const OpenSansLightChars18:Class;
		[Embed(source="../../../../../../assets/fonts/OpenSansLight18.fnt", mimeType="application/octet-stream")]
		private static const OpenSansLightXML18:Class;
		private static var OpenSansLightBitmapFontTexture18:Texture;
		private static var OpenSansLightBitmapFont18:BitmapFont;
		
		
		private static var defaultTextFormat:BitmapFontTextFormat;
		private static var defaultTextFormatBold:BitmapFontTextFormat;
		private static var defaultTextFormatBoldHLT1:BitmapFontTextFormat;
		private static var highlightTextFormat:BitmapFontTextFormat;
		private static var headerTextFormat:BitmapFontTextFormat;
		private static var headerButtonTextFormat:BitmapFontTextFormat;
		private static var accessoryButtonTextFormat:BitmapFontTextFormat;
		private static var subHeaderTextFormat:BitmapFontTextFormat;
		private static var subHeaderHighlightedTextFormat:BitmapFontTextFormat;
		private static var graphFeatuedTwoTextFormat:BitmapFontTextFormat;
		private static var graphFeatuedFourTextFormat:BitmapFontTextFormat;
		private static var graphFeatuedFiveTextFormat:BitmapFontTextFormat;
		private static var blockTextFormat:BitmapFontTextFormat;
		private static var blockTextFormatDark:BitmapFontTextFormat;
		private static var blockTextFormatHighlight:BitmapFontTextFormat;
		private static var listTextFormat:BitmapFontTextFormat;
		private static var listDetailsTextFormat:BitmapFontTextFormat;
		private static var listDetailsHighlightTextFormat:BitmapFontTextFormat;
		
		public static function initFonts():void{
			if(!openSansBitmapFont){
				var fontTexture:Texture = Texture.fromBitmap(new OpenSansChars());
				var fontDescriptorXML:XML = XML(new OpenSansXML());
				openSansBitmapFont = new BitmapFont(fontTexture, fontDescriptorXML)
			}
			if(!OpenSansBoldBitmapFont){
				var fontBoldTexture:Texture = Texture.fromBitmap(new OpenSansBoldChars());
				var fontBoldDescriptorXML:XML = XML(new OpenSansBoldXML());
				OpenSansBoldBitmapFont = new BitmapFont(fontBoldTexture, fontBoldDescriptorXML)
			}
			
			if(!openSansBitmapFont24){
				var fontTexture24:Texture = Texture.fromBitmap(new OpenSansChars24());
				var fontDescriptorXML24:XML = XML(new OpenSansXML24());
				openSansBitmapFont24 = new BitmapFont(fontTexture24, fontDescriptorXML24)
			}
			if(!OpenSansBoldBitmapFont24){
				var fontBoldTexture24:Texture = Texture.fromBitmap(new OpenSansBoldChars24());
				var fontBoldDescriptorXML24:XML = XML(new OpenSansBoldXML24());
				OpenSansBoldBitmapFont24 = new BitmapFont(fontBoldTexture24, fontBoldDescriptorXML24)
			}
			if(!OpenSansExtraBoldBitmapFont35){
				var fontExtraBoldTexture35:Texture = Texture.fromBitmap(new OpenSansExtraBoldChars35());
				var fontExtraBoldDescriptorXML35:XML = XML(new OpenSansExtraBoldXML35());
				OpenSansExtraBoldBitmapFont35 = new BitmapFont(fontExtraBoldTexture35, fontExtraBoldDescriptorXML35)
			}
			if(!OpenSansLightBitmapFont18){
				var fontLightTexture18:Texture = Texture.fromBitmap(new OpenSansLightChars18());
				var fontLightDescriptorXML18:XML = XML(new OpenSansLightXML18());
				OpenSansLightBitmapFont18 = new BitmapFont(fontLightTexture18, fontLightDescriptorXML18)
			}
		}
		
		public static function getDefaultFont():BitmapFont{
			initFonts();
			return openSansBitmapFont
		}
		
		public static function getTextFormat(textType:String = "", forcedAlignment:String="", forcedColor:Number=-1):BitmapFontTextFormat{
			initFonts();
			if(textType == ""){
				textType = DEFAULT_TEAXT_FORMAT;
			}
			switch(textType){
				case DEFAULT_TEAXT_FORMAT://Default text format here
					if(!defaultTextFormat){
						defaultTextFormat = new BitmapFontTextFormat(openSansBitmapFont, 30, GlobalAppValues.WHITE_COLOR, TextFormatAlign.LEFT);
					}
					return forceStyle(defaultTextFormat, forcedAlignment, forcedColor);
					break
				case DEFAULT_TEAXT_FORMAT_BOLD://Default text format bold here
					if(!defaultTextFormatBold){
						defaultTextFormatBold = new BitmapFontTextFormat(OpenSansBoldBitmapFont, 30, GlobalAppValues.WHITE_COLOR, TextFormatAlign.LEFT);
					}
					return forceStyle(defaultTextFormatBold, forcedAlignment, forcedColor);
					break
				case DEFAULT_TEAXT_FORMAT_BOLD_HLT1://Default text format bold Highlight1 here
					if(!defaultTextFormatBoldHLT1){
						defaultTextFormatBoldHLT1 = new BitmapFontTextFormat(OpenSansBoldBitmapFont, 30, GlobalAppValues.FEATURED_COLOR_ONE, TextFormatAlign.LEFT);
					}
					return forceStyle(defaultTextFormatBoldHLT1, forcedAlignment, forcedColor);
					break
				case HIGHLIGHT_TEAXT_FORMAT://Highlight text format here
					if(!highlightTextFormat){
						highlightTextFormat = new BitmapFontTextFormat(OpenSansExtraBoldBitmapFont35, 35, GlobalAppValues.FEATURED_COLOR_TWO, TextFormatAlign.LEFT);
					}
					return forceStyle(highlightTextFormat, forcedAlignment, forcedColor);
					break
				case HEADER_TEAXT_FORMAT://Config header text format here
					if(!headerTextFormat){
						headerTextFormat = new BitmapFontTextFormat(OpenSansBoldBitmapFont, 30, GlobalAppValues.FEATURED_COLOR_TWO, TextFormatAlign.CENTER);
					}
					return forceStyle(headerTextFormat, forcedAlignment, forcedColor);
					break
				case HEADER_BUTTON_TEXT_FORMAT://Config header button text format here
					if(!headerButtonTextFormat){
						headerButtonTextFormat = new BitmapFontTextFormat(OpenSansBoldBitmapFont24, 24, GlobalAppValues.WHITE_COLOR, TextFormatAlign.CENTER);
					}
					return forceStyle(headerButtonTextFormat, forcedAlignment, forcedColor);
					break
				case ACCESSORY_BUTTON_TEXT_FORMAT://Config accessory button text format here
					if(!accessoryButtonTextFormat){
						accessoryButtonTextFormat = new BitmapFontTextFormat(OpenSansBoldBitmapFont24, 24, GlobalAppValues.WHITE_COLOR, TextFormatAlign.CENTER);
					}
					return forceStyle(accessoryButtonTextFormat, forcedAlignment, forcedColor);
					break
				case SUB_HEADER_TEAXT_FORMAT://Config sub header text format here
					if(!subHeaderTextFormat){
						subHeaderTextFormat = new BitmapFontTextFormat(OpenSansBoldBitmapFont24, 24, GlobalAppValues.WHITE_COLOR, TextFormatAlign.LEFT);
					}
					return forceStyle(subHeaderTextFormat, forcedAlignment, forcedColor);
					break
				case SUB_HEADER_HIGHLIGHTED_TEAXT_FORMAT://Config sub header highlighted text format here
					if(!subHeaderHighlightedTextFormat){
						subHeaderHighlightedTextFormat = new BitmapFontTextFormat(OpenSansBoldBitmapFont24, 24, GlobalAppValues.FEATURED_COLOR_TWO, TextFormatAlign.LEFT);
					}
					return forceStyle(subHeaderHighlightedTextFormat, forcedAlignment, forcedColor);
					break
				case GRAPH_FEATURED_TWO_TEAXT_FORMAT://Graph Featured Two text format here
					if(!graphFeatuedTwoTextFormat){
						graphFeatuedTwoTextFormat = new BitmapFontTextFormat(OpenSansBoldBitmapFont24, 24, GlobalAppValues.FEATURED_COLOR_TWO, TextFormatAlign.CENTER);
					}
					return forceStyle(graphFeatuedTwoTextFormat, forcedAlignment, forcedColor);
					break
				case GRAPH_FEATURED_FOUR_TEAXT_FORMAT://Graph Featured Four text format here
					if(!graphFeatuedFourTextFormat){
						graphFeatuedFourTextFormat = new BitmapFontTextFormat(OpenSansBoldBitmapFont24, 24, GlobalAppValues.FEATURED_COLOR_FOUR, TextFormatAlign.CENTER);
					}
					return forceStyle(graphFeatuedFourTextFormat, forcedAlignment, forcedColor);
					break
				case GRAPH_FEATURED_FIVE_TEAXT_FORMAT://Graph Featured Five text format here
					if(!graphFeatuedFiveTextFormat){
						graphFeatuedFiveTextFormat = new BitmapFontTextFormat(OpenSansBoldBitmapFont24, 24, GlobalAppValues.FEATURED_COLOR_FIVE, TextFormatAlign.CENTER);
					}
					return forceStyle(graphFeatuedFiveTextFormat, forcedAlignment, forcedColor);
					break
				case BLOCK_TEXT_FORMAT://Config block text format here
					if(!blockTextFormat){
						blockTextFormat = new BitmapFontTextFormat(openSansBitmapFont24, 24, GlobalAppValues.WHITE_COLOR, TextFormatAlign.LEFT);
					}
					return forceStyle(blockTextFormat, forcedAlignment, forcedColor);
					break
				case BLOCK_TEXT_FORMAT_DARK://Config block text dark format here
					if(!blockTextFormatDark){
						blockTextFormatDark = new BitmapFontTextFormat(openSansBitmapFont24, 24, GlobalAppValues.MID_GRAY_COLOR, TextFormatAlign.LEFT);
					}
					return forceStyle(blockTextFormatDark, forcedAlignment, forcedColor);
					break
				case BLOCK_TEXT_FORMAT_HIGHLIGHT://Config block text highlight format here
					if(!blockTextFormatHighlight){
						blockTextFormatHighlight = new BitmapFontTextFormat(openSansBitmapFont24, 24, GlobalAppValues.FEATURED_COLOR_ONE, TextFormatAlign.LEFT);
					}
					return forceStyle(blockTextFormatHighlight, forcedAlignment, forcedColor);
					break
				case LIST_TEXT_FORMAT://Config list text format here
					if(!listTextFormat){
						listTextFormat = new BitmapFontTextFormat(OpenSansBoldBitmapFont, 30, GlobalAppValues.WHITE_COLOR, TextFormatAlign.LEFT);
					}
					return forceStyle(listTextFormat, forcedAlignment, forcedColor);
					break
				case LIST_DETAILS_TEXT_FORMAT://Config list Details text format here
					if(!listDetailsTextFormat){
						listDetailsTextFormat = new BitmapFontTextFormat(OpenSansLightBitmapFont18, 18, GlobalAppValues.WHITE_COLOR, TextFormatAlign.LEFT);
					}
					return forceStyle(listDetailsTextFormat, forcedAlignment, forcedColor);
					break
				case LIST_DETAILS_HIGHLIGHT_TEXT_FORMAT://Config list Details Highlight text format here
					if(!listDetailsHighlightTextFormat){
						listDetailsHighlightTextFormat = new BitmapFontTextFormat(OpenSansLightBitmapFont18, 18, GlobalAppValues.FEATURED_COLOR_TWO, TextFormatAlign.LEFT);
					}
					return forceStyle(listDetailsHighlightTextFormat, forcedAlignment, forcedColor);
					break
				default:
					throw new Error("Provided text type is not supported\nPlease use one of the static values provided by 'BitmapFontTextFormat' class");
			}
		}
		private static function forceStyle(format:BitmapFontTextFormat, forcedAlignment:String="", forcedColor:Number=-1):BitmapFontTextFormat{
			if(forcedAlignment != "" || forcedColor != -1){
				var forcedFormat:BitmapFontTextFormat = new BitmapFontTextFormat(format.font, format.size, format.color, format.align);
				if(forcedAlignment != ""){
					forcedFormat.align = forcedAlignment;
				}
				if(forcedColor != -1){
					forcedFormat.color = forcedColor;
				}	
				return forcedFormat
			}else{
				return format
			}
		}
	}
}