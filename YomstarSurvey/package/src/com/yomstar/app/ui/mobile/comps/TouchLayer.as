package com.yomstar.app.ui.mobile.comps
{
	import starling.display.Sprite;
	
	public class TouchLayer extends Sprite
	{
		private var transQuad:Spacer;
		public function TouchLayer()
		{
			super();
			transQuad = new Spacer();
			transQuad.visible = true;
			transQuad.touchable = true;
			transQuad.alpha = 0;
			addChild(transQuad);
		}
		
		override public function get height():Number
		{
			// TODO Auto Generated method stub
			return super.height;
			transQuad.height = height;
		}
		
		override public function get width():Number
		{
			// TODO Auto Generated method stub
			return super.width;
			transQuad.width = width;
		}
	}
}