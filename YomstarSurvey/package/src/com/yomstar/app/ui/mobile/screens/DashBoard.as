package com.yomstar.app.ui.mobile.screens
{
	import com.yomstar.app.config.SurveySorting;
	import com.yomstar.app.dataObjects.GeoData;
	import com.yomstar.app.dataObjects.Sample;
	import com.yomstar.app.events.ImageLoaderEvent;
	import com.yomstar.app.services.AccountDetails;
	import com.yomstar.app.services.AccountStats;
	import com.yomstar.app.services.AvatarImageLoader;
	import com.yomstar.app.services.PlansDetails;
	import com.yomstar.app.services.SurveyResponses;
	import com.yomstar.app.services.UserInfo;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.TextFormats;
	import com.yomstar.app.ui.mobile.comps.BorderedImage;
	import com.yomstar.app.ui.mobile.comps.BottomShadowedLine;
	import com.yomstar.app.ui.mobile.comps.FeathersImage;
	import com.yomstar.app.ui.mobile.comps.IconnedLabel;
	import com.yomstar.app.ui.mobile.comps.IconnedTextInput;
	import com.yomstar.app.ui.mobile.comps.LightPad;
	import com.yomstar.app.ui.mobile.comps.ListDetailsCalloutButton;
	import com.yomstar.app.ui.mobile.comps.ListDetailsCalloutContent;
	import com.yomstar.app.ui.mobile.comps.ListSurveysCalloutButton;
	import com.yomstar.app.ui.mobile.comps.Spacer;
	import com.yomstar.app.ui.mobile.renderers.SampleItemRenderer;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.utils.DateFormat;
	import com.yomstar.app.utils.ObjectDigger;
	
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollText;
	import feathers.controls.Scroller;
	import feathers.controls.TabBar;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.ITextRenderer;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	import feathers.skins.StandardIcons;
	
	import flash.events.ErrorEvent;
	import flash.events.IOErrorEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import pl.mateuszmackowiak.nativeANE.alert.NativeAlert;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;

	public class DashBoard extends YomScreen
	{
		private const TEXTLINE_VERTICAL_GAP:uint = 5;
		private const VERTICAL_GAP:uint = 20;
		private const HORIZENTAL_GAP:uint = 20;
		private const PADDING_TOP:uint = 20;
		private const PADDING_RIGHT:uint = 20;
		private const PADDING_LEFT:uint = 20;
		private const PADDING_BOTTOM:uint = 20;
		private var _initialized:Boolean;
		private var _avatar:BorderedImage;
		private var _welcomeText:Label;
		private var _planText:Label;
		private var _responsesText:IconnedLabel;
		private var _seperator:BottomShadowedLine;
		private var _list:List;
		private var _listdb:ListCollection;
		public function DashBoard()
		{
			_initialized = false;
			super();
		}
		override protected function draw():void
		{
			super.draw();
			if(!_initialized){
				_initialized = true;
				this._welcomeText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.HIGHLIGHT_TEAXT_FORMAT)};
				this._welcomeText.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
				
				this._planText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_HIGHLIGHTED_TEAXT_FORMAT)};
				this._planText.textRendererProperties.textFormat.align = TextFormatAlign.LEFT;
				
				this._responsesText.textRendererProperties = {wordWrap:true , isHTML:true, textFormat:TextFormats.getTextFormat(TextFormats.SUB_HEADER_HIGHLIGHTED_TEAXT_FORMAT)};
			}
			
			var realestateHight:int = this.actualHeight;
			var realestateWidth:int = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			const dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
			const styleInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STYLES);
			const stateInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_STATE);
			const sizeInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_SIZE);
			if(dataInvalid)
			{
				this.commitData();
			}
			this.header.validate();
			this._welcomeText.validate();
			if(dataInvalid || sizeInvalid)
			{
				this.layout();
			}
		}
		protected function commitData():void
		{
			if(UserInfo.userInfo){
				this._welcomeText.text = "Welcome "+UserInfo.userInfo.firstName+"!";
				this._responsesText.text = "Last 10 responses";
				this._responsesText.visible = true;
				this._seperator.visible = true;
				refreshAvatar();
				if(AccountDetails.accountDetails){
					_planText.text = "Plan: "+AccountDetails.accountDetails.planName;
				}
			}
			if(SurveyResponses.surveyResponses){
				displayResponses();
			}
		}
		protected function layout():void{
			if(this._avatar){
				this._avatar.x = PADDING_LEFT;
				this._avatar.y = PADDING_TOP+this.header.height;
				this._avatar.validate();
				this._welcomeText.width = actualWidth-this._avatar.x-this._avatar.width-HORIZENTAL_GAP-PADDING_RIGHT;
			}else{
				this._welcomeText.width = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			}
			this._welcomeText.validate();
			this._welcomeText.x = actualWidth-PADDING_RIGHT-_welcomeText.width;
			this._welcomeText.y = PADDING_TOP+this.header.height;
			this._welcomeText.validate();
			this._planText.x = this._welcomeText.x;
			this._planText.width = this._welcomeText.width;
			this._planText.y = this._welcomeText.y+this._welcomeText.height+TEXTLINE_VERTICAL_GAP;
			this._planText.validate();
			
			this._responsesText.x = PADDING_LEFT;
			this._responsesText.width = actualWidth-PADDING_LEFT-PADDING_RIGHT;
			
			if(this._avatar){
				this._responsesText.y = Math.max((this._avatar.y+this._avatar.height), (this._planText.y+this._planText.height))+PADDING_TOP+VERTICAL_GAP;
			}else{
				this._responsesText.y = (this._planText.y+this._planText.height)+PADDING_TOP+VERTICAL_GAP;
			}
			this._responsesText.validate();
			this._seperator.width = actualWidth;
			this._seperator.y = this._responsesText.y + this._responsesText.height+VERTICAL_GAP;
			this._seperator.validate();
			this._list.width = actualWidth;
			this._list.y = this._seperator.y + (this._seperator.height*0.5);
			this._list.height = actualHeight-this._list.y;
		}
		override protected function initialize():void
		{
			super.initialize();
			this.backButtonHandler = this.onBackButton;
			this._welcomeText = new Label();
			this.addChildAt(this._welcomeText, 0);
			this._planText = new Label();
			this.addChild(this._planText);
			
			this._responsesText = new IconnedLabel();
			this._responsesText.visible = false;
			this.addChild(this._responsesText);
			this._responsesText.icon = new FeathersImage(R.getAtlas().getTexture("Icon_Human"), GlobalAppValues.FEATURED_COLOR_TWO);
			this._seperator = new BottomShadowedLine();
			this._seperator.visible = false;
			
			_list = new List();
			this._list.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._list.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this._list.scrollerProperties.snapScrollPositionsToPixels = true;
			this._list.addEventListener(starling.events.Event.CHANGE, listChanged);
			this.addChild(this._list);
			this._list.itemRendererType = SampleItemRenderer;
			this.addChild(this._seperator);
			
		}
		private function listChanged(e:starling.events.Event):void
		{
			dispatchEventWith("sampleSelected", false, this._list.selectedItem);
		}
		override protected function run():void{
			super.run();
			busyIndicator.start();
			refresh();
		}
		
		override public function refresh():void
		{
			
			super.refresh();
			getUserInfo();
		}
		private function getUserInfo():void{
			var userInfo:UserInfo = new UserInfo();
			userInfo.addEventListener(flash.events.Event.COMPLETE, onUserInfo, false, 0, true);
			userInfo.addEventListener(flash.events.ErrorEvent.ERROR, onUserInfoError, false, 0, true);
			userInfo.getInfo(Main.data.token);
		}
		
		private function onUserInfoError(event:flash.events.ErrorEvent):void
		{
			var userInfo:UserInfo = event.currentTarget as UserInfo;
			userInfo.removeEventListener(flash.events.Event.COMPLETE, onUserInfo);
			userInfo.removeEventListener(flash.events.ErrorEvent.ERROR, onUserInfoError);
			if(alert){
				alert.message = event.text;
				alert.closeLabel = "OK";
				alert.title = "Error";
				alert.show();
			}
		}
		private function onUserInfo(event:flash.events.Event):void
		{
			var userInfo:UserInfo = event.target as UserInfo;
			userInfo.removeEventListener(flash.events.Event.COMPLETE, onUserInfo);
			userInfo.removeEventListener(flash.events.ErrorEvent.ERROR, onUserInfoError);
			this.invalidate(INVALIDATION_FLAG_DATA);
			getAccountDetails();
		}
		private function displayUserInfo():void{
			
			//this._welcomeText.text = "Welcome "+UserInfo.userInfo.firstName+"!";
			var avatarLoader:AvatarImageLoader = Main.avatarImageLoaderPool.getObj() as AvatarImageLoader;
			//avatarLoader.addEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
			//avatarLoader.addEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
			avatarLoader.getAvatar(UserInfo.userInfo.email);
		}
		private function getAccountDetails():void{
			var accountDetails:AccountDetails = new AccountDetails();
			accountDetails.addEventListener(flash.events.Event.COMPLETE, onAccountDetails, false, 0, true);
			accountDetails.addEventListener(flash.events.ErrorEvent.ERROR, onAccountDetailsError, false, 0, true);
			accountDetails.getDetails(Main.data.token);
		}
		private function onAccountDetailsError(event:flash.events.ErrorEvent):void
		{
			trace("onAccountStatsError: "+event.text);
			var accountDetails:AccountDetails = event.currentTarget as AccountDetails;
			accountDetails.removeEventListener(flash.events.Event.COMPLETE, onAccountDetails);
			accountDetails.removeEventListener(flash.events.ErrorEvent.ERROR, onAccountDetailsError);
			if(alert){
				alert.message = event.text;
				alert.closeLabel = "OK";
				alert.title = "Error";
				alert.show();
			}
		}
		private function onAccountDetails(event:flash.events.Event):void
		{
			this.invalidate(INVALIDATION_FLAG_DATA);
			var accountDetails:AccountDetails = event.currentTarget as AccountDetails;
			accountDetails.removeEventListener(flash.events.Event.COMPLETE, onAccountDetails);
			accountDetails.removeEventListener(flash.events.ErrorEvent.ERROR, onAccountDetailsError);
			//sample result id:bjS7OO13TiKrOe5OFioxKw, subscriberToken:d55cbe0ff9029fd3bc1be52303ceab67d3ab9b79, country:Australia, planId:21476, industry:elearning, dateCreated:2012-12-04T15:57:14, planName:YomStar Now, active:true, name:Amir's company
			//trace("Account Details: "+ObjectDeigger.digg(AccountDetails.accountDetails));
			getLatestResponses();
		}
		private function getLatestResponses():void{
			var latestResponses:SurveyResponses = new SurveyResponses();
			latestResponses.addEventListener(flash.events.Event.COMPLETE, onLatestResponses, false, 0, true);
			latestResponses.addEventListener(flash.events.ErrorEvent.ERROR, onLatestResponsesError, false, 0, true);
			latestResponses.getResponses(Main.data.token, 0, 10, SurveySorting.DESCENDING);
		}
		private function onLatestResponsesError(event:flash.events.ErrorEvent):void
		{
			trace("onLatestResponsesError: "+event.text);
			var latestResponses:SurveyResponses = event.currentTarget as SurveyResponses;
			latestResponses.removeEventListener(flash.events.Event.COMPLETE, onLatestResponses);
			latestResponses.removeEventListener(flash.events.ErrorEvent.ERROR, onLatestResponsesError);
			if(alert){
				alert.message = event.text;
				alert.closeLabel = "OK";
				alert.title = "Error";
				alert.show();
			}
			busyIndicator.stop();
			getAccountStats();
		}
		private function onLatestResponses(event:flash.events.Event):void
		{
			var latestResponses:SurveyResponses = event.currentTarget as SurveyResponses;
			latestResponses.removeEventListener(flash.events.Event.COMPLETE, onLatestResponses);
			latestResponses.removeEventListener(flash.events.ErrorEvent.ERROR, onLatestResponsesError);
			//sample result [{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"woluGR1hSlOJoGN3pEi9Ng","version":1,"latitude":-27.470933,"longitude":153.02350199999998,"averagePercentageScore":54,"isGeolocated":true,"dateCaptured":"2012-12-11T16:57:14","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":75,"responses":["option 2"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"CRHHD3pRy6zeZ1pxFhebA","version":1,"averagePercentageScore":41,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:37","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":50,"responses":["option 3"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"HbcyoJTgqOuoKwn4FOg","version":1,"averagePercentageScore":12,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:45","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":0,"responses":["option 1"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":25,"responses":["option 4"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"LeDRxjFQcqQKLQKT4tJVw","version":1,"averagePercentageScore":58,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:50","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":50,"responses":["option 3"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":50,"responses":["option 3"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"lkc5f39QAm1C4pFXELO4Q","version":1,"averagePercentageScore":100,"isGeolocated":false,"dateCaptured":"2012-12-11T16:58:56","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":75,"responses":["option 4"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":100,"responses":["option 1"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"jkKlDhWxTcuTKX5Dp4M2pg","version":1,"averagePercentageScore":66,"isGeolocated":false,"dateCaptured":"2012-12-11T16:59:01","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":100,"responses":["option 1"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"6tePcDq3RNO1CwCtd0zCw","version":1,"averagePercentageScore":41,"isGeolocated":false,"dateCaptured":"2012-12-11T16:59:07","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":50,"responses":["option 3"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"iKQfUMHKRc6KdViQU02M1A","version":1,"averagePercentageScore":66,"isGeolocated":false,"dateCaptured":"2012-12-11T16:59:17","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":25,"responses":["option 2"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":100,"responses":["option 1"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"SXWOaJPoRsevJJqL7pPww","version":1,"latitude":-27.470933,"longitude":153.02350199999998,"averagePercentageScore":70,"isGeolocated":true,"dateCaptured":"2012-12-11T16:59:34","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":50,"responses":["option 3"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":75,"responses":["option 2"]}]},{"baselineName":"Mobile App Test Survey","baselineExternalId":"WJGosSJaRU2KuPBank1bCg","externalId":"A4SuNuCIRh6ezMNQDE58Lw","version":1,"latitude":-27.470933,"longitude":153.02350199999998,"averagePercentageScore":62,"isGeolocated":true,"dateCaptured":"2012-12-11T16:59:51","questions":[{"text":"Question 1","externalId":"LKsx6blQlq7YgYGgj1KJA","score":75,"responses":["option 4"]},{"text":"Question 2","externalId":"4bR1w016SK6JSlgcX2cZFg","score":25,"responses":["option 4"]}]}]
			//trace("Latest Response: "+ObjectDeigger.digg(SurveyResponses.surveyResponses[0]));
			busyIndicator.stop();
			this.invalidate(INVALIDATION_FLAG_DATA);
			getAccountStats();
		}
		private function getAccountStats():void{
			var accountStats:AccountStats = new AccountStats();
			accountStats.addEventListener(flash.events.Event.COMPLETE, onAccountStats, false, 0, true);
			accountStats.addEventListener(flash.events.ErrorEvent.ERROR, onAccountStatsError, false, 0, true);
			accountStats.getStats(Main.data.token);
		}
		private function onAccountStatsError(event:flash.events.ErrorEvent):void
		{
			trace("onAccountStatsError: "+event.text);
			var accountStats:AccountStats = event.currentTarget as AccountStats;
			accountStats.removeEventListener(flash.events.Event.COMPLETE, onAccountStats);
			accountStats.removeEventListener(flash.events.ErrorEvent.ERROR, onAccountStatsError);
			if(alert){
				alert.message = event.text;
				alert.closeLabel = "OK";
				alert.title = "Error";
				alert.show();
			}
			
		}
		private function onAccountStats(event:flash.events.Event):void
		{
			var accountStats:AccountStats = event.currentTarget as AccountStats;
			accountStats.removeEventListener(flash.events.Event.COMPLETE, onAccountStats);
			accountStats.removeEventListener(flash.events.ErrorEvent.ERROR, onAccountStatsError);
			//sample result totalQRCodeScans:0, sampleCountForPeriod:0, qrCodeCount:1, activeBaseline:1, totalSampleCount:0, totalOverPlanLimit:0, totalRedemption:0
			//trace("Account Stats: "+ObjectDeigger.digg(AccountStats.accountStats));
			getPlanDetails();
		}
		
		private function getPlanDetails():void{
			var plansDetails:PlansDetails = new PlansDetails();
			plansDetails.addEventListener(flash.events.Event.COMPLETE, onPlansDetails, false, 0, true);
			plansDetails.addEventListener(flash.events.ErrorEvent.ERROR, onPlansDetailsError, false, 0, true);
			plansDetails.getPlans();
		}
		private function onPlansDetailsError(event:flash.events.ErrorEvent):void
		{
			trace("onAccountStatsError: "+event.text);
			var plansDetails:PlansDetails = event.currentTarget as PlansDetails;
			plansDetails.removeEventListener(flash.events.Event.COMPLETE, onPlansDetails);
			plansDetails.removeEventListener(flash.events.ErrorEvent.ERROR, onPlansDetailsError);
		}
		private function onPlansDetails(event:flash.events.Event):void
		{
			var plansDetails:PlansDetails = event.currentTarget as PlansDetails;
			plansDetails.removeEventListener(flash.events.Event.COMPLETE, onPlansDetails);
			plansDetails.removeEventListener(flash.events.ErrorEvent.ERROR, onPlansDetailsError);
			//sample result Plans Details[0]: isUpgradeable:true, isPublic:true, dateCreated:2012-11-27T11:26:52, isFree:true, _desc:Free, id:PkkevgdQS1yc16IWBhMvQ, planOrder:1, maxQRCode:5, planId:21476, sampleLimit:50, maxActiveSurvey:1, maxSites:0, name:YomStar Now
			//trace("Plans Details[0]: "+ObjectDeigger.digg(PlansDetails.plansDetails[0]));
			this.invalidate(INVALIDATION_FLAG_DATA);
		}
		
		private function displayResponses():void{//http://feathersui.com/documentation/feathers/controls/renderers/BaseDefaultItemRenderer.html
			_listdb = new ListCollection(SurveyResponses.surveyResponses.samples);
			this._list.itemRendererProperties.labelField = "baselineName";
			const layout:VerticalLayout = new VerticalLayout();
			layout.gap = 0;
			layout.paddingTop = 0;
			layout.paddingRight = 0;
			layout.paddingBottom = 0;
			layout.paddingLeft = 0;
			layout.horizontalAlign = VerticalLayout.HORIZONTAL_ALIGN_JUSTIFY;
			layout.verticalAlign = VerticalLayout.VERTICAL_ALIGN_TOP;
			layout.hasVariableItemDimensions = true;
			this._list.layout = layout;
			this._list.itemRendererProperties.accessoryFunction  = null//listItemAccessoryFunction;Expanding features for lists removed
			this._list.itemRendererProperties.isExpandable = false;
			this._list.itemRendererProperties.detailsLabelFunction = listItemDetailsFunction;
			this._list.scrollerProperties.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			this._list.scrollerProperties.horizentalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			this._list.scrollerProperties.snapScrollPositionsToPixels = true;
			_list.dataProvider = _listdb;
		}
		private function listItemDetailsFunction(o:Sample):String{
			var detailsStr:String = String(o.dateCaptured).replace("T", " at ");
			return detailsStr;
		}
		/*private function listItemAccessoryFunction(o:Sample):DisplayObject{
			var accessoryBtn:Button = new Button();
			accessoryBtn.defaultIcon = new Image(R.getAtlas().getTexture("Icon_Plus"));
			return accessoryBtn;
		}*/
		private function refreshAvatar():void{
			var avatarLoader:AvatarImageLoader = Main.avatarImageLoaderPool.getObj() as AvatarImageLoader;
			avatarLoader.addEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
			avatarLoader.addEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
			avatarLoader.getAvatar(UserInfo.userInfo.email);
		}
		private function onAvatarLoadError(e:IOErrorEvent):void{
			var avatarLoader:AvatarImageLoader = AvatarImageLoader(e.currentTarget);
			avatarLoader.removeEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
			avatarLoader.removeEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
			Main.staticMapManagerPool.returnObj(avatarLoader);
			trace("Avatar load error: "+e.text);
		}
		private function onAvatarLoaded(e:ImageLoaderEvent):void{
			trace("onAvatarRecieved");
			var avatarLoader:AvatarImageLoader = AvatarImageLoader(e.currentTarget);
			avatarLoader.removeEventListener(ImageLoaderEvent.IMAGE_RECIEVED, onAvatarLoaded);
			avatarLoader.removeEventListener(IOErrorEvent.IO_ERROR, onAvatarLoadError);
			if(!this._avatar){
				this._avatar = new BorderedImage(e.imageTexture, 5, GlobalAppValues.FEATURED_COLOR_TWO);
			}else{
				this._avatar.texture.dispose();
				this._avatar.texture= e.imageTexture;
			}
			this.addChild(this._avatar);
			this.invalidate(INVALIDATION_FLAG_LAYOUT);
			this.invalidate(INVALIDATION_FLAG_SIZE);
			Main.staticMapManagerPool.returnObj(avatarLoader);
		}
		
		override public function dispose():void
		{
			super.dispose();
			if(this._avatar){
				this._avatar.texture.dispose();
			}
		}
		
		
		private function onBackButton():void
		{
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
	}
}