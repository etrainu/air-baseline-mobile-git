package
{
	import com.gabob.common.GaObjectPool;
	import com.gabob.common.GaTimerPool;
	import com.gabob.common.GaURLRequestPool;
	import com.google.zxing.aztec.Point;
	import com.greensock.TweenLite;
	import com.greensock.easing.Expo;
	import com.yomstar.app.services.AccountDetails;
	import com.yomstar.app.services.AccountStats;
	import com.yomstar.app.services.AvatarImageLoader;
	import com.yomstar.app.services.PlansDetails;
	import com.yomstar.app.services.SSLResolver;
	import com.yomstar.app.services.StaticMapManager;
	import com.yomstar.app.services.SurveyResponses;
	import com.yomstar.app.services.UserInfo;
	import com.yomstar.app.ui.mobile.Config;
	import com.yomstar.app.ui.mobile.Styles.GlobalAppValues;
	import com.yomstar.app.ui.mobile.Styles.YomstarMobileTheme;
	import com.yomstar.app.ui.mobile.comps.Settings;
	import com.yomstar.app.ui.mobile.comps.SettingsRefresher;
	import com.yomstar.app.ui.mobile.resources.R;
	import com.yomstar.app.ui.mobile.screens.DashBoard;
	import com.yomstar.app.ui.mobile.screens.LatestResponses;
	import com.yomstar.app.ui.mobile.screens.SampleReview;
	import com.yomstar.app.ui.mobile.screens.SurveyDetails;
	import com.yomstar.app.ui.mobile.screens.SurveyList;
	import com.yomstar.app.ui.mobile.screens.SurveyView;
	import com.yomstar.app.ui.mobile.screens.UserLogin;
	import com.yomstar.app.ui.mobile.screens.WebViewScreen;
	import com.yomstar.app.ui.mobile.screens.YomScreen;
	import com.yomstar.app.ui.mobile.utils.HistoryManager;
	
	import feathers.controls.ScreenNavigator;
	import feathers.controls.ScreenNavigatorItem;
	import feathers.controls.TabBar;
	import feathers.core.FeathersControl;
	import feathers.data.ListCollection;
	import feathers.motion.transitions.ScreenSlidingStackTransitionManager;
	import feathers.system.DeviceCapabilities;
	
	import flash.display.Loader;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	
	import pl.mateuszmackowiak.nativeANE.alert.NativeAlert;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.ResizeEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class Main extends Sprite
	{
		public static var data:Object = {};
		public static var urlLoaderPool:GaObjectPool = new GaObjectPool(URLLoader, 2);
		public static var loaderPool:GaObjectPool = new GaObjectPool(Loader, 2);
		public static var alertPool:GaObjectPool = new GaObjectPool(NativeAlert, 2);
		public static var staticMapManagerPool:GaObjectPool = new GaObjectPool(StaticMapManager, 2);
		public static var avatarImageLoaderPool:GaObjectPool = new GaObjectPool(AvatarImageLoader, 2);
		public static var urlRequestPool:GaURLRequestPool = new GaURLRequestPool(2);
		public static var timerPool:GaTimerPool = new GaTimerPool(2);
		private static const USER_LOGIN:String = "userLogin";
		private static const DASHBOARD:String = "dashBoard";
		private static const LATEST_RESPONSES:String = "latestResponses";
		private static const ACTIVE_SURVEY_LIST:String = "activeSurveyList";
		private static const SURVEY_DETAILS:String = "surveyDetails";
		private static const SAMPLE_REVIEW:String = "sampleReview";
		private static const WEB_VIEW:String = "webView";
		private static const SURVEY_VIEW:String = "surveyView";
		private static const SETTINGS:String = "settingsView";
		
		private var nav:ScreenNavigator;
		private var sDetails:ScreenNavigatorItem;
		private var login:ScreenNavigatorItem;
		private var sampleReview:ScreenNavigatorItem;
		private var sWebView:ScreenNavigatorItem;
		private var sSurveyView:ScreenNavigatorItem;
		private var transition:ScreenSlidingStackTransitionManager;
		private var _settings:Settings;
		private var _sidedScreen:YomScreen;
		private var _tabBar:TabBar;
		private var _lastAppliedTravelPoint:Point;
		private var _theme:YomstarMobileTheme;
		public function Main()
		{
			_lastAppliedTravelPoint = new Point(Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY);
			addEventListener(Event.ADDED_TO_STAGE, init);
			Starling.current.showStats = true;
			Starling.current.showStatsAt("middle", "top");
			this._settings = new Settings();
			this._settings.addEventListener("logout", onSettingsLogedOutRequest);
			this._settings.addEventListener(SettingsRefresher.REFRESH_REQUEST, onRefreshRequest);
		}
		private function onRefreshRequest(e:Event):void{
			YomScreen(nav.activeScreen).refresh();
		}
		private function init(e:Event):void{
			stage.addEventListener(Event.RESIZE, onResize);
			_theme = new YomstarMobileTheme(stage);
			data.theme = _theme;
			const scaledDPI:int = DeviceCapabilities.dpi / Starling.contentScaleFactor;
			nav = new ScreenNavigator();
			nav.addEventListener(starling.events.Event.CHANGE, onScreenChange);
			addChild(nav);
			login = new ScreenNavigatorItem(UserLogin, {loginSuccess: onLogedIn}, null);
			login.properties = {autoLogin:true};
			nav.addScreen(USER_LOGIN, login);
			
			var dashBoard:ScreenNavigatorItem = new ScreenNavigatorItem(DashBoard, {complete: onLogOutRequest, settings:onSettingsRequest, sampleSelected:sampleSelected}, null);
			nav.addScreen(DASHBOARD, dashBoard);
			
			var latestResponses:ScreenNavigatorItem = new ScreenNavigatorItem(LatestResponses, {surveySelected: surveySelected, complete: onLogOutRequest, settings:onSettingsRequest, sampleSelected:sampleSelected}, null);
			nav.addScreen(LATEST_RESPONSES, latestResponses);
			
			var sList:ScreenNavigatorItem = new ScreenNavigatorItem(SurveyList, {surveySelected: surveySelected, complete: onBackRequest, settings:onSettingsRequest, webViewRequest: onWebViewRequest, surveyRequest:onSurveyRequest}, null);
			nav.addScreen(ACTIVE_SURVEY_LIST, sList);
			
			sWebView = new ScreenNavigatorItem(WebViewScreen, {complete: onBackRequest, settings:onSettingsRequest}, null);
			nav.addScreen(WEB_VIEW, sWebView);
			
			sSurveyView = new ScreenNavigatorItem(SurveyView, {complete: onBackRequest, settings:onSettingsRequest}, null);
			nav.addScreen(SURVEY_VIEW, sSurveyView);
			
			sDetails = new ScreenNavigatorItem(SurveyDetails, {complete: onBackRequest, settings:onSettingsRequest, sampleSelected:sampleSelected}, null);
			nav.addScreen(SURVEY_DETAILS, sDetails);

			sampleReview = new ScreenNavigatorItem(SampleReview, {complete: onBackRequest, settings:onSettingsRequest}, null);
			nav.addScreen(SAMPLE_REVIEW, sampleReview);
			
			transition = new ScreenSlidingStackTransitionManager(nav);
			transition.duration = Config.TRANSITION_DURATION;
			nav.showScreen(USER_LOGIN);
			var sslResolver:SSLResolver = new SSLResolver();
			sslResolver.resolve(Starling.current.nativeStage);
			
			this._tabBar = new TabBar();
			this._tabBar.addEventListener(starling.events.Event.CHANGE, tabBar_changeHandler);
			var dashoardImageSelected:Image = new Image(R.getAtlas().getTexture("DashBoardIcon"));
			dashoardImageSelected.color = GlobalAppValues.FEATURED_COLOR_ONE;
			var surveyImageSelected:Image = new Image(R.getAtlas().getTexture("SurveysIcon"));
			surveyImageSelected.color = GlobalAppValues.FEATURED_COLOR_ONE;
			var reportsImageSelected:Image = new Image(R.getAtlas().getTexture("ResponsesIcon"));
			reportsImageSelected.color = GlobalAppValues.FEATURED_COLOR_ONE;
			this._tabBar.dataProvider = new ListCollection(
				[
					{ label: "Dashboard", defaultIcon:new Image(R.getAtlas().getTexture("DashBoardIcon")), defaultSelectedIcon:dashoardImageSelected },
					{ label: "Surveys", defaultIcon: new Image(R.getAtlas().getTexture("SurveysIcon")), defaultSelectedIcon:surveyImageSelected  },
					{ label: "Reports", defaultIcon: new Image(R.getAtlas().getTexture("ResponsesIcon")), defaultSelectedIcon:reportsImageSelected  }
				]);
			//addChild(this._tabBar);
			this.updatePositions(this.stage.stageWidth, this.stage.stageHeight);
			tabBar_changeHandler(null);
		}
		private function tabBar_changeHandler(event:starling.events.Event):void
		{
			if(data.token){
				switch(this._tabBar.selectedIndex){
					case 0:
						nav.showScreen(DASHBOARD);
						break
					case 1:
						nav.showScreen(ACTIVE_SURVEY_LIST);
						break
					case 2:
						nav.showScreen(LATEST_RESPONSES);
						break
				}
			}
		}
		private function onScreenChange(event:Event):void
		{
			trace("screenChanged to "+nav.activeScreenID);
			HistoryManager.addScreen(nav.activeScreenID);
			if(nav.activeScreenID == USER_LOGIN || nav.activeScreenID == SURVEY_DETAILS || nav.activeScreenID == SAMPLE_REVIEW){//Removing tab bar from some screens
				if(this.stage){
					if(this.stage.stageWidth && _tabBar){
						TweenLite.killTweensOf(_tabBar);
						_tabBar.flatten();
						_tabBar.touchable = false;
						TweenLite.to(_tabBar, 0.7, {y:this.stage.stageHeight, ease:Expo.easeOut, onComplete:tabBarRemoved});
					}
				}
			}else{
				if(!_tabBar.parent){
					addChild(_tabBar);
					_tabBar.unflatten();
					_tabBar.touchable = true;
					this._tabBar.validate();
					if(this.stage){
						if(this.stage.stageWidth){
							this.updatePositions(this.stage.stageWidth, this.stage.stageHeight);
							TweenLite.from(_tabBar, 0.7, {y:this.stage.stageHeight, ease:Expo.easeOut});
						}
					}
				}
			}
		}
		private function tabBarRemoved():void{
			removeChild(_tabBar);
			this.updatePositions(this.stage.stageWidth, this.stage.stageHeight);
		}
		private function onResize(event:ResizeEvent):void
		{
			updatePositions(event.width, event.height);
		}
		
		private function updatePositions(width:int, height:int):void
		{
			var scale:Number = Starling.current.contentScaleFactor;
			var viewPort:Rectangle = new Rectangle(0, 0, width, height);
			Starling.current.viewPort = viewPort;
			stage.stageWidth  = viewPort.width  / scale;
			stage.stageHeight = viewPort.height / scale;
			nav.width = width;
			nav.height = height;
			if(this._tabBar){
				if(this._tabBar.parent){
					this._tabBar.width = width;
					this._tabBar.x = (width - this._tabBar.width) / 2;
					this._tabBar.y = height - this._tabBar.height;
					this._tabBar.validate();
					nav.height = height-this._tabBar.height;
				}
			}
			nav.validate();
			if(_settings.parent){
				_settings.width=nav.width-Config.SLIDE_MENU_MARGIN;
				_settings.height=height;
				_settings.x = Config.SLIDE_MENU_MARGIN;
				if(_sidedScreen){
					_sidedScreen.x = -(nav.width-Config.SLIDE_MENU_MARGIN);
				}
			}
		}
		private function onWebViewRequest(e:Event, si:Object):void{
			sWebView.properties = {url:si.baseline.shortURL};
			nav.showScreen(WEB_VIEW);
		}
		private function onSurveyRequest(e:Event, si:Object):void{
			data.shareURI = si.shareURI;
			sSurveyView.properties = {baseline:si.baseline};
			nav.showScreen(SURVEY_VIEW);
		}
		private function onLogedIn(e:Event, si:Object):void{
			if(this._settings){
				this._settings.invalidate(FeathersControl.INVALIDATION_FLAG_DATA);
			}
			data.token = si.token;
			_tabBar.selectedIndex = 0;
			nav.showScreen(DASHBOARD);
		}
		private function onBackRequest(e:Event):void{
			nav.showScreen(HistoryManager.previousScreen());
		}
		private function onSettingsLogedOutRequest(e:Event):void{
			removeSettings(performSettingsLogout);
		}
		private function performSettingsLogout():void{
			settingsRemoved();
			onLogOutRequest();
		}
		private function onSettingsRequest(e:Event, si:Object):void{
			_sidedScreen = e.currentTarget as YomScreen;
			_sidedScreen.mergeLayers();
			_sidedScreen.slideSnapshotTo(-(nav.width-Config.SLIDE_MENU_MARGIN), -1, 0.7, 0.3, monitorSideBarTouch);
			TweenLite.killTweensOf(_tabBar);
			_tabBar.flatten();
			_tabBar.touchable = false;
			TweenLite.to(_tabBar, 0.7, {x:-(nav.width-Config.SLIDE_MENU_MARGIN), ease:Expo.easeOut, delay:0.3});
			_settings.width=nav.width-Config.SLIDE_MENU_MARGIN;
			_settings.height=stage.stageHeight;
			_settings.x = Config.SLIDE_MENU_MARGIN-GlobalAppValues.SHADOW_OVERLAP;
			addChild(_settings);
			TweenLite.killTweensOf(_settings);
			TweenLite.from(_settings, 0.7, {x:nav.width, ease:Expo.easeOut, delay:0.3});
		}
		private function monitorSideBarTouch():void{
			nav.addEventListener(starling.events.TouchEvent.TOUCH, onSnapshotTouch);
		}
		private function onSnapshotTouch(e:TouchEvent):void{
			var touch:Touch = e.touches[0];
			switch(touch.phase){
				case TouchPhase.ENDED:
					_lastAppliedTravelPoint.x=Number.NEGATIVE_INFINITY;
					removeSettings();
					break
				case TouchPhase.MOVED:
					var travelX:int;
					if(this._lastAppliedTravelPoint.x != Number.NEGATIVE_INFINITY){
						travelX = touch.globalX-this._lastAppliedTravelPoint.x;
					}else{
						travelX = touch.globalX-touch.previousGlobalX;
					}
					trace("travelX: "+travelX);
					trace("_theme.appScale: "+_theme.appScale);
					if(Math.abs(travelX) > 15){
						_lastAppliedTravelPoint.x = touch.globalX;
						TweenLite.killTweensOf(_settings);
						TweenLite.to(_settings, 0.7, {x:_settings.x+(travelX/_theme.appScale), ease:Expo.easeOut});
						_sidedScreen.dragSnapshotTo(travelX*0.7, 0, 0.7);
						TweenLite.to(_tabBar, 0.7, {x:_tabBar.x+(travelX*0.7), ease:Expo.easeOut});
					}
					break
			}
		}
		private function removeSettings(onCompleteFunction:Function = null):void{
			if(onCompleteFunction == null){
				onCompleteFunction = settingsRemoved;
			}
			TweenLite.killTweensOf(_settings);
			nav.removeEventListener(starling.events.TouchEvent.TOUCH, onSnapshotTouch);
			TweenLite.to(_settings, 0.7, {x:nav.width, ease:Expo.easeOut, delay:0, onComplete:onCompleteFunction});
			_sidedScreen.slideSnapshotTo(0, -1, 0.7, 0, _sidedScreen.unmergeLayers);
			
			TweenLite.to(_tabBar, 0.7, {x:0, ease:Expo.easeOut});
			_tabBar.unflatten();
			_tabBar.touchable = true;
		}
		private function settingsRemoved():void{
			removeChild(_settings);
		}
		private function onLogOutRequest(e:Event = null):void{
			HistoryManager.clearHistory();
			data.token = null;
			PlansDetails.plansDetails = null;
			UserInfo.userInfo = null;
			AccountStats.accountStats = null;
			AccountDetails.accountDetails = null;
			SurveyResponses.surveyResponses = null;
			login.properties = {autoLogin:false};
			nav.showScreen(USER_LOGIN);
		}
		private function surveySelected(e:Event, si:Object):void{
			sDetails.properties = {surveyObject:si};
			nav.showScreen(SURVEY_DETAILS);
		}
		private function sampleSelected(e:Event, si:Object):void{
			sampleReview.properties = {sampleObject:si};
			nav.showScreen(SAMPLE_REVIEW);
		}
	}
}