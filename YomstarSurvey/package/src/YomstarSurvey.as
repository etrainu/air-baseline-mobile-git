package
{
	import com.yomstar.app.ui.mobile.Config;
	import com.yomstar.app.ui.mobile.resources.R;
	
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageAspectRatio;
	import flash.display.StageDisplayState;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	import org.gestouch.core.Gestouch;
	import org.gestouch.extensions.starling.StarlingDisplayListAdapter;
	import org.gestouch.extensions.starling.StarlingTouchHitTester;
	import org.gestouch.input.NativeInputAdapter;
	
	import pl.mateuszmackowiak.nativeANE.alert.NativeAlert;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.Event;
	import com.yomstar.app.ui.mobile.utils.OSUtils;
	
	[SWF(frameRate="60", width="100",height="100", backgroundColor="#202224")]
	public class YomstarSurvey extends Sprite
	{
		private var _starling:Starling;
		[Embed(source="assets/images/logo.png")]
		private const SplashBitmap:Class;
		private var _splash:Bitmap;
		private var prevWidth:Number = 0;
		private var initialized:Boolean;

		public function YomstarSurvey()
		{
			initialized = false;
			this.mouseEnabled = this.mouseChildren = false;
			addEventListener(flash.events.Event.ENTER_FRAME, monitorStageSize, false, 0, true);
		}
		private function monitorStageSize(e:flash.events.Event):void{
			if(stage){
				if(stage.width != prevWidth){
					prevWidth = stage.width;
					stage_resizeHandler();
				}
				if(!initialized){
					init();
				}
			}else{
				trace("no stage");
			}
		}
		private function setupStarling():void
		{
			Starling.handleLostContext = !OSUtils.isIOS();
			Starling.multitouchEnabled = true;
			this._starling = new Starling(Main, this.stage);
			_starling.addEventListener(starling.events.Event.CONTEXT3D_CREATE, removeSplash);
			Gestouch.inputAdapter ||= new NativeInputAdapter(stage);
			Gestouch.addDisplayListAdapter(starling.display.DisplayObject, new StarlingDisplayListAdapter());
			Gestouch.addTouchHitTester(new StarlingTouchHitTester(_starling), -1);
		}
		private function init(e:flash.events.Event = null):void{
			initialized = true;
			this.stage.addEventListener(flash.events.Event.RESIZE, stage_resizeHandler);
			this.stage.addEventListener(flash.events.Event.DEACTIVATE, stage_deactivateHandler);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.EXITING, onAppExiting);
			this.stage.scaleMode = StageScaleMode.NO_SCALE;
			this.stage.align = StageAlign.TOP_LEFT;
			this.stage.displayState = StageDisplayState.NORMAL;
			setupSplash();
			setupStarling();
		}
		private function setupAppScreen():void{
			this.stage.displayState = StageDisplayState.NORMAL;
			this.stage.autoOrients = true;
		}
		private function setupSplash():void{
			_splash = new SplashBitmap() as Bitmap;
			_splash.addEventListener(flash.events.Event.ADDED_TO_STAGE, onSplashStart, false, 0, true);
			addChild(_splash);
			
		}
		private function onAppExiting(e:flash.events.Event):void{
			NativeAlert.dispose();
		}
		private function onSplashStart(e:flash.events.Event):void{
			stage_resizeHandler();
			_splash.removeEventListener(flash.events.Event.ADDED_TO_STAGE, onSplashStart);
			alignSplash();
		}
		private function alignSplash():void{
			_splash.x = 0.5*(stage.stageWidth-_splash.width);
			_splash.y = 0.75*(stage.stageHeight-_splash.height);
		}
		private function removeSplash(e:starling.events.Event):void{
			removeEventListener(flash.events.Event.ENTER_FRAME, monitorStageSize);
			_starling.removeEventListener(starling.events.Event.CONTEXT3D_CREATE, removeSplash);
			removeChild(_splash);
			_splash = null;
			setupAppScreen();
			startApp();
		}
		private function startApp():void{
			this._starling.enableErrorChecking = false;
			this._starling.antiAliasing = 0;
			this._starling.start();
			stage_resizeHandler();
		}
		private function stage_resizeHandler(event:flash.events.Event = null):void
		{
			if(_starling){
				this._starling.stage.stageWidth = this.stage.stageWidth;
				this._starling.stage.stageHeight = this.stage.stageHeight;
				
				const viewPort:Rectangle = this._starling.viewPort;
				viewPort.width = this.stage.stageWidth;
				viewPort.height = this.stage.stageHeight;
				try
				{
					this._starling.viewPort = viewPort;
				}
				catch(error:Error) {}
			}
			if(_splash){
				alignSplash();
			}
		}
		
		private function stage_deactivateHandler(event:flash.events.Event):void
		{
			stage.frameRate = 4;
			if(_starling){
				this._starling.stop();
			}
			this.stage.addEventListener(flash.events.Event.ACTIVATE, stage_activateHandler, false, 0, true);
		}
		
		private function stage_activateHandler(event:flash.events.Event):void
		{
			stage.frameRate = 60;
			if(_starling){
				this._starling.start();
			}
			this.stage.removeEventListener(flash.events.Event.ACTIVATE, stage_activateHandler);
		}
	}
}